<?php

$root = dirname(__FILE__);
$params = require_once($root . DIRECTORY_SEPARATOR . 'params.php');
$mainEnvFile = $root . DIRECTORY_SEPARATOR . 'main-env.php';
$mainEnvConfiguration = file_exists($mainEnvFile) ? require($mainEnvFile) : array();

if (isset($_GET['pqp_peterson_mm'])) {
	
	if(!isset($mainEnvConfiguration['components'])) {
		$mainEnvConfiguration['components'] = array();
	}
	
	$mainEnvConfiguration['components']['db'] = array(
		'enableProfiling' => true,
		'enableParamLogging' => true,
	);
	$mainEnvConfiguration['components']['log'] = array(
		'class' => 'CLogRouter',
		'routes' => array(
			array(
				'class'=>'ext.pqp.PQPLogRoute',
				'categories' => 'application.*, exception.*',
			),
		),
	);
}

return CMap::mergeArray(
array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Mobile Marketer', //trademark in views/layout/main.php
    
    //'theme'=>'mobile',
    // language settings
    'sourceLanguage'=>'00',
    'language'=>'en',

	// preloading log & boostrap component
	'preload'=>array('log','bootstrap'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.extensions.phpass.PasswordHash',
        'application.extensions.yii-mail.YiiMailMessage',
        'application.extensions.mobiledetect.Mobile_Detect',
        'application.extensions.custom-extensions.*',
		'application.extensions.imperavi-redactor-widget.*',
	),
	'modules'=>array(
		// uncomment the following to enable the Gii tool
		/*'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1',
			'generatorPaths'=>array(
                'bootstrap.gii',
            ),
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),*/
		'admin',		
	),

	// application components
	'components'=>array(
        'request'=>array(
            'class' => 'HttpRequest',
			'enableCsrfValidation'=>true,
			'enableCookieValidation'=>true,
			'noCsrfValidationRoutes'=>array('api/create', 'api/update/', 'api/delete'),
        ),
        'bootstrap'=>array(
            'class'=>'ext.bootstrap.components.Bootstrap',
            'responsiveCss' => true,
        ),
		'user'=>array(
			'class' => 'WebUser', //Use the protected/components WebUser class
			'allowAutoLogin'=>true, // enable cookie-based authentication
		),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions'=>array(
                'host'=>'smtp.mandrillapp.com',
                'port'=>587,
                'username'=>'peterson@salescampaigns.com',
                'password'=>'4jj7BU0dpIcYyXIDKc_UBg',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
		'sendgrid'=>array(
			'class' => 'ext.sendgrid.sendgrid',
			'sg_user'=>'cgsmith',
			'sg_api_key'=>'DynaTron78BR', 
		),
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules' => $params['url.rules'],
		),
		'db'=>array(
			'connectionString' => $params['db.connectionString'],
			'emulatePrepare' => true,
			'username' => $params['db.username'],
			'password' => $params['db.password'],
			'charset' => 'utf8',
			'schemaCachingDuration' => 3600,
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'trace, info, error, warning',
                    'categories'=>'system.*',
				),
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'info, error, warning',
                    'logFile'=>'mobilemarketer.log',
                    'categories'=>'custom',
                ),
				// uncomment the following to show log messages on web pages
                
                
				/* array(
					'class'=>'CWebLogRoute',
				),  */



			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
        'licenseKey' => "Leased-5ebfd434a0",
        'licensePath' => 'licensefile.txt',
        'mobileTheme' => 'mobile',
		'adminEmail'=>array('minturn@salescampaigns.com'=>'Mobile Marketer'),
        'company'=>'Sales Automation Support, Inc.',
		'phone'=>'1-262-754-8712',
		'web'=>'http://www.salessupport.com',
        'companyUrl'=>'http://www.salessupportcenter.com',
        'build'=>'700f7d2',
        'images'=>array(
            'headblank'=>'images/headblank.jpg',
            'sigblank'=>'images/sigblank.jpg',
        ),
        //phpass settings
        'phpass'=>array(
            'iteration_count_log2'=>8, //controls number of iterations for key stretching. ie: 8 means 2^8 times
            'portable_hashes'=>false,
        ),

        //This is used for production confirmation numbers
        'confirmation'=>array(
            'prefix'=>'SA',
        ),

        //You may use this to display news flashes on the Home Page of Mobile Marketer
        'news'=>array( //displays a flash on the main page with information
            'on'=>true, //true or false (on or off)
            'type'=>'info', //success, info, warning, error (green, blue, yellow, red)
            'bold'=>'Welcome to Mobile Marketer', //registered trade mark in views/site/index.php
            'text'=>'Click around and get used to our new layout! There are plenty of features coming down the road
            that we know you\'ll love.',
        ),
		'apiSalt' => 'G;?Qn>i)b;h(`Si%t*(p~(.Ab9}Z6uD:Y5a',
		'convertApi' => array(
			'pdfKey'=>'493212372'
		),
		'currency_symbol' => '$',
		'params' => $params,
	),
),
$mainEnvConfiguration
);
