<?php
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Mobile Marketer', //trademark in views/layout/main.php
	// preloading log & boostrap component
	'preload'=>array('log'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.extensions.yii-mail.YiiMailMessage',
	),
	// application components
	'components'=>array(
		'user' => array(
            'class' => 'ConsoleUser', //Use the protected/components WebUser class
            'primaryKey' => 14,
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions'=>array(
                'host'=>'smtp.mandrillapp.com',
                'port'=>587,
                'username'=>'peterson@salescampaigns.com',
                'password'=>'4jj7BU0dpIcYyXIDKc_UBg',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
		//MySQL Database connector
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=peterson_mm',
			'emulatePrepare' => true,
			'username' => 'peterson_mm',
			'password' => "LjUp:]Sb7+*[dY'p?(",
			'charset' => 'utf8',
            'schemaCachingDuration' => 3600,
		),
		'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'logFile' => 'cron.log',
                    'levels' => 'error, warning',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'trace',
                    'logFile' => 'cron_trace.log',
                ),
            ),
        ),
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
        'licenseKey' => "Leased-5ebfd434a0",
        'licensePath' => 'licensefile.txt',
        'mobileTheme' => 'mobile',
		'adminEmail'=>'minturn@salescampaigns.com',
        'company'=>'Sales Automation Support, Inc.',
		'phone'=>'1-262-754-8712',
		'web'=>'http://www.salessupport.com',
        'companyUrl'=>'http://www.salessupportcenter.com',
        'build'=>'700f7d2',
        'images'=>array(
            'headblank'=>'images/headblank.jpg',
            'sigblank'=>'images/sigblank.jpg',
        ),
        //phpass settings
        'phpass'=>array(
            'iteration_count_log2'=>8, //controls number of iterations for key stretching. ie: 8 means 2^8 times
            'portable_hashes'=>false,
        ),
        //This is used for production confirmation numbers
        'confirmation'=>array(
            'prefix'=>'SA',
        ),
        //You may use this to display news flashes on the Home Page of Mobile Marketer
        'news'=>array( //displays a flash on the main page with information
            'on'=>true, //true or false (on or off)
            'type'=>'info', //success, info, warning, error (green, blue, yellow, red)
            'bold'=>'Welcome to Mobile Marketer', //registered trade mark in views/site/index.php
            'text'=>'Click around and get used to our new layout! There are plenty of features coming down the road
            that we know you\'ll love.',
        ),
		'apiSalt' => 'G;?Qn>i)b;h(`Si%t*(p~(.Ab9}Z6uD:Y5a',
		'convertApi' => array(
			'pdfKey'=>'493212372'
		),
		'currency_symbol' => '$',
	),
);