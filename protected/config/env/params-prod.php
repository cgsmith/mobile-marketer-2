<?php
/**
 * params-prod.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 5:51 PM
 */
return array(
	'env.code' => 'prod',

	// DB connection configurations
	'db.name' => 'peterson_mm',
	'db.connectionString' => "mysql:host=localhost;dbname=peterson_mm",
	'db.username' => "peterson_mm",
	'db.password' => "LjUp:]Sb7+*[dY'p?(",
	'db.charset' => "utf8",
	'db.emulatePrepare' => true,
	'db.tablePrefix' => "tbl_",
		
	// this is used in contact page
	'adminEmail'=>'cstrand@total-mechanical.com',
);