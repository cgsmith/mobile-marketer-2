<?php
/**
 * params.php
 *
 * Holds frontend specific application parameters.
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/22/12
 * Time: 1:38 PM
 */

$paramsEnvFile = $root . DIRECTORY_SEPARATOR . 'params-env.php';
$paramsEnvFileArray = file_exists($paramsEnvFile) ? require($paramsEnvFile) : array();

return CMap::mergeArray(
		array(
			'url.rules' => array(
                'lead/<id:\d+>/<title:.*?>'=>'lead/view',
                'leads/<tag:.*?>'=>'lead/index',
                // REST Patterns
				// array('api/view', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
				// array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
				// array('api/view', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
                array('api/lookup', 'pattern'=>'api/<model:\w+>/lookup', 'verb'=>'GET'),
                array('api/update', 'pattern'=>'api/<model:\w+>/update/<id:\d+>', 'verb'=>'POST'),
                array('api/delete', 'pattern'=>'api/<model:\w+>/delete/<id:\d+>', 'verb'=>'DELETE'),
                array('api/create', 'pattern'=>'api/<model:\w+>/create', 'verb'=>'POST'),
				// array('api/create', 'pattern'=>'api/<model:\w+>/order', 'verb'=>'POST'),
                // Other Controllers
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			// add here all parameters
			'param' => 'value',
		),
		$paramsEnvFileArray
);