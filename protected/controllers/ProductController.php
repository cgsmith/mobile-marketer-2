<?php

class ProductController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
//                array('allow',  // allow all users to perform 'index' and 'view' actions
//                        'actions'=>array('view'),
//                        'users'=>array('*'),
//                ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('view','components', 'component'),
                'users'=>array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index', 'create','update', 'GetProductsGrid'),
                'roles'=>array('admin', 'manager'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete','sendTestEmails'),
                'roles'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
//            $components = Component::model()->findByAttributes(array('product_id'=>$id));
        $model=Product::model()->with('components')->findByPk($id);
        if($model===null) throw new CHttpException(404,Yii::t('core','The requested page does not exist.'));
        if(!$model->isUserbelongsToProduct() && !Yii::app()->user->checkAccess('admin')){
            throw new CHttpException(401, Yii::t('core','You are not authorized to view this product.'));
        }
        $this->render('view',array(
//                    'model'=>$this->loadModel($id),
            'model'=>$model,
        ));
    }

    // TODO: moved to ComponentController
    /**
     * @param unknown $id
     * @param unknown $docId
     * @return unknown
     */
    public function componentMergeButton($id,$docId) {
        $docs = Yii::app()->db->createCommand()
            ->select('id, CONCAT(first_name, " " ,last_name) AS name')
            ->from('customer_user')
            ->where('customer_main_id=:id',array(':id'=>$id))
            ->queryAll();
        $onclick='';
        $htmlItems = '<form method=\"GET\" action=\"'.Yii::app()->baseUrl.'/component/preview\"><input type=\"hidden\" name=\"id\" value=\"'.$docId.'\">User: <select name=\"user\">';
        foreach ($docs as $doc) {
            $htmlItems .= '<option value=\"'.$doc['id'].'\">'.$doc['name'].'</option>';
        }
        $htmlItems .= '</select><br><input type=\"submit\"></form>';
        $onclick ='js:bootbox.modal("'.$htmlItems.'", "Select a User");';


        $button = $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Merge',
            'size'=>'mini',
            'htmlOptions'=>array(
                'style'=>'margin-left:3px',
                'onclick'=>$onclick,
            ),
        ));
        return $button;
    }
    public function actionComponents($id)
    {
//            $model = $this->loadModel($id);
        $model = Product::model()->with('components')->findByPk($id);
        $component = array();
        if($model===null) throw new CHttpException(404,'The requested page does not exist.');

        if(!$model->isUserbelongsToProduct() && !Yii::app()->user->checkAccess('admin')){
            throw new CHttpException(401, Yii::t('core','You are not authorized to perform this action.'));
        }

        if(isset($_POST['Component']['id'])){
            $component = $this->updateComponent($model, $_POST['Component']['id']);
        } else if(!empty($model->components[0])) {
            $component = $model->components[0];
        } else { // redirect if product don't have any component
            Yii::app()->user->setFlash('notice',Yii::t('core', 'There is no components created yet. Please create one.'));
            $this->redirect($this->createUrl('component/create', array('product_id'=>$id)));
        }

        $this->render('componentView',array(
            'model' => $model,
            'component' => $component,
        ));
    }


    protected function createComponent($product){
        $component = new Component;
        if(isset($_POST['Component'])){
            $component->attributes = $_POST['Component'];
            if($product->addComponent($component)){
                Yii::app()->user->setFlash('componentCreated', 'The component has been created successfully.');
                $this->refresh();
            }
        } else {
            // Load the first component.
            $component = $product->components[0];
        }
        return $component;
    }

    protected function updateComponent($product, $component_id){
        $component = Component::model()->findByPk($component_id);
        if($component!==NULL){
            $_document_id = $component->document_id;
            if(isset($_POST['Component'])){
                $component->attributes = $_POST['Component'];

                $component_file = CUploadedFile::getInstance($component,'document_id'); //Get uploaded file instance
                if (!empty($component_file)) {
                    $attachment = ($component->component_type_id == 12) ? true : false;
                    $component->processDocument($component_file, $component,$attachment);
                }
                // if($product->updateComponent($component)){
                if($component->save()){
                    if(!empty($_document_id) && empty($component->document_id)){
                        $component->document_id = $_document_id;
                        $component->save();
                    }
                    Yii::app()->user->setFlash('componentCreated', 'The component has been created successfully.');
                    $this->refresh();
                }
            } else {
                // Load the first component.
                $component = $product->components[0];
            }
        }
        return $component;
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new Product;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Product']))
        {
            $model->attributes=$_POST['Product'];
            $model->active = true;

            if($model->save())
                $this->redirect(array('component/create','product_id'=>$model->id));
        }
        if(isset($_GET['id']))
            $model->customer_main_id = $_GET['id'];
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Product']))
        {
            $model->attributes=$_POST['Product'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $customer = $model->customer_main_id;
        $model->active = 0;
        $model->save();
        //$this->loadModel($id)->delete();
        Yii::app()->user->setFlash('productDeleted', 'Product and components deleted successfully.');
        $this->redirect(array('customer/'.$customer));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $product_filter = array();
        $customer_id = Yii::app()->user->customer_id;
        if(Yii::app()->user->checkAccess('admin')){
            $product_filter = array(
                'criteria' => array(
                    'condition'=>'customer_main_id=:customerId AND active = :active',
                    'params'=>array(':customerId'=>$customer_id, ':active'=>1),
                )
            );
        }
        $dataProvider=new CActiveDataProvider('Product', $product_filter);
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    public function actionGetProductsGrid($show_inactive){
        $product_filter = array();
        $customer_id = Yii::app()->user->customer_id;
        if(Yii::app()->user->checkAccess('admin')){
            if($show_inactive == "1")
                $_condition = 'customer_main_id='.$customer_id;
            elseif($show_inactive == "0")
                $_condition = 'customer_main_id='.$customer_id.' AND active = 1';
            $product_filter = array(
                'criteria' => array(
                    'condition'=>$_condition,
                )
            );
        }
        $dataProvider=new CActiveDataProvider('Product', $product_filter);
        $this->renderPartial('_products_grid', array('dataProvider' => $dataProvider, 'show_inactive' => $show_inactive));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Product('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Product']))
            $model->attributes=$_GET['Product'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=Product::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if(isset($_POST['ajax']) && $_POST['ajax']==='product-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actionComponent(){
//            CVarDumper::dump($_GET);
        if(isset($_GET['email'])){
            $component = Component::model()->findByPk($_GET['email']);
            if($component->email_content){
                echo $component->getPreview();
            }
        }
        if(isset($_GET['pdf'])){
            $file = dirname(Yii::app()->basePath).DIRECTORY_SEPARATOR.'components/pdf/'.$_GET['pdf'];
            if(file_exists($file)){
                Yii::app()->request->xSendFile($file,array("mimeType"=>"application/pdf","terminate"=>false));
                readfile($file);
            } else {
                throw new CHttpException('404', "File Not Found.");
            }
        }
        exit;
    }

    public function actionSendTestEmails() {
        if (isset($_REQUEST['emails'])) {
            $emails = explode(',',$_REQUEST['emails']); //emails to array for swiftmailer
            $product = $this->loadModel($_REQUEST['productid']);
            $components = $product->getComponentEmails();

            $production = new Production();
            foreach ($components as $component) {
                $production->sendTestEmail($product->id,$component['id'],$emails);
            }

        }
    }
}
