<?php

class ProductionController extends Controller {
    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }
    
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index','produce', 'ajaxupdate', 'sendemail','sendbatchemail'),
                'users'=>array('@'),
                'roles' => array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    public function actionIndex() {
        error_reporting(E_ERROR);
        if(isset($_POST['Production'])){
            $data = $_POST['Production'];
            $selected_orders = $data['selected'];
            if(!empty($selected_orders)){
            	// No need for .docx files as required @ 02/06/2014 so commented
                //$download_type = $data['download_type'];
            	$download_type = 1;	// refers to type ZIP
                Production::getMergedDocxAsSingleFile($selected_orders, $download_type);
            }
        }
        // Switch Autoloader to Yii
        spl_autoload_register(array('YiiBase','autoload'));
        $date = array('from'=>  date('m/d/Y', time()), 'to'=>date('m/d/Y', time()));
        $criteria = new CDbCriteria();
        $date_from = date('Y-m-d', strtotime($date['from']));
        $date_to = date('Y-m-d', strtotime($date['to']));
              
        if (isset($_GET['Date'])){
            if(!empty($_GET['Date']['from'])){
                $date['from'] = $_GET['Date']['from'];
                $date_from = date('Y-m-d', strtotime($date['from']));
                $criteria->compare('date_ordered', '>='.$date_from);
            }
            if(!empty($_GET['Date']['to'])){
                $date['to'] = $_GET['Date']['to'];
                $date_to = date('Y-m-d', (strtotime($date['to'])+86400) );
                $criteria->compare('date_ordered', '<'.$date_to);
            }
        }
    
//        $dataProvider = new CActiveDataProvider('Order', array(
//            'criteria' => $criteria,
//            'pagination' => array(
//                'pageSize' => 10,
//            ),
//        ));
//        $criteria->with = array('lead', 'order', 'order.orderLeads', 'order.product', 'order.frequency', 'order.customerUser');
        $filter = isset($_GET['filter'])? $_GET['filter']: Production::NOT_PRINTED;
        $components_data = Production::listComponentsOrdered($filter, Component::TYPE_ALL, array(Component::TYPE_EMAIL));
        //var_dump($components_data);exit;
        $list_data = array();
        $count = 0;
//        if(!empty($date_from) && !empty($date_to)){
            foreach($components_data as $key => $data){
                $frequency_days = json_decode($data['frequency_days']);
                $production_date_timestamp = strtotime($data['date_ordered'])+ ($frequency_days[(int)$data['component_sequence']-1] * 24*60*60);
                $data['date_production'] = date('m-d-Y',$production_date_timestamp);
                if(!empty($date_from) && !empty($date_to)){
                    if($production_date_timestamp<strtotime($date['from']) || $production_date_timestamp>=strtotime('+1 day', strtotime($date['to']))){
                        continue;
                    }
                }
                $data['user'] = $data['customer_user_firstname'].' '.$data['customer_user_lastname'];
                // order-component-lead
                $data['selected'] = $data['id'].'-'.$data['component_id'].'-'.$data['lead_id'];
                $list_data[] = $data;
                
                $count++;
            }
        //var_dump($list_data);exit;
//        }
        
//        $count_sql = "SELECT 
//                    count(c.id) AS total_components
//                FROM 
//                    component c, frequency f, product p, `order` o, order_leads ol, customer_lead cl
//                WHERE 
//                    c.product_id=p.id AND 
//                    p.id=o.product_id AND 
//                    o.id=ol.order_id AND 
//                    ol.lead_id=cl.id AND 
//                    p.frequency_id=f.id AND
//                    o.id IS NOT NULL";
//        $count = Yii::app()->db->createCommand($count_sql)->queryScalar();
        $dataProvider = new CArrayDataProvider($list_data, array(
            'keyField' => 'id',
            'id' => 'id',
//            'totalItemCount' => $count,
            'pagination' => false,
            'sort'=>array(
                'attributes'=>array(
                     'id','product_name','product_type','component_name','component_type','date_ordered','customer_name','user','date_production', 'component_sequence', 'lead_first_name','lead_last_name'
                ),
            ),
        ));
        
        $this->render('index', array('dataProvider'=>$dataProvider, 'date'=>$date, 'model'=>new Production));
    }
    
    /**
     * This function produces a production document based on the order number
     * If $test=true then the model will not advance the curr_seq_number
     *
     * Steps
     * -Get order number and customer number
     * -Get user
     * -Get leads
     * -Get product's components based on sequence number
     */
    public function actionProduce($order_id,$component_id,$lead_id) {
        
        $order = Order::model()->with(array('customerUser', 'product'))->findByPk($order_id);
        $lead = Lead::model()->findByPk($lead_id);
        $component = Component::model()->with(array('componentDocument'))->findByPk($component_id);
        
        if($order==null || $lead==null || $component==null || $component->componentDocument==null){
            throw new CHttpException(404, Yii::t('core', 'Cannot find info related with this order.'));
        }
        
        $component_file_path = dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . $component->componentDocument->url;
        if(!file_exists($component_file_path)){
            throw new CHttpException(404, Yii::t('core', 'File not found.'));
        }
        
        $fileTime = time();
        // order_id-product_id-component_id-lead_id.timestamp
        $new_filename = $order->id.'-'.$order->product->id.'-'.$component->id.'-'.$lead->id.'-'.$fileTime;
        $mergedFile = Production::getMergedDocx($component_file_path, $new_filename, $order, $component, $lead);
        
        $conversionSave = $new_filename.'.pdf';
        $pdffile = $component->createPDF($mergedFile,'components/pdf/ordered/',$conversionSave,Yii::app()->params['convertApi']['pdfKey']);
//        $file = dirname(Yii::app()->basePath).DIRECTORY_SEPARATOR.'components/pdf/'.$_GET['pdf'];
        if(file_exists($pdffile)){
            Yii::app()->request->xSendFile($pdffile,array("mimeType"=>"application/pdf","terminate"=>false));
            readfile($pdffile);
        } else {
            throw new CHttpException('404', "File Not Found.");
        }
    }

    public function actionSendemail($order_id, $component_id, $lead_id){
        $result = Production::sendEmail($order_id, $component_id, $lead_id);
        if($result){
            Yii::app()->user->setFlash('success', 'Email has been sent successfully.');
        } else {
            Yii::app()->user->setFlash('error', 'Email sending failed. Try again.');
        }
        $this->redirect(array('production/index'));
    }
 public function actionSendbatchemail($order_id){
        $order = Order::model()->with(array('customerUser','customerUser.customerMain', 'product'))->findByPk($order_id);

        $components = Component::model()->with(array('componentDocument','componentType'))->findByPk();
     echo $order->product->name;

     exit;//var_dump($order);exit;
     //$components =
        $component = Component::model()->with(array('componentDocument'))->findByPk($component_id);
        $u = $order->customerUser;

     if($order==null || $lead==null || $component==null){
         throw new CHttpException(404, Yii::t('core', 'Cannot find info related with this order.'));
     }

        $lead = Lead::model()->findByPk($lead_id);
        $user = array(
                'fn'=>$u->first_name,
                'ln'=>$u->last_name,
                't'=>$u->title,
                'co'=>$u->customerMain->name,
                'a1'=>$u->address1,
                'a2'=>$u->address2,
                'c'=>$u->city,
                's'=>$u->state,
                'z'=>$u->zip,
                'p'=>$u->phone,
                'm'=>$u->mobile,
                'f'=>$u->fax,
                'e'=>$u->email,
            );
        $l = array(
                'fn'=>$lead->first,
                'ln'=>$lead->last,
                't'=>$lead->title,
                'co'=>$lead->company,
                'a1'=>$lead->address1,
                'a2'=>$lead->address2,
                'c'=>$lead->city,
                's'=>$lead->state,
                'z'=>$lead->zip,
                'p'=>$lead->phone,
//                'm'=>$lead->mobile,
                'f'=>$lead->fax,
                'e'=>$lead->email,
                'notes'=>$lead->notes,
            );


            $email_content = $component->processEmail($user, $l);
//            $recipients = array($l->email);
            $recipients = array('chris@cgsmith.net'); // TODO: remove this and uncomment above line
            $recipientsName = array($lead->first . ' '. $lead->last);
            $message = new YiiMailMessage;
            $message->from = array($u->email => $u->first_name. ' ' .$u->last_name);
            $message->to = ['chris@cgsmith.net'];//[$lead->email => $lead->first. ' ' .$lead->last];
            $message->subject = $component->name;
            $message->setBody($email_content, 'text/html');
            $message->attach(Swift_Attachment::fromPath('components/5-13-1363381070.pdf')->setFilename('LPWI Newsletter and Annual Convention Invite.pdf'));
            $result = Yii::app()->mail->send($message);
            //$result =

                /*Yii::app()->sendgrid->sendMail(
                    $recipients,
                    $recipientsName,
                    '',
                    'Subject',
                    $email_content,
                    '',  // text
                    $u->email,// from_email
                    'faisal.cgs@nxvt.com', // bcc
                    $u->first_name.' '.$u->last_name,
                    $u->email // replyto
                );*/
            if($result){
                Yii::app()->user->setFlash('success', 'Email has been sent successfully.');
            } else {
                Yii::app()->user->setFlash('error', 'Email sending failed. Try again.');
            }
            //$this->redirect(array('production/index'));

    }

    public function actionAjaxupdate(){
        if(empty($_POST['Production'])){
//            throw new CHttpException(500, Yii::t('core', 'Invalid Request.'));
        }
//        $command = Yii::app()->db->createCommand();
        $production = new Production();
        switch($_GET['act']){
            case 'markasprinted':
                $selected_orders = explode(',', $_POST['selected']);
                foreach($selected_orders as $selected){
                    list($order_id, $component_id, $lead_id) = explode('-', $selected);
                    $production->markAsPrinted($order_id, $component_id, $lead_id); // it also calls addToBillingAndHistory();
                }
                echo json_encode(array('success'=>1));
                break;
            case 'confirm':
                // It looks for docx file. What if the PDF could not be generated?
//                if($model->fileExists()){
                    list($order_id, $component_id, $lead_id) = explode('-', $_POST['selected']);
                    $success = (bool)$production->markAsPrinted($order_id, $component_id, $lead_id);
                    echo json_encode(array('success'=>$success));
//                }
                break;
            default:
                throw new CHttpException(500, 'Invalid Request.');
                break;
        }
        Yii::app()->end();
    }

}