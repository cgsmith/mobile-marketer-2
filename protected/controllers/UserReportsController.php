<?php

class UserReportsController extends Controller
{
    public $date_from;
    public $date_to;

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','useremail','getCallReportForUser'),
                'roles'=>array('user')
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->render('index');
    }

    public function actionUseremail(){
        $this->date_from = date('Y-m-d H:i:s', $_GET['date_from']);
        $this->date_to = date('Y-m-d H:i:s', $_GET['date_to']);

        if(isset($_GET['date_from']) && isset($_GET['date_to'])){
            echo $this->getCallReportForUser(
                Yii::app()->user->id
            );
        }
    }

    //TODO: Normalize user call reports
    public function getCallReportForUser($user_id){
        $sql = "
            SELECT
                ohc.component_name AS component_name,
                ohc.component_type AS component_type,
                cl.first AS lead_firstname,
                cl.last AS lead_lastname,
                CONCAT_WS(' ', cu.first_name, cu.last_name) AS user,
                cl.company AS lead_company,
                cl.phone AS lead_phone,
                op.date_printed AS date_printed,
                o.confirmation_number AS confirmation
            FROM
                `order` o INNER JOIN order_printed op
                ON o.id = op.order_id

                LEFT JOIN customer_user cu
                ON cu.id = o.customer_user_id

                LEFT JOIN customer_lead cl
                ON cl.id = op.lead_id

                LEFT JOIN order_history_components ohc
                ON op.id = ohc.order_printed_id
            WHERE
                op.date_printed >= '".$this->date_from."' AND op.date_printed < '".$this->date_to."' AND o.customer_user_id=".$user_id."
            AND cu.receive_reports = 1
            AND ohc.component_type != 'Envelope'
            GROUP BY op.date_printed,ohc.component_name,lead_lastname asc
        ";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
//        echo '<pre>';
//        print_r($results);
//        echo '</pre>';
//        exit;
        if(empty($results)){
            return 'No Results';
        } else {
            $report_text = '';
            $report_text .= '<h2>User Call Report: '.date('m-d-Y', strtotime($this->date_from)).' through '.date('m-d-Y', strtotime($this->date_to)).'</h2>';
            $count_results = count($results);
            $lead_count_string = $count_results>1?' leads':' lead';
            $report_text .= '<h3>'.$results[0]['user'] . ' - Materials sent to '.$count_results . $lead_count_string. '</h3>';

            foreach($results as $result){
                $date = new DateTime($result['date_printed']);
                $date->modify('+1 week');
                $format =
                    '%s, %s<br/>
                     %s<br/>
                     %s<br/>
                     <br/>
                     Call on %s<br/>
                     <br/>
                     %s - %s - %s<br/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%s - %s<br/>';
                $report_text .= sprintf($format,$result['lead_lastname'],$result['lead_firstname'],$result['lead_company'],
                    $result['lead_phone'],$date->format('l m/d/Y'),$result['component_type'],$result['component_name'],
                    $result['confirmation'],$result['date_printed'],$result['component_name']);
                $report_text .= '-----------------------------------------<br /><br />';
            }
            return $report_text;
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Postage the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Reports::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

} 