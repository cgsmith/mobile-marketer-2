<?php

class ComponentController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        private $_product = null;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
                    'accessControl', // perform access control for CRUD operations
                    'productContext + create' // check to ensure a product is selected.
		);
	}
        
        public function filterProductContext($filterChain){
            if(isset($_GET['product_id'])){
                $this->loadProduct($_GET['product_id']);
            } else {
                throw new CHttpException(403, 'Must specify a product before performing this action.');
            }
            $filterChain->run();
        }
        
        protected function loadProduct($productId){
            if($this->_product===null) {
                $this->_product=Product::model()->findByPk($productId);
                if($this->_product===null) {
                    throw new CHttpException(404,'The requested project does not exist.');
                }
            }
            return $this->_product;
        }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','preview','document'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($product_id)
	{

        $criteria = new CDbCriteria();
        $criteria->addCondition('product_id',$product_id);
        $model = Component::model()->findAll($criteria);
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
            $model=new Component;
            if(!$this->_product->isUserbelongsToProduct() && !Yii::app()->user->checkAccess('admin')){
                throw new CHttpException(401, Yii::t('core', 'You are not authorized to perform this action.'));
            }
        // We will preset the model with the product information if the ID is set
            if(isset($_GET['product_id'])) {
                $model->product_id = Yii::app()->request->getQuery('product_id'); //set the product_id for the model
                $sequenceQuery =  Yii::app()->db->createCommand()
                    ->select('MAX(sequence)')
                    ->from('component')
                    ->where('product_id=:id AND active=1',array(':id'=>$model->product_id))
                    ->queryColumn();
                $model->sequence = ceil($sequenceQuery[0] + .009);//set the next highest sequence
                $product = Product::model()->findByPk($model->product_id);
            }

            /**
             * If component is posted then perform the following:
             *  1. Check if all the proper fields are completed
             *  2. Insert the database entry so we can get the ID of our component
             *  3. Upload the DOCX file to the appropriate directory (name: productid-componentid-time().extension)
             *      a. Update component table entry with docxid from document table
             *  4. Submit DOCX file to API for PDF conversion
             *  5. When PDF is received back, link in document table
             */
            if(isset($_POST['Component'])) {
                $model->attributes=$_POST['Component'];
                $model->active = true;

//                if (!$model->save()) { //Save the model and then get the component ID
//                   print_r($model->getErrors()); //TODO: DO NOT print errors!
//                    exit;
//                }
//                $componentId = $model->getPrimaryKey(); //The component ID is used for updates and file naming
                $transaction = Yii::app()->db->beginTransaction();
                if($model->save()){
                        if($model->component_type_id != 8){
                            $component_file = CUploadedFile::getInstance($model,'document_id'); //Get uploaded file instance
                            if (!empty($component_file)) {
                                $attachment = ($model->component_type_id == 12 || $model->component_type_id == 3) ? true : false;
                                $model->processDocument($component_file, $model, $attachment);
                            }
                        } else {
                            $user = array();
                            $lead = array();
                            $model->processEmail($user, $lead); // TODO: need to implement this
                        }
                        if($model->hasErrors()){
                            $transaction->rollback();
                        } else {
                            $transaction->commit();
                            $this->redirect(array('product/components','id'=>$model->product_id));
                        }
                }
            }

            $this->render('create',array(
                            'model'=>$model,
                            'product'=>$product,
            ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
//		$this->performAjaxValidation($model);

		if(isset($_POST['Component']))
		{
                    $model->attributes=$_POST['Component'];
                    if($model->save()) {
                        if($model->component_type_id!=8){
                            $component_file = CUploadedFile::getInstance($model,'document_id'); //Get uploaded file instance
                            if (!empty($component_file)) {
                                $attachment = ($model->component_type_id == 12) ? true : false;
                                $model->processDocument($component_file, $model, $attachment);
                            } else {
                                $model->addError('document_id', 'Document cannot be uploaded. Try again.');
                            }
                        } else {
                            $user = array();
                            $lead = array();
                            $model->processEmail($user, $lead); // TODO: need to implement this
                        }
                        $this->redirect(array('product/components','id'=>$model->product_id));
                    } else {
                        if(isset($_GET['ajax'])){
                            
                        }
                    }
		}
                
                if(isset($_GET['ajax'])){ // Its for "product/components/8"
                    echo $this->renderPartial('_form',array('model'=>$model, 'ajax'=>true));
                } else {
                    $this->render('update',array(
                            'model'=>$model,
                    ));
                }

                    
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);
        $id = $model->product_id;
        $model->delete();
        Yii::app()->getUser()->setFlash('success', 'Component deleted successfully.');
        $this->redirect(array('product/components/'.$id));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Component');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Component('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Component']))
			$model->attributes=$_GET['Component'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    public function actionDocument($document_id)
    {
        $doc = ComponentDocument::model()->findByPk($document_id);
        return Yii::app()->getRequest()->sendFile('Source File.docx',file_get_contents($doc->url),true);
    }

    public function actionPreview($id,$user)
    {
        $model= Component::model()->findByPk($id);

        $user = User::model()->findByPk($user);

        //var_dump($user);exit;
        $user2 = $model->setUserVariables($user);
        $lead = $model->setLeadVariables(Lead::model()->findByPk(1));
        $mergedFile = $model->mergeFields($model->componentDocument->url,[$user2],[$lead]);
        $model->changePictures($mergedFile,['signature'=>$user->signature_file,'headshot'=>$user->headshot_file]);
        if(Yii::app()->user->checkAccess('admin'))
        	$model->addRedPreviewBox($mergedFile); 
        
        return Yii::app()->getRequest()->sendFile('Example Merge.docx',
            file_get_contents($mergedFile),true);
    }

    public function componentMergeButton($id,$docId) {
    	$docs = Yii::app()->db->createCommand()
    	->select('id, CONCAT(first_name, " " ,last_name) AS name')
    	->from('customer_user')
    	->where('customer_main_id=:id',array(':id'=>$id))
    	->queryAll();
    	$onclick='';
    	$htmlItems = '<form method=\"GET\" action=\"'.Yii::app()->baseUrl.'/component/preview\"><input type=\"hidden\" name=\"id\" value=\"'.$docId.'\">User: <select name=\"user\">';
    	foreach ($docs as $doc) {
    		$htmlItems .= '<option value=\"'.$doc['id'].'\">'.$doc['name'].'</option>';
    	}
    	$htmlItems .= '</select><br><input type=\"submit\"></form>';
    	$onclick ='js:bootbox.modal("'.$htmlItems.'", "Select a User");';
    
    
    	$button = $this->widget('bootstrap.widgets.TbButton', array(
    			'label'=>'Merge',
    			'size'=>'mini',
    			'htmlOptions'=>array(
    					'style'=>'margin-left:3px',
    					'onclick'=>$onclick,
    			),
    	));
    	return $button;
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Component::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='component-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
