<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{

        return array(
            array('allow',  // allow authenticated users to perform the following actions
                'actions'=>array('index','view','update', 'generateapikey'),
                'roles'=>array('user','manager','admin'),
            ),
            array('allow', // allow admin user to perform 'admin' actions
                'actions'=>array('admin','delete'),
                'roles'=>array('admin'),
            ),
            array('deny',
                'actions'=>array('index','view','update','admin','delete', 'generateapikey'),
                'users'=>array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id=null)
    {
        $model=new User;
        if($id !== null){
            $company = Customer::model()->findByPk($id);
            $user = User::model()->findByAttributes(array('customer_main_id'=>$id));
            $model->customer_main_id = $id;
            $model->address1 = $user->address1;
            $model->address2 = $user->address2;
            $model->city = $user->city;
            $model->state = $user->state;
            $model->zip = $user->zip;
        } else {
            $company = null;
        }


        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['User']))
        {
            //Create the user entry
            //Get random password first
            $length = 10;
            $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
            shuffle($chars);
            $password = implode(array_slice($chars, 0, $length));

            $model->attributes=$_POST['User'];
            $model->password1 = $password; //Both password1 and password2 have to be set for the hash to generate
            $model->password = $password; //Both password1 and password2 have to be set for the hash to generate
            $model->password2 = $password;
            $model->active=true;

            if($model->save()){
				// create a sample lead for our new user model todo abstract when necessary
				$sampleLeadController = new LeadController();
				$sampleLeadAttributes = array(
					'active'=>1,
					'email'=>'peterson@salescampaigns.com',
					'first'=>'Tim',
					'last' => 'Peterson',
					'title' => 'President',
					'company' => 'Sales Automation Support, Inc.',
					'address1' => '17025 W Rogers Drive',
					'city' => 'New Berlin',
					'state' => 'WI',
					'zip' => '53151',
					'phone' => '(262) 754-8712',
					'notes' => "Tim Peterson helped set us up with Mobile Marketer",
				);
				$sampleLeadController->createSampleLead($sampleLeadAttributes);

				// todo abstract email
                $message = new YiiMailMessage;
                $message->view = 'email';
				$message->subject = "You are all set on Mobile Marketer! Let's sell together!";
				$email = "Hello!<br><br>
								We are glad to have you as part of <a href=\"https://www.mobilemarketer.net\">Mobile Marketer</a>!  Below is your username
								and temporary password to get started.<br><br>
								Username: {$model->email}<br>
								Password: {$password}<br>
								<br>
								Note: once you have logged in please change your password under the user profile screen.<br><br>
								<h5>Let's sell together!</h5>";
				$params = array('content' => $email);
				$message->setBody($params, 'text/html');
                $message->addBcc('minturn@salescampaigns.com');
                $message->addBcc('peterson@salescampaigns.com');
                $message->addTo($model->email);
                $message->from = Yii::app()->params['adminEmail'];
                Yii::app()->mail->send($message);

                Yii::app()->getUser()->setFlash('success','New customer created successfully.');
                $this->redirect(array('view','id'=>$model->id));
            }
        }

        $this->render('create',array(
            'model'=>$model,
            'company'=>$company,
        ));
    }


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        //NOTE!!!  IF YOU ARE LOOKING FOR THE USER PROFILE LOOK IN SITECONTROLLER.PHP
		$model=$this->loadModel($id);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
                $this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
            $model=$this->loadModel($id);
            $customer_id = $model->customer_main_id;
//            $model->setAttribute('active',0); // set active to false (essentially deleting the lead)
            $model->active = 0;
//            $model->setAttribute('email',null); //set email to null so they may be created again
            $model->email = NULL;
            $success = false;
            $model->scenario = 'delete';
            if ($model->save()) {
                $success = true;
            }
            if($_POST['ajax']){
                if($success) echo CJSON::encode(array('success'=>true));
                else echo CJSON::encode (array('success'=>false));
                Yii::app()->end();
            }
            if(!isset($_GET['ajax'])){
                if($success){
                    Yii::app()->getUser()->setFlash('success','User removed successfully.');
                } else {
                    Yii::app()->getUser()->setFlash('error','User could not be removed.');
                }
                $this->redirect(array('customer/'.$customer_id));
            }
            
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionGenerateapikey(){
            echo CJSON::encode(Api::generateApiKey(Yii::app()->user));
        }
}
