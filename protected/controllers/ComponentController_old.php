<?php

class ComponentController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function parseHeader($header='')
    {
        $resArr = array();
        $headerArr = explode("\n",$header);
        foreach ($headerArr as $key => $value) {
            $tmpArr=explode(": ",$value);
            if (count($tmpArr)<1) continue;
            $resArr = array_merge($resArr, array($tmpArr[0] => count($tmpArr) < 2 ? "" : $tmpArr[1]));
        }
        return $resArr;
    }

    /**
     * Converts the file into a PDF so it is viewable from the order screen
     * @param $file string being converted
     * @param $savePath string it will save to
     * @param $saveFile string the filename is going to be
     * @param $apiKey string for convertAPI
     * @return bool|string bool false if no conversion, or return save string
     */
    public function createPDF($file,$savePath,$saveFile,$apiKey)
    {
        try {
            $postData = array('OutputFileName'=>$saveFile,
                              'ApiKey'=>$apiKey,
                              'file'=>"@".$_SERVER['DOCUMENT_ROOT'].$file,
                              'InputFormat'=>'docx');
            $ch = curl_init("http://do.convertapi.com/word2pdf");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch,CURLOPT_HEADER,1);
            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$postData);

            if (($result = curl_exec($ch))=== false){
                die('Curl error: '.curl_error($ch));
            }
            $headers = curl_getinfo($ch);
            $header=$this->parseHeader(substr($result,0,$headers["header_size"]));
            $body=substr($result, $headers["header_size"]);

            curl_close($ch);
            if (0 < $headers['http_code'] && $headers['http_code'] < 400) {
                // Check for Result = true
                if (in_array('Result',array_keys($header)) ?  !$header['Result']=="True" : true) {
                    die("Something went wrong with Conversion service."); //TODO: Route to error log
                    return false;
                }

                $fp = fopen($savePath.$saveFile, "wbx");
                fwrite($fp, $body);

                return $savePath.$saveFile;
            }else {
                die( "Exception Message : ".$result .".<br />Status Code :".$headers['http_code'].".<br />"); //TODO : route to error log
                return false;
            }
        }catch (Exception $e) {
            die("Exception Message :".$e.Message."</br>");
            return false;
        }
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($product_id)
	{

        $criteria = new CDbCriteria();
        $criteria->addCondition('product_id',$product_id);
        $model = Component::model()->findAll($criteria);
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Component;

        // We will preset the model with the product information if the ID is set
        if(isset($_GET['product_id'])) {
            $model->product_id = Yii::app()->request->getQuery('product_id'); //set the product_id for the model
            $sequenceQuery =  Yii::app()->db->createCommand()
                ->select('MAX(sequence)')
                ->from('component')
                ->where('product_id=:id AND active=1',array(':id'=>$model->product_id))
                ->queryColumn();
            $model->sequence = ceil($sequenceQuery[0] + .009);//set the next highest sequence
            $product = Product::model()->findByPk($model->product_id);
        }

        /**
         * If component is posted then perform the following:
         *  1. Check if all the proper fields are completed
         *  2. Insert the database entry so we can get the ID of our component
         *  3. Upload the DOCX file to the appropriate directory (name: productid-componentid-time().extension)
         *      a. Update component table entry with docxid from document table
         *  4. Submit DOCX file to API for PDF conversion
         *  5. When PDF is received back, link in document table
         */
        if(isset($_POST['Component'])) {
            $model->attributes=$_POST['Component'];
            $model->active = true;

            if (!$model->save()) { //Save the model and then get the component ID
               print_r($model->getErrors()); //TODO: DO NOT print errors!
                exit;
            }
            $componentId = $model->getPrimaryKey(); //The component ID is used for updates and file naming

            $component_file = CUploadedFile::getInstance($model,'document_id'); //Get uploaded file instance
            if (!empty($component_file)) {
                $compDocModel = new ComponentDocument; //Instantiate a new model for the component document
                $fileTime = time();
                $componentSave = 'components/'.$model->product_id.'-' //Where document will be stored and saved
                                  .$componentId.'-'
                                  .$fileTime.'.'.$component_file->getExtensionName();
                $conversionSave = $model->product_id.'-' //What the converted document will be called
                                  .$componentId.'-'
                                  .$fileTime.'.pdf';
                $component_file->saveAs($componentSave); //Save the file to the path
                chmod($componentSave,0755);

                //Set the model's attributes
                $compDocModel->url = $componentSave;
                $compDocModel->name = $component_file->getName();
                if (!$compDocModel->save()) {
                    print_r($compDocModel->getErrors()); //TODO: Route error properly!
                    exit;
                }

                //Update the component with the document model ID
                $model->document_id = $compDocModel->getPrimaryKey();
                if (!$model->save()) {
                    print_r($model->getErrors());  //TODO: err printing
                    exit;
                }

                //Merge the file with 'john doe' information TODO: This should be sent off to BG process
                require_once('components/tbs_class_php5.php');
                require_once('components/tbs_plugin_opentbs.php');
                $u[] = array(
                    'fn'=>'Tim',
                    'ln'=>'Peterson',
                    't'=>'President',
                    'co'=>'Sales Automation Support, Inc.',
                    'p'=>'262-754-8712',
                    'm'=>'414-788-3647',
                    'e'=>'peterson@salescampaigns.com'
                );
                $l[] = array(
                    'fn'=>'tim',
                    'ln'=>'peterson',
                    't'=>'president',
                    'co'=>'sales automation support, inc.',
                    'a1'=>'17025 W rogers drive',
                    'a2'=>'suite 100',
                    'c'=>'new berlin',
                    's'=>'WI',
                    'z'=>'53151-2232');


                //Instantiate new class
                $tbs = new clsTinyButStrong;
                $tbs->NoErr = true;
                $tbs->Plugin(TBS_INSTALL,OPENTBS_PLUGIN);

                $tbs->LoadTemplate($componentSave);
                $tbs->MergeBlock('l',$l);
                $tbs->MergeBlock('u',$u);
                $mergeFile = 'components/merge.docx';
                $tbs->Show(OPENTBS_FILE, $mergeFile);
                chmod($mergeFile,0755);

                //Send the file off to get a PDF made
                //TODO: This task should be sent to a background process
                $pdffile = $this->createPDF('/'.$mergeFile,'components/pdf/',$conversionSave,'493212372');
                $compDocModel->pdf_url = $pdffile;
                $compDocModel->save();
                unlink($mergeFile);

				$this->redirect(array('view','id'=>$model->product_id));
		    }
        }

		$this->render('create',array(
			'model'=>$model,
            'product'=>$product,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Component']))
		{
			$model->attributes=$_POST['Component'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Component');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Component('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Component']))
			$model->attributes=$_GET['Component'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Component::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='component-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
