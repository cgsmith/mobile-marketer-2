<?php

class ApiController extends Controller {
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */

    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format (XML or JSON)
     */
    private $format = 'json';
    private $apiusername = null;
    private $apikey = null;
    private $user = null;

    public function filters() {

        return array(
            'authenticate + index, create, lookup, update, delete' // check to ensure a product is selected.
        );
    }

    public function filterAuthenticate($filterChain) {
        if (!$this->authenticate()) {
            $this->_sendResponse(401, CJSON::encode(array('error' => true, 'message' => 'API username and/or key is invalid')));
        }
        $filterChain->run();
    }

    public function actionIndex() {
        
    }

    private function processPhpUserAuth() {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header('WWW-Authenticate: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            Yii::app()->end();
        } else {
            $this->apiusername = $_SERVER['PHP_AUTH_USER'];
            $this->apikey = $_SERVER['PHP_AUTH_PW'];
        }
    }

    // Actions
    public function actionList() {
        // Get the respective model instance
        switch ($_GET['model']) {
            case 'leads':
                $models = Lead::model()->findAll();
                break;
            default:
                // Model not implemented error
                $this->_sendResponse(501, sprintf(
                                'Error: Mode <b>list</b> is not implemented for model <b>%s</b>', $_GET['model']));
                Yii::app()->end();
        }
        // Did we get some results?
        if (empty($models)) {
            // No
            $this->_sendResponse(200, sprintf('No items where found for model <b>%s</b>', $_GET['model']));
        } else {
            // Prepare response
            $rows = array();
            foreach ($models as $model)
                $rows[] = $model->attributes;
            // Send the response
            $this->_sendResponse(200, CJSON::encode($rows));
        }
    }

    public function actionLookup() {
        $this->authenticate();
        // Check if id was submitted via GET
        if (!isset($_GET))
            $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => 'Parameters are missing')));

        $output = array();
        switch ($_GET['model']) {
            // Find respective model    
            case 'leads':
//                $model = Lead::model()->findByPk($_GET['id']);
//                $attributes = array('id'=>$_GET['id']);
                $attributes = array();
                $criteria = new CDbCriteria();
                $criteria->limit = 100;
                if (isset($_GET['email'])) {
                    $attributes = array_merge($attributes, array('email' => $_GET['email']));
                }
                if (isset($_GET['first'])) {
                    $attributes = array_merge($attributes, array('first' => $_GET['first']));
                }
                if (isset($_GET['last'])) {
                    $attributes = array_merge($attributes, array('last' => $_GET['last']));
                }
                if (isset($_GET['company'])) {
                    $attributes = array_merge($attributes, array('company' => $_GET['company']));
                }

                $isAdmin = $this->user->roles=='admin';
                if (!$isAdmin) {
                    $attributes = array_merge($attributes, array('customer_user_id' => $this->user->id));
                }
//                if(empty($attributes) && !$isAdmin){
////                    $this->_sendResponse(500, 'Error: Required Parameters are missing');
//                    $this->_sendResponse(500, CJSON::encode(array('error'=>true, 'message'=>'Required Parameters are missing')));
//                }
                if (isset($_GET['limit'])) {
                    if (is_numeric($_GET['limit'])) {
                        $criteria->limit = $_GET['limit'];
                    } else {
                        $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => 'Parameter limit needs numeric value.')));
                    }
                }
                if (!empty($attributes)) {
                    $criteria->addColumnCondition($attributes);
                }
                $model = Lead::model()->findAll($criteria);

                $output['count'] = count($model);
                $output['items'] = array();
                foreach ($model as $i => $m) {
                    $record = array();
                    $record['first'] = $m->first;
                    $record['last'] = $m->last;
                    $record['email'] = $m->email;
                    $record['title'] = $m->title;
                    $record['company'] = $m->company;
                    $record['address1'] = $m->address1;
                    $record['address2'] = $m->address2;
                    $record['city'] = $m->city;
                    $record['state'] = $m->state;
                    $record['zip'] = $m->zip;
                    $record['phone'] = $m->phone;
                    $record['fax'] = $m->fax;
                    $record['date_created'] = $m->date_created;
                    $record['date_updated'] = $m->date_updated;
                    $record['active'] = $m->active;
                    $output['items'][] = $record;
                }
                break;
            case 'production':
                $isAdmin = $this->user->roles=='admin';
                if (!$isAdmin) {
//                    $attributes = array_merge($attributes, array('customer_user_id' => $this->user->id));
                    $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => 'Access not allowed.')));
                } else {
                    $date_from = '';
                    $date_to = '';
                    if (isset($_GET['Date'])){
                        if(!empty($_GET['start_date'])){
                            $date['from'] = $_GET['start_date'];
                            $date_from = date('Y-m-d', strtotime($date['from']));
                        }
                        if(!empty($_GET['end_date'])){
                            $date['to'] = $_GET['end_date'];
                            $date_to = date('Y-m-d', (strtotime($date['to'])+86400) );
                        }
                    }
                    $filter = isset($_GET['filter'])? $_GET['filter']: Production::NOT_PRINTED;
                    $records = Production::getComponentsOrdered($date_from, $date_to, $filter);
                    $output['count'] = count($records);
                    $output['items'] = array();
                    foreach($records as $r){
                        $record = array();
                        $record["order_id"]=$r['id'];
                        $record["component"]= $r['component_name'];
                        $record["component_type"]= $r['component_type'];
                        $record["sequence"]= $r['component_sequence'];
                        $record["product"]=$r['product_name'];
                        $record["product_type"]=$r['product_type'];
                        $record["confirmation_number"]= $r['confirmation_number'];
                        $record["customer"]=$r['customer_name'];
                        $record["user"]= $r['customer_user_firstname'] . ' ' . $r['customer_user_lastname'];
                        $record["lead"]= $r['lead_first_name'] . ' ' . $r['lead_last_name'];
                        $record["lead_company"]= $r['lead_company'];
                        $record["production_date"]=$r['production_date_timestamp'];
//                        $record["status"]= $r['active'];
                        $output['items'][]=$record;
                    }
                }
                break;
            default:
                $this->invalidRequest();
        }
        // Did we find the requested model? If not, raise an error
        if (is_null($output['count']==0))
            $this->_sendResponse(404, CJSON::encode(array('error' => true, 'message' => 'No Item found')));
        else
//            $this->_sendResponse(200, CJSON::encode($model));
            $this->_sendResponse(200, CJSON::encode($output));
    }

    public function actionCreate() {
        switch ($_GET['model']) {
            // Get an instance of the respective model
            case 'leads':
                $model = new Lead('create');
                break;
            case 'production':
                $model = new Order();
                break;
            default:
                $this->invalidRequest();
        }
        // Try to assign POST values to attributes
        foreach ($_POST as $var => $value) {
            // Does the model have this attribute? If not raise an error
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $var, $_GET['model']))));
        }

        // Try to save the model
        if ($model->save())
            $this->_sendResponse(200, CJSON::encode($model));
        else {
            // Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
            $msg .= "<ul>";
            foreach ($model->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => $msg)));
        }
    }

    public function actionUpdate() {
        switch ($_GET['model']) {
            // Get an instance of the respective model
            case 'leads':
//                $model = new Lead('update');
                $model = Lead::model()->findByPk($_GET['id']);
                break;
//            case 'production':
//                $model = new Order();
//                break;
            default:
                $this->invalidRequest();
        }
        // Try to assign POST values to attributes
        foreach ($_POST as $var => $value) {
            // Does the model have this attribute? If not raise an error
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $var, $_GET['model']))));
        }
        
        if($model==null){
            $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => Yii::t('core', 'Lead Not found'))));
        }
        // Try to save the model
        if($this->user->roles=='admin' || $this->isLeadbelongsToUser($model->id)){
            if ($model->update()){
                $this->_sendResponse(200, CJSON::encode($model));
            } else {
                // Errors occurred
                $msg = "<h1>Error</h1>";
                $msg .= sprintf("Couldn't update model <b>%s</b>", $_GET['model']);
                $msg .= "<ul>";
                foreach ($model->errors as $attribute => $attr_errors) {
                    $msg .= "<li>Attribute: $attribute</li>";
                    $msg .= "<ul>";
                    foreach ($attr_errors as $attr_error)
                        $msg .= "<li>$attr_error</li>";
                    $msg .= "</ul>";
                }
                $msg .= "</ul>";
                $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => $msg)));
            }
        } else {
            $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => Yii::t('core', 'Not authorized'))));
        }
    }

    public function actionDelete() {
        if(!isset($_POST['id'])|| empty($_POST['id'])){
            $this->requiredParamatersMissing(array('id'));
        }
        $isAdmin = $this->user->roles=='admin';
        $attributes = array('id'=>$_POST['id'], 'active'=>1);
        if(!$isAdmin){
            array_merge($attributes, array('customer_user_id'=>$this->user->id));
        }
        
        switch($_GET['model']) {
            // Load the respective model
            case 'leads':
                $model = Lead::model()->findByAttributes($attributes);
                break;
            case 'production':
                $model = Order::model()->findByAttributes($attributes);
                break;
            default:
                $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => sprintf('Mode <b>delete</b> is not implemented for model <b>%s</b>',$_GET['model']) )));
                Yii::app()->end();
        }
        // Was a model found? If not, raise an error
        if($model === null)
            $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => sprintf("Didn't find any model lead <b>%s</b> with ID <b>%s</b>.",$_GET['model'], $_POST['id']) )));
        // Delete the model
//        $num = $model->delete();
        $model->active = 0; //  don't delete record but make it inactive.
        if($model->save())
            $this->_sendResponse(200, CJSON::encode(array('success'=>true, 'message'=>'Record Deleted Successfully.')));
        else
            $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => $model->errors.sprintf("Error: Couldn't delete model <b>%s</b> with ID <b>%s</b>.", $_GET['model'], $_POST['id']) )));
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'application/json') {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);
        // pages with body are easy
        if ($body != '') {
            // send the body
            echo $body;
        } else { // we need to create the body if none is passed
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
//            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
            // this should be templated in a real-world solution
            $body = array('error' => true, 'message' => $message);
            echo $body;
        }
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status) {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    private function authenticate() {
        $this->processPhpUserAuth();
//        $user=User::model()->find('LOWER(email)=:apiusername AND password=:apikey',array(':apiusername'=>strtolower($this->apiusername), ':apikey'=>$this->apikey));
        if (empty($this->apikey) || empty($this->apiusername)) {
            $this->_sendResponse(401, CJSON::encode(array('error' => true, 'message' => 'API username and or key is invalid')));
        }

//        $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
        $this->user = User::model()->findByAttributes(array('api_username' => $this->apiusername, 'api_key' => $this->apikey, 'active' => '1'));
        if ($this->user !== null) {
//            if($ph->CheckPassword($this->apikey,$this->user->password)){
            return true;
//            }
        }
        return false;
    }

    private function invalidRequest() {
        $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => 'Invalid Request')));
        Yii::app()->end();
    }

    protected function beforeAction($action) {
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute) {
                $route->enabled = false;
            }
        }
        return true;
    }
    
    protected function requiredParamatersMissing($params = array()){
        $parameters = '';
        foreach($params as $i => $param){
            $parameters .= ($i+1).': '.$param;
        }
        $this->_sendResponse(500, CJSON::encode(array('error' => true, 'message' => 'Error: Required Parameters '.$parameters.' are missing')));
    }
    
    protected function isLeadbelongsToUser($lead_id){
        $sql = "SELECT * FROM customer_lead WHERE customer_user_id=:userId AND id=:leadId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(':leadId', $lead_id, PDO::PARAM_INT);
        $command->bindValue(':userId', $this->user->id, PDO::PARAM_INT);
        return $command->execute()==1;
    }

}
