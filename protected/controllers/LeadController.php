<?php

class LeadController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow authenticated users to perform the following actions
				'actions'=>array('createSampleLead','import','lookup','index','view','create','update', 'copy','delete','history', 'activeCampaigns','export'),
				'roles'=>array('user','manager','admin'),
			),
			array('allow', // allow admin user to perform 'admin' actions
				'actions'=>array('admin'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    /*
     * Lookup/search for leads before ordering.  The 'keywords' are OR logic while everything else is AND
     */
    public function actionLookup()
    {
        $model = new Lead('search'); //Use 'search' scenario
        $model->in_campaign=null; //Null so radio button on form is not selected by default
        $model->been_in_campaign=null; //Null so radio button on form is not selected by default
//        Keyword::model()->findAll(
//                array(
//                    'condition'=>'customer_main_id=:id',
//                    'params'=>array(':id'=>Yii::app()->user->customer_id)
//                )
//            );
        if(isset($_GET['Lead'])) {
            $criteria = new CDbCriteria();
            foreach ($_GET['Lead'] as $key => $val){
                if ($val != ''){
                    if ($key === 'keywords'){ //'keywords' need to be matched like %"4"% due to storing JSON in column
                        foreach ($val as $json){
                            $criteria->addSearchCondition($key,'"'.$json.'"',true,'OR');
                        }
                    } else {
						if ('first' == $key || 'last' == $key) {
							$criteria->addCondition($key ." LIKE '". $val ."%'");
						}else{
							$criteria->addSearchCondition($key,$val); //defaults to AND logic
						}
                    }
                }
            }
            //Ensure we only search for user's leads
            $criteria->addSearchCondition('customer_user_id',Yii::app()->user->id,false,'AND','=');
            $dataProvider=new CActiveDataProvider('Lead',array(
                'pagination'=>array(
                    'pageSize'=>Yii::app()->user->pagination,
                ),
                'criteria'=>$criteria));
            $this->render('index',array(
                'dataProvider'=>$dataProvider,
            ));
        } else {
            $this->render('lookup',array(
                'model'=>$model,
            ));
        }
    }

    /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

        // Ensure the user can view the specified ID, if not, throw a 401, log error
        // TODO: Possibly change this to authenticate on the Yii->app()->user->check() level
        if ($this->loadModel($id)->customer_user_id !== Yii::app()->user->id){
            Yii::log('User lead request ID mismatch.  UserID('.Yii::app()->user->id
                .') ModelID('.$this->loadModel($id)->customer_user_id.')','warning','custom');
            throw new CHttpException(401,'Authentication has failed for the request. The failure has been logged.');
        }

        $model = $this->loadModel($id);
        if ($model->keywords !== null){
            $in = null;
            foreach (CJSON::decode($model->keywords) as $keyword){
            $in .= $keyword.',';
        }
        $in = rtrim($in,",");
        $sql = "SELECT keyword FROM customer_keyword WHERE id IN ($in)";
            $model->keywords = null;
            foreach (Yii::app()->db->createCommand($sql)->queryAll() as $keyword){
                $model->keywords .= $keyword['keyword'].', ';
            }

            $model->keywords = rtrim($model->keywords,', ');

        }
		$this->render('view',array(
			'model'=>$model,
		));
	}


	public function actionHistory($id)
	{

        // Ensure the user can view the specified ID, if not, throw a 401, log error
        // TODO: Possibly change this to authenticate on the Yii->app()->user->check() level
        if ($this->loadModel($id)->customer_user_id !== Yii::app()->user->id){
            Yii::log('User lead request ID mismatch.  UserID('.Yii::app()->user->id
                .') ModelID('.$this->loadModel($id)->customer_user_id.')','warning','custom');
            throw new CHttpException(401,'Authentication has failed for the request. The failure has been logged.');
        }

   /*     SELECT o.confirmation_number,o.date_ordered,op.date_printed,p.name,c.name FROM `order_leads` AS ol
LEFT JOIN `order` AS o
 ON o.id = ol.order_id
LEFT JOIN order_printed AS op
 ON op.order_id = o.id
LEFT JOIN product AS p
 ON o.product_id = p.id
LEFT JOIN component AS c
 ON op.component_id = c.id
WHERE ol.lead_id = 1
ORDER BY ol.order_id DESC*/
        $rawHistory = Yii::app()->db->createCommand()
            ->select('o.confirmation_number,o.date_ordered,op.date_printed,p.name AS prod,c.name AS comp')
            ->from('order_leads AS ol')
            ->leftJoin('order AS o','o.id = ol.order_id')
            ->leftJoin('order_printed AS op','op.order_id = o.id')
            ->leftJoin('product AS p','o.product_id = p.id')
            ->leftJoin('component AS c','op.component_id = c.id')
            ->where('ol.lead_id=:id', array(':id'=>$id))
            ->queryAll();
        $history = new CArrayDataProvider($rawHistory, array(
            'keyField'=>'confirmation_number',
            'sort'=>array(
                'attributes'=>array(
                    'confirmation_number','date_ordered','date_printed','prod','comp'
                ),
            ),
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
        $model = $this->loadModel($id);
		$this->render('history',array(
			'history'=>$history,
            'model'=>$model,
		));
	}
	
	/**
	 * 
	 * @param integer $id the ID of the lead
	 * @throws CHttpException
	 */
	public function actionActiveCampaigns($id){
		// Ensure the user can view the specified ID, if not, throw a 401, log error
		// TODO: Possibly change this to authenticate on the Yii->app()->user->check() level
		if ($this->loadModel($id)->customer_user_id !== Yii::app()->user->id){
			Yii::log('User lead request ID mismatch.  UserID('.Yii::app()->user->id
			.') ModelID('.$this->loadModel($id)->customer_user_id.')','warning','custom');
			throw new CHttpException(401,'Authentication has failed for the request. The failure has been logged.');
		}

		/*     SELECT * -- o.confirmation_number,o.date_ordered,op.date_printed,p.name,c.name
        FROM `order_leads` AS ol
		LEFT JOIN `order` AS o
		ON o.id = ol.order_id
		-- LEFT JOIN order_printed AS op
		-- ON op.order_id = o.id
		-- LEFT JOIN product AS p
		-- ON o.product_id = p.id
		WHERE ol.lead_id = 9395 AND ol.active = 1
		ORDER BY ol.order_id DESC*/
		$rawHistory = Yii::app()->db->createCommand()
			->select('o.id as order_id, o.confirmation_number,o.date_ordered,p.name AS prod')
			->from('order_leads AS ol')
			->leftJoin('order AS o','o.id = ol.order_id')
			->leftJoin('product AS p','o.product_id = p.id')
			->where('ol.lead_id=:id  and ol.active = 1 and o.active = 1', array(':id'=>$id))
			->queryAll();
		$history = new CArrayDataProvider($rawHistory, array(
				'keyField'=>'confirmation_number',
				'sort'=>array(
						'attributes'=>array(
								'confirmation_number','date_ordered','date_printed','prod','comp'
						),
				),
				'pagination'=>array(
						'pageSize'=>100,
				),
		));
		$model = $this->loadModel($id);
		$this->render('activeCampaigns',array(
				'history'=>$history,
				'model'=>$model,
		));
	}

    public function actionExport()
    {
        //Get active leads for export
        $criteria = new CDbCriteria();
        $criteria->addCondition('customer_user_id='.Yii::app()->user->id.' AND active=1');
        $leads = Lead::model()->findAll($criteria);

        //Stream leads and export data
        $fp = fopen('php://temp', 'w');

        // Write a header of csv file
        $headers = array('id','first','last','title','company',
            'address1','address2','city','state','zip','phone',
            'mobile','fax','email','date_created','date_updated',
            'notes',
        );
        $row = array();
        foreach($headers as $header) {
            $row[] = Lead::model()->getAttributeLabel($header);
        }
        fputcsv($fp,$row);


        /*
         * Get models, write to a file, then change page and re-init DataProvider
         * with next page and repeat writing again
         */

            foreach($leads as $model) {
                $row = array();
                foreach($headers as $head) {
                    $row[] = CHtml::value($model,$head);
                }
                fputcsv($fp,$row);
            }
            unset($model,$leads);

        /*
         * save csv content to a Session
         */
        rewind($fp);
        Yii::app()->user->setState('export',stream_get_contents($fp));
        Yii::app()->request->sendFile('export.csv',Yii::app()->user->getState('export'));
        fclose($fp);
    }

    public function actionImport() {
        $model = new LeadImport();

        if(isset($_POST['LeadImport'])) {
            $lead_model = new Lead();
            $model->attributes=$_POST['LeadImport'];
            if($model->validate()){
                $model->file=CUploadedFile::getInstance($model,'file');
                if($model->file!=NULL){
                    $time = time();
                    $date = date('Y-m-d H:i:s', $time);
                    $appendNotes = date('ymd').' Imported';
                    $user_id = Yii::app()->user->id;
                    $customer_id = Yii::app()->user->customer_id;
                    $file_name = 'imports/user-'.$user_id.'-'.$time.'.csv';
                    
                    $model->file->saveAs($file_name);
                    chmod($file_name,0755);
                    
                    $h = fopen($file_name,'r');
                    
                    if ($h !== FALSE) {
                        $fields_list = array();
                        $fields = fgetcsv($h, 1000, ',','"');
                        foreach($fields as $i => $field){
                            if(!$lead_model->hasAttribute($field)){
                                $model->addError('file', 'Invalid attribute "'.$field.'" found in CSV file.');
                            } else {
                                $fields_list[$i] = $field;
                            }
                        }
//                        echo '<pre>';
//                        $field_count = count($fields_list);
                        $lead_model_array = array();
                        $emails = array();
                        // now get the values;
                        $row_count = 1;
                        while(($data = fgetcsv($h, 1000, ',','"')) !== FALSE) {
//                            print_r($data);
//                            $num = count($data);
                            $l = new Lead();
                            foreach($fields_list as $i => $field) {
                                if($field=='keywords'){
//                                    echo $data[$i];
                                    if(isset($data[$i]) && $data[$i] !== ''){
                                        $keywords_array = explode(',', $data[$i]);
                                        $data[$i] = $model->lookup_keywords($keywords_array);
                                        /*foreach($keywords_array as $keyword){
                                            if ($keyword !== ''){
                                                if(!$model->isKeywordbelongsToCustomer($keyword)){
                                                    $model->addError('file', 'Invalid keyword id "'.$keyword.'" ('.gettype($keyword).') added at row '. $row_count);
                                                }
                                            }
                                        }
                                        $data[$i] = json_encode($keywords_array);*/
                                    } else {
                                        $data[$i] = null;
                                    }
                                }
                                if ($field == 'email' && $data[$i] == '') {
                                    $l->$field = null;
                                }else{
                                    $l->$field = $data[$i];
                                }
                            }
                            $l->customer_user_id = $user_id;
                            $l->date_created = $l->date_updated = $date;
                            $l->customer_main_id = $customer_id;
                            $l->notes = $l->notes . $appendNotes;
                            $l->active = 1;

                            if ($l->email !== null) {
                                if(in_array($l->email, $emails)){
                                    $model->addError('file', 'Duplicate Email "'.$l->email.'" found at row '. $row_count);
                                } else {
                                    $emails[] = $l->email;
                                }
                            }

                            // Process keywords
                                
                            
                            if(!$l->validate()){
                                $errors = $l->getErrors();
                                //error_details = '<ul>';
                                $error_details = '';
                                foreach($errors as $error){
                                    $error_text = '';
                                    foreach($error as $er){
                                        $error_text .= $er;
                                    }
                                    echo $error_details .= '<li>Row '.$row_count.': '.$error_text.'</li>';
                                }
                                //$error_details .= '</ul>';
                                $model->addError('file', $error_details);
                            }
                            $lead_model_array[] = $l;
                            $row_count++;
                        }
                        if(!$model->hasErrors()){
//                            $connection = $lead_model->dbConnection->beginTransaction();
                            foreach($lead_model_array as $lead){
                                $lead->save(false);
                            }
//                            $connection->commit();
                            Yii::app()->user->setFlash('success',Yii::t('core', 'The leads have been successfully uploaded.'));
                            $this->redirect(array('lead/index'));
                        }
                    }
                    fclose($h);
//                    Yii::app()->getUser()->setFlash('error','Feature not implemented yet.');
                }
            }
                
        }

        $this->render('import',array(
            'model'=>$model
        ));
    }

	// Creates a sample lead with default attributes
	public function createSampleLead($attributes)
	{
		$model = new Lead('create');

		$model->attributes = $attributes;

		$model->save();
		return;
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Lead('create');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Lead']))
		{
			$model->attributes=$_POST['Lead'];
            $model->setAttribute('customer_main_id',Yii::app()->user->customer_id);
            $model->setAttribute('customer_user_id',Yii::app()->user->id);

            if (isset($_POST['keywords'])){
                $model->setAttribute('keywords',CJSON::encode($_POST['keywords'])); //Insert JSON keywords into DB
            } else {
                $model->setAttribute('keywords',null);
            }

			if($model->save())
				$this->redirect(array('lead/view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * 
	 * @param integer $id
	 */
	public function actionCopy($id){
		$model=$this->loadModel($id);
		
		$lead = new Lead();
		$lead->attributes = $model->attributes;
		
		$lead->id = null;
		$lead->first = "";
		$lead->last = "";
		$lead->email = "";
		
		if($lead->save())
			$this->redirect(array('update', 'id'=>$lead->id));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model->scenario='create';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Lead']))
		{
			$model->attributes=$_POST['Lead'];
            $model->setAttribute('date_updated',date('Y-m-d H:i:s')); //Create datetime for database field
            if (isset($_POST['keywords'])){
                $model->setAttribute('keywords',CJSON::encode($_POST['keywords'])); //Insert JSON keywords into DB
            } else {
                $model->setAttribute('keywords',null);
            }
			if($model->save()){
                Yii::app()->getUser()->setFlash('success','Lead successfully updated.'); //Display flash on success
				$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        // we only allow deletion via POST request
        $model=$this->loadModel($id);
        $model->setAttribute('active',false); // set active to false (essentially deleting the lead)
        $model->setAttribute('date_updated',date('Y-m-d H:i:s')); // create datetime for database field
        if ($model->save()) {
            $sql = "UPDATE `order_leads` SET `active`=0 WHERE `lead_id` = :lid";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(':lid',$id,PDO::PARAM_INT);
            $command->execute();
            Yii::app()->getUser()->setFlash('success','Lead removed successfully along with any active orders.');
        } else {
            Yii::app()->getUser()->setFlash('error','Lead could not be removed.');
            //throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }

        $this->redirect(array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $dataProvider=new CActiveDataProvider('Lead',array(
                'pagination'=>array(
                    'pageSize'=>Yii::app()->user->pagination,
                ),
                'criteria'=>array(
                    'condition'=>'customer_user_id=:user_id AND active=1',
                    'params' => array(':user_id'=>Yii::app()->user->id)
                ),
            ));
            $this->render('index',array(
                'dataProvider'=>$dataProvider,
            ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Lead('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Lead']))
			$model->attributes=$_GET['Lead'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Lead::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='lead-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}