<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {

        return array(
            array('allow',  // allow authenticated users to perform the following actions
                'actions'=>array('reports','profile','impersonateCancel'),
                'roles'=>array('user','manager','admin','prod'),
            ),
            array('allow',
                'actions'=>array('impersonate'),
                'roles'=>array('admin', 'manager'),
            ),
            array('deny',
                'actions'=>array('reports','profile','impersonate'),
                'users'=>array('*'),
            ),
        );
    }

    /**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $mobile = new Mobile_Detect;
        if (Yii::app()->user->isGuest && $mobile->isMobile()) 
            $this->redirect(Yii::app()->createUrl('site/login'));
		$this->render('index');
	}

	/**
	 * Renders report file for reporting about lead generation
	 */
	public function actionReports()
	{
		$this->render('reports');
	}

	/**
	 * Renders profile for user updating.  Only updates the user profile, does not update any
     * company information
	 */
	public function actionProfile($id)
	{
        $model=User::model()->findByPk($id);

        //Check if the form has been submitted.
        if(isset($_POST['User'])) {
            $model->attributes=$_POST['User'];
            if ($_POST['User']['password1'] !== '') {  //If user has something entered in the password field, change pw
                $model->setScenario('changePassword');
            }

            // Validate and then save the model
            if ($model->validate()) {

                $headshot_file  = CUploadedFile::getInstance($model,'headshot_file');
                $signature_file = CUploadedFile::getInstance($model,'signature_file');
                if (!empty($headshot_file)){
                    $headshotSave = 'images/headshots/'.Yii::app()->user->id.'-'.time().'.'.$headshot_file->getExtensionName();
                    $headshot_file->saveAs($headshotSave);
                    $model->headshot_file =$headshotSave;
                } else{
                    unset($model->headshot_file);
                }
                if (!empty($signature_file)){
                    $signatureSave = 'images/signatures/'.Yii::app()->user->id.'-'.time().'.'.$signature_file->getExtensionName();
                    $signature_file->saveAs($signatureSave);
                    $model->signature_file =$signatureSave;
                }else {
                    unset($model->signature_file);
                }

                if ($model->save(false)) {
                    Yii::app()->user->setState('pagination',$model->pagination);
                    Yii::app()->getUser()->setFlash('success','User profile successfully updated.');
                } else {
                    Yii::app()->getUser()->setFlash('error','User profile did not save!');
                }
                $this->redirect(array('site/profile', 'id'=>Yii::app()->user->id));
            }
        }


		$this->render('profile',array(
            'model'=>$model,
        ));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

    public function actionImpersonate($id,$cancel=false)
    {
        $ui = UserIdentity::impersonate($id,$cancel);
        if($ui)
            Yii::app()->user->login($ui, 0);
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionImpersonateCancel($cancel=true)
    {
        $ui = UserIdentity::impersonate(Yii::app()->user->originalId,$cancel);
        if($ui)
            Yii::app()->user->login($ui, 0);
        $this->redirect(Yii::app()->homeUrl);
    }

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
                    $model->attributes=$_POST['LoginForm'];
                    // validate user input and redirect to the previous page if valid
                    if($model->validate() && $model->login())
                    if (Yii::app()->user->start_page !== null)
                    {
                        $this->redirect(array(Yii::app()->user->start_page));
                    } else {
                        $this->redirect(Yii::app()->user->returnUrl);
                    }
                }
		// display the login form
		$this->render('login',array('model'=>$model));
	}
        
	public function actionResetpassword()
	{
		$model=new ResetpasswordForm;
                
		// collect user input data
		if(isset($_POST['ResetpasswordForm']))
		{
                    $model->attributes=$_POST['ResetpasswordForm'];
                    // validate user input and redirect to the previous page if valid
                    if($model->validate()){
                        $model->saveResetPassword();
                        $model->sendEmail();
                        Yii::app()->user->setFlash('success',Yii::t('core', 'A password reset has been sent to your email address.'));
                        $this->redirect(array('site/login'));
                    } else {
                        $model->clearErrors();
                        Yii::app()->user->setFlash('error',Yii::t('core', 'The user with the specified email does not exists. Please try again.'));
                    }
                }
		// display the login form
		$this->render('reset-password',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('site/login','logout'=>'1'));
	}

}