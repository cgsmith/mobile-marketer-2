<?php

class OrdersController extends Controller
{
    public function actionVerify()
    {
        if(!isset($_REQUEST['selected'])){
            throw new CHttpException(412,'Leads not provided.');
        }
        $selected = json_decode(html_entity_decode($_REQUEST['selected']));
        $order_customer_leads_list = new CArrayDataProvider(Customer::getCustomerLeads($selected));
        $product = Product::model()->findByPk($_REQUEST['product_id']);
        error_reporting(7);
        $this->render('verify',array(
            'order_customer_leads_list' => $order_customer_leads_list,
            'product' => $product,
        ));
    }
    
    public function actionConfirm(){
        if(isset($_POST['order_product_id']) && isset($_POST['order_selected'])) {
            if(!isset($_POST['order_product_id']) || !isset($_POST['order_selected'])){
                throw new CHttpException(412,'Required Product and/or leads not provided.');
            }
            $product_id = $_POST['order_product_id'];
            $selected_leads = json_decode($_POST['order_selected']);
            $selected_leads_list = Customer::getCustomerLeads($selected_leads);
            
            if(count($selected_leads_list)>0){
                $order_model = new Order();
                $order_model->active = true;
                $order_model->date_ordered = date('Y-m-d H:i:s',time());
                $order_model->product_id = $product_id;
                $order_model->customer_user_id = yii::app()->user->id;
                if($order_model->save()){
                    $order_id = $order_model->getPrimaryKey();
                    $order_model->confirmation_number = Yii::app()->params['confirmation']['prefix'] . $order_model->id;
                     //Update the record with confirmation_number.
//                    $order_model->setAttribute('confirmation_number',Yii::app()->params['confirmation']['prefix'] . $order_model->id);
                    $order_model->save();
                    
                    foreach($selected_leads_list as $lead){
                        $model = new OrderLeads();
                        $model->order_id = $order_id;
                        $model->lead_id = $lead['id'];
                        $model->active = true;
                        if(!$model->save()) {
                            throw new CHttpException(412,'Order Leads table cannot save');
                        }

                        $leadModel = Lead::model()->findByPk($lead['id']);
                        $leadModel->in_campaign = true;
                        $leadModel->been_in_campaign = true;
                        if(!$leadModel->save()) {
                            throw new CHttpException(412,'Cannot modify lead during order creation');
                        }
                        unset($leadModel);

                    }
                    Yii::app()->user->setFlash('success','Your order has been placed successfully.');
                    $this->redirect(array('confirmation','id'=>$order_model->id));
                } else {
                    Yii::app()->user->setFlash('error','Errors occurred while processing your order.  Please try again.');
                }
                    
            }
        }
        $url = $this->createUrl('lead/index');
        $this->redirect($url);
    }

    /**
     * Displays products available for purchase
     */
    public function actionCreate()
    {
        if (isset($_POST['email-blast'])) {
            $model = new EmailBlast;
            $this->render('email',array(
                'model'=>$model,
            ));
        }else{
            //Array of products [productName, productID, ProductType]
            $products = Yii::app()->db->createCommand()
                ->select('product.id,product.product_type_id,product.name,product_type.type')
                ->from('product')
                ->where('customer_main_id=:cid AND active=:active',array(':cid'=>Yii::app()->user->customer_id,':active'=>1))
                ->join('product_type','product.product_type_id=product_type.id')
                ->order('product.product_type_id ASC, product.name ASC')
                ->queryAll();

            $this->render('create',array(
                'products'=>$products,
            ));
        }
    }
    
    public function actionIndex() {
//        $count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM `order_leads` ol LEFT OUTER JOIN `order` o ON o.id = ol.order_id WHERE o.active=1 AND o.customer_user_id='.Yii::app()->user->id)->queryScalar();
//        
//        $sql = "SELECT * 
//                FROM 
//                    order_leads ol 
//                    LEFT OUTER JOIN `order` o
//                    ON ol.order_id = o.id 
//                    LEFT OUTER JOIN customer_lead cl 
//                    ON ol.lead_id = cl.id 
//                WHERE 
//                    cl.customer_user_id=:customerUserId AND o.active=:active";
//        $dataProvider= new CSqlDataProvider($sql,array(
//            'params'=>array(
//                ':customerUserId'=>Yii::app()->user->id,
//                ':active'=>true,
//            ),
//            'totalItemCount'=>$count,
//        ));
        
        $date_from = '';
        $date_to = '';
                
        if (isset($_GET['Date'])){
            if(!empty($_GET['Date']['from'])){
                $date['from'] = $_GET['Date']['from'];
                $date_from = date('Y-m-d', strtotime($date['from']));
//                $criteria->compare('date_ordered', '>='.$date_from);
            }
            if(!empty($_GET['Date']['to'])){
                $date['to'] = $_GET['Date']['to'];
                $date_to = date('Y-m-d', (strtotime($date['to'])+86400) );
//                $criteria->compare('date_ordered', '<'.$date_to);
            }
        }
        
        $filter = isset($_GET['filter'])? $_GET['filter']: Production::NOT_PRINTED;
        $list_data = Production::getComponentsOrdered($date_from, $date_to, $filter);
        $dataProvider = new CArrayDataProvider($list_data, array(
            'keyField' => 'id',
            'id' => 'id',
            'pagination' => false,
            'sort'=>array(
                'attributes'=>array(
                     'id','product_name','product_type','component_name','component_type','date_ordered','customer_name','user','date_production', 'component_sequence', 'lead_first_name','lead_last_name'
                ),
            ),
        ));
        
        $this->render('index',array(
            'dataProvider'=>  $dataProvider,
            'production_model' => new Production(),
        ));
    }

    public function actionConfirmation($id)
    {
        $this->render('confirmation',array(
            'confirmation'=>$id
        ));
    }
    
    public function actionCancelOrder(){
    	$leadId = Yii::app()->request->getParam('leadId');
    	$orderId = Yii::app()->request->getParam('orderId');
    	if( !empty($leadId) && !empty($orderId)){
    		$orderLead = OrderLeads::model()->find('lead_id=:lead_id AND order_id=:order_id', array('lead_id'=>$leadId, 'order_id'=>$orderId));
    		$orderLead->active = 0;
    		if($orderLead->save())
    			echo CJSON::encode(array('status'=>'success', ));
    		else 
    			echo CJSON::encode(array('status'=>'failure', ));
    	}
    	
    	Yii::app()->end();
    }
    
    public function actionCancel(){
    	if(!empty($_POST['txt-order-number'])){
    		$orderNumber = $_POST['txt-order-number'];
    		
    		$order = Order::model()->findByPk($orderNumber);
    		
    		if($order->active==0)
    			Yii::app()->user->setFlash('warning', '<strong>Inactive Order!</strong> Order <strong>'.$order->id.'</strong> is already inactive.');
    		else {
    			$order->active = 0;
    			if($order->save())
    				Yii::app()->user->setFlash('success', '<strong>Order Canceled!</strong> Order <strong>.$order->id.</strong> has been canceled.');
    			else
    				Yii::app()->user->setFlash('error', '<strong>Error!</strong> While trying to update Order\'s status.');
    		}
    	}
    	$this->render('cancel');
    }

    /**
     * return list of active campaigns user has
     */
    public function actionCampaigns() {

    }

    protected function createTable($name)
    {
        return "<table><th style='width: 400px'><strong>$name</strong></th><th></th>";
    }

    protected function addTable($product,$blank=null)
    {
        $html = null;
        $product['onclickid'] = 'orderenable';
        if ($blank) {
            $product['id'] = 0;
            $product['name'] = 'None';
            $product['onclickid'] = 'orderdisable';
        }

        $inputType =  (in_array($product['product_type_id'],Order::model()->checkBoxes)) ? 'checkbox' : 'radio' ;
        if (!(($inputType === 'checkbox') && ($product['name'] === 'None'))) {
            $html = "<tr>
                <td><input type='".$inputType."' class='".$product['onclickid']."' name='product_id' value='".$product['id']."'/> ".$product['name']." </td>
                <td>".$this->gridPreviewButton($product)."</td>
                </tr>";
        }
        return $html;
    }

    protected function endTable()
    {
        return "</table>";
    }

    protected function gridPreviewButton($data)
    {
        // ... generate the output for the column

        // Params:
        // $data ... the current row data
        // $row ... the row index
        $docs = Yii::app()->db->createCommand()
            ->select('id, document_id,component_type_id,name,description')
            ->from('component')
            ->where('product_id=:id',array(':id'=>$data['id']))
            ->queryAll();
        $list = array();
        foreach ($docs as $doc) {
            if ($doc['document_id'] > 0) {
                $comDoc = ComponentDocument::model()->findByPk($doc['document_id']); //TODO: should this be createCommand()?
                $list[] = array($doc['name'],$comDoc->pdf_url,$doc['description'], $doc['component_type_id'], $doc['id']);
            }
            if($doc['component_type_id']==Component::TYPE_EMAIL){
                $list[] = array($doc['name'],'',$doc['description'], $doc['component_type_id'], $doc['id']);
            }
        }

        $htmlItems=null;
        $noPreview = true;
        $type='';
        $label='No Preview';
        $icon=null;
        $onclick='';
        if (isset($list)){
            foreach ($list as $item) {
                if($item[3]==Component::TYPE_EMAIL){
                    $htmlItems .= '<a href=\"'.Yii::app()->baseUrl.'/product/component/email/'.$item[4].'\" target=\"_blank\">'.$item[0].': '.$item[2].'</a><br>';
                } else {
                    $htmlItems .= '<a href=\"'.Yii::app()->baseUrl.'/product/'.str_replace('components', 'component', $item[1]).'\" target=\"_blank\">'.$item[0].': '.$item[2].'</a><br>';
                }
                
                $noPreview = false;
                $type='primary';
                $label='Preview';
                $icon='search white';
            }
            $onclick ='js:bootbox.modal("'.$htmlItems.'", "Product Preview");';
        }

        $button = $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>$label,
            'size'=>'mini',
            'type'=>$type,
            'icon'=>$icon,

            'htmlOptions'=>array(
                'style'=>'margin-left:3px',
                'onclick'=>$onclick,
                'disabled'=>$noPreview,
            ),
        ),true);
        return $button;
    }
}
