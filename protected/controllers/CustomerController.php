<?php

class CustomerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view','create','update','createkeyword'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','active','createkeyword','deletekeyword'),
                'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            $model = $this->loadModel($id);
            if(!$model->isUserAllowed() && !Yii::app()->user->checkAccess('admin')){
                throw new CHttpException(401, Yii::t('core', 'You are not authorized to access this page.'));
            }
            $this->render('view',array(
                    'model'=>$model,
            ));
	}

    /**
     * @throws CHttpException
     */
    public function actionCreateKeyword()
    {
        if (isset($_POST['Keyword']))
        {
            if (strpos($_POST['Keyword']['keyword'],','))
            {
                $keywords = explode(',',$_POST['Keyword']['keyword']);
                foreach ($keywords as $keyword) {
                    $model=new Keyword;
                    $model->keyword=$keyword;
                    $model->customer_main_id=$_POST['customer_main_id'];
                    if(!$model->save()){throw new CHttpException(500,'Internal server error, failure to save keyword.');}
                }
            }
            else
            {
                $model=new Keyword;
                $model->attributes=$_POST['Keyword'];
                $model->customer_main_id=$_POST['customer_main_id'];
                if(!$model->save()){throw new CHttpException(500,'Internal server error, failure to save keyword.');}
            }
            Yii::app()->getUser()->setFlash('success','New keyword created successfully.');
            $this->redirect(array('view','id'=>$model->customer_main_id));
        }
    }

    /**
     * @param $id
     */
    public function actionDeleteKeyword($id)
    {
        $model= Keyword::model()->findByPk($id);
        $model->setAttribute('active',0); //set active to false

        if(!$model->save())
        {
            Yii::app()->getUser()->setFlash('error','Keyword failed to delete!');
        }else {
            Yii::app()->getUser()->setFlash('success','Keyword removed.');
        }
        $this->redirect(array('view','id'=>$model->customer_main_id));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            /*
             * 1. Create customer
             * 2. Create address(es) for customer with customer_main_id
             * 3. Create manager(s) for customer with customer_main_id
             *    Use main address as manager's address
             * 4. Send email to manager(s) with login information
             */
            $model=new Customer('insert');
            $address = new Address('insert');
            
            $all_submitted_users = array();
            $all_users_valid = true;
            $valid_users = array();
            
            if(isset($_POST['Customer'])) {
                //Create the main customer entry
                $model->attributes=$_POST['Customer'];
                $model->active = true; //Set active to true so the account is enabled on creation
    //			if(!$model->save()){throw new CHttpException(500,'Internal Server Error occurring at customer.');}
                $transaction=$model->dbConnection->beginTransaction();
//                if($model->save()){
                    try{
                        if(!$model->save()){
                            throw new Exception('Customer could not be saved.');
                        }
                        //Create the customer address entry
                        $address->attributes=$_POST['Address'];
                        $address->customer_main_id = $model->id; //Use newly generated ID for other forms
                        $address->billing = 1; //TODO: CHANGE SO BILLING OPTION IS ON THE FORM
                        $address->active = true; //Set address to active
                        if(!$address->save()){
                            throw new Exception('Customer address could not be saved.');
                        }


                        //Create the user entry
                        $all_users_valid = true;
                        $valid_users = array();
                        
                        foreach($_POST['User'] as $index => $submitted_user){
                            // Skip if email is empty
                            if($submitted_user['email']==''){
                                continue;
                            }
                            //Get random password first
                            $password = $this->getRandomPassword();
                            
                            $user = new User();
                            $user->attributes = $submitted_user;
                            $user->password1 = $password; //Both password1 and password2 have to be set for the hash to generate
                            $user->password = $password; //Both password1 and password2 have to be set for the hash to generate
                            $user->password2 = $password;
                            $user->address1 = $address->address1;
                            $user->address2 = $address->address2;
                            $user->city = $address->city;
                            $user->state = $address->state;
                            $user->zip = $address->zip;
                            $user->customer_main_id = $model->id;
                            $user->roles = 'manager';
                            $user->active = true;
                            
                            $all_submitted_users[] = $user;
                            if(!$user->validate()){
                                $all_users_valid = false;
                            } else {
                                $valid_users[] = $user;
                            }
                        }
                        if($all_users_valid && count($valid_users)>0){
                            foreach($valid_users as $user){
                                //TODO: SEND EMAILS THROUGH GEARMAN OR SIMILAR PROGRAM
                                //TODO: mobile and fax of user failed to set unsafe attribute.
                                $password = $user->password; //  get the password before its encrypted.
                                $user->save(false); // false means don't validate the data as we already performed validation.
                                $message = new YiiMailMessage;
								$message->view = 'email';
								$message->subject = "You are all set on Mobile Marketer! Let's sell together!";
								$email = "Hello!<br><br>
								We are glad to have you as part of <a href=\"https://www.mobilemarketer.net\">Mobile Marketer</a>!  Below is your username
								and temporary password to get started.<br><br>
								Username: {$user->email}<br>
								Password: {$password}<br>
								<br>
								Note: once you have logged in please change your password under the user profile screen.<br><br>
								<h5>Let's sell together!</h5>";
								$params = array('content' => $email);
                                $message->setBody($params, 'text/html');
                                $message->addTo($user->email);
                                $message->from = Yii::app()->params['adminEmail'];
                                Yii::app()->mail->send($message);
                            }
                        } else {
                            throw new Exception("Manager users fields need to filled properly.");
                        }
                        $transaction->commit();
                        Yii::app()->user->setFlash('success','New customer created successfully.');
                        $this->redirect(array('/customer/index'));
                    } catch(Exception $e){
                        $transaction->rollback();
                        Yii::log("Unable to save customer: ". $e->getMessage(), CLogger::LEVEL_ERROR, 'app.controllers.customer.create');
//                        Yii::app()->user->setFlash('error','Fields with * are required.');
                    }
//                }
            }
            $users = (count($all_submitted_users)>0 || isset($_POST['Customer'])) ? $all_submitted_users : array(new User('insert'));
            $this->render('create',array(
                'model'=>$model,
                'address'=>$address,
                'users' => $users,
            ));
	}
        
        protected function getRandomPassword(){
            $length = 10;
            $chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
            shuffle($chars);
            $password = implode(array_slice($chars, 0, $length));
            return $password;
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            if(!Yii::app()->user->checkAccess('admin')){
                throw new CHttpException(401);
            }
//            $model=$this->loadModel($id);
            $model = Customer::model()->with(array('users'=>array('condition'=>'users.active=1')),'addresses')->findByPk($id);
            if($model==null){
                throw new CHttpException(404, 'Not Found');
            }
            
            if($model===null) throw new CHttpException(404,'The requested page does not exist.');
            $users = $model->users;
            $addresses = $model->addresses;

            // Uncomment the following line if AJAX validation is needed
            $valid_users = array();
            $users_to_be_inserted = array();
            
            if(isset($_POST['Customer'])) {
                $addresses = $this->getAddressesToUpdate($model->id); // returns Address models
                $users = $this->getUsersToUpdate($model->id); // returns User models
                $model->attributes=$_POST['Customer'];
                
                $valid = true;
                $valid_users = true;
                $is_users_to_inserted_valid = true;
                
                if($model->validate()){
                    foreach ($addresses as $i => &$address){
                        if(isset($_POST['Address'][$i])){
                            $address->attributes=$_POST['Address'][$i];
                            $valid = $address->validate() && $valid;
                        }
                    }
                    if($valid){
                        foreach ($addresses as &$address){
                            $address->save();
                        }
                    }
                    
                    // process and save users
                    foreach ($users as $i => &$user){
                        if(isset($_POST['User'][$i])){
                            $user->attributes=$_POST['User'][$i];
                            $valid_users = $user->validate() && $valid_users;
                        }
                    }
                    if($valid_users){
                        foreach ($users as &$user){
                            $user->save();
                        }
                    }
                    
                    // For newly added users
                    $users_to_be_inserted = $this->getUsersToInsert($model->id, $address);
                    foreach ($users_to_be_inserted as $user) {
                        $is_users_to_inserted_valid = $user->validate() && $is_users_to_inserted_valid;
                    }
                    if($is_users_to_inserted_valid){
                        foreach ($users_to_be_inserted as $user) {
                            $user->save(false);
                        }
                        // all are saved make is empty array
                        $users_to_be_inserted = array();
                    }
                }
                if($valid && $valid_users && $is_users_to_inserted_valid && $model->save()){
                    $this->redirect(array('view','id'=>$model->id));
                }
            }

            $this->render('update',array(
                'model'=>$model,
                'users'=>$users,
                'address'=>$addresses,
                'newusers' => $users_to_be_inserted,
            ));                
	}
        
        protected function getAddressesToUpdate($customer_id){
            $criteria = new CDbCriteria();
            $ids = array();
            foreach($_POST['Address'] as $address){
                if(!isset($address['id'])){
                    continue;
                }
                $ids[] = $address['id'];
            }
            $criteria->addInCondition('id', $ids);
            $criteria->addColumnCondition(array('customer_main_id'=>$customer_id));
            $criteria->addColumnCondition(array('active'=>1));
//            if(!Yii::app()->user->checkAccess('admin')){
//                $criteria->addColumnCondition(array('customer_main_id'=>Yii::app()->user->id));
//            }
            $criteria->index = 'id';
            return Address::model()->findAll($criteria);
        }
        
        protected function getUsersToInsert($customer_id, $address){
            $users_to_be_inserted = array();
            foreach($_POST['User'] as $submitted_user){
                if(!isset($submitted_user['id'])){
                    $password = $this->getRandomPassword();
                    $user = new User();
                    $user->attributes = $submitted_user;
                    $user->password1 = $password; //Both password1 and password2 have to be set for the hash to generate
                    $user->password = $password; //Both password1 and password2 have to be set for the hash to generate
                    $user->password2 = $password;
                    $user->address1 = $address->address1;
                    $user->address2 = $address->address2;
                    $user->city = $address->city;
                    $user->state = $address->state;
                    $user->zip = $address->zip;
                    $user->customer_main_id = $customer_id;
                    $user->roles = 'manager';
                    $user->active = true;

                    $users_to_be_inserted[] = $user;
                }
            }
            
            return $users_to_be_inserted;
        }
        
        protected function getUsersToUpdate($customer_id){
            $criteria = new CDbCriteria();
            $ids = array();
            foreach($_POST['User'] as $user){
                if(!isset($user['id'])){
                    continue;
                }
                $ids[] = $user['id'];
            }
            $criteria->addInCondition('id', $ids);
            $criteria->addColumnCondition(array('customer_main_id'=>$customer_id));
            $criteria->addColumnCondition(array('active'=>1));
            $criteria->index = 'id';
            return User::model()->findAll($criteria);
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model=$this->loadModel($id);
        $model->setAttribute('active',0); // set active to false (essentially deleting)
        if ($model->save()) {
            Yii::app()->getUser()->setFlash('success','Customer set to inactive.');

        } else {
            Yii::app()->getUser()->setFlash('error','Customer could not be removed.');
        }

        $this->redirect(array('index'));
	}

    public function actionActive($id)
    {
        $model=$this->loadModel($id);
        $model->setAttribute('active',true);
        if ($model->save()) {
            Yii::app()->getUser()->setFlash('success','Customer set to active.');
        } else {
            Yii::app()->getUser()->setFlash('error','Customer could not be activated.');
        }

        $this->redirect(array('index'));
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $dataProvider= new CActiveDataProvider('Customer',array(
                'criteria'=>array(
                    'order'=>'name ASC',
                    //'with'=>array('addresses'),
                ),
                'pagination'=>false,
            ));

            $this->render('index',array(
                    'dataProvider'=>$dataProvider,
            ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Customer('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Customer']))
			$model->attributes=$_GET['Customer'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Customer::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='customer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
