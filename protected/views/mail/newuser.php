<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Mobile Marketer</title>
</head>

<body>
<div style = "width:600px;">

    <div style = "border:1px solid #060; padding:10px;">

        <div style = "padding:5px;"><a href = "http://www.mobilemarketer.net"><img src="http://www.salessupportcenter.com/images/MM_Logo_sm.png" border='0' /></a></div>

        <!-- start body -->
        <div>
            <h1>You're all setup!</h1>
            <div><?php echo $model->first_name;?>,<br/><br/>
            Feel free to login at http://beta.mobilemarketer.net with the following credentials:<br/>
            <br/>
            <strong>Username</strong> <?php echo $model->email;?><br/>
            <strong>Password</strong> <?php echo $password;?><br/>
            </div>
            <?php echo $content; ?>
        </div>
        <!-- end body -->

        <!-- start footer -->
        <div></div>
        <!-- end footer -->

    </div>

</div>
</body>
</html>
