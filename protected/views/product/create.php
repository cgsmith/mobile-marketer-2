<?php
$this->breadcrumbs=array(
	'Products'=>array('index'),
	'Create',
);

?>

<h1>Create Product</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>