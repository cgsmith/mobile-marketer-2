<?php
/* @var $this ComponentController */
$this->breadcrumbs=array(
    'Customers'=>array('/customer/index'),
    'Customer'=>array('/customer/'.$model->customer_main_id),
    $model->name=>array('/product/'.$model->id),
    'Components'
);
Yii::app()->clientScript->registerScript(
    'loadcomponent',
    '$("#product").on("change",function(e){
        $("#component-list").addClass("loading bgright norepeat");
        $.ajax("'.$this->createAbsoluteUrl("component/update").'", {
            "type":"get",
            data:{id:$(this).val(), ajax:"componentform"},
            dataType:"html",
            success:function(data){
                $("#component-details .form-wrapper").html(data);
                $("#Component_email_content").redactor();
            },
            complete:function(){
                $("#component-list").removeClass("loading");
                $("#component-list").removeClass("bgright");
            },
        });
    });',
    CClientScript::POS_READY
);

?>

<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
                <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
        <?php }
    }?>
</div>
<h2>Components for <?php echo $model->name; ?></h2>
<div clas="container-responsive">
    <div id="component-list" class="span-6">
        <h3>Component List</h3>
        <?php
        $components = $model->components;
            echo CHtml::dropDownList('product', '',
                CHtml::listData($components, 'id', function($components){
                    return CHtml::decode($components->sequence . ' - '. $components->name); }), array('size'=>10));
            //$model->components->sequence.' '.
//            echo CHtml::dropDownList('product', '', CHtml::listData($model->components, 'id', 'name'), array('size'=>10));
        ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>Yii::t('core','Add Components'),
            'icon' =>'plus-sign white',
            'type'=>'primary',
            'url'=>array(
                'component/create',
                'product_id'=>$model->id,
            )));
        ?>
    </div>
    <div id="component-details" class="span-18 last">
        <h3>Component Details</h3>
        <div class="form-wrapper">
        <?php
//            $component_model = $model->components[0];

            echo $this->renderPartial('/component/_form', array('model'=>$component, 'ajax'=>true));
        ?>
        </div>
    </div>
</div>
<?php
?>
