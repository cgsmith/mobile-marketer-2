<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'product-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php
    //TODO: This should not be in the view like this
    
    if(Yii::app()->user->checkAccess('admin')){
        $criteria = new CDbCriteria();
        $criteria->addCondition('active=1');
        $criteria->order = 'name ASC';
        echo $form->dropDownListRow(
                $model,
                'customer_main_id',CHtml::listData(
                    Customer::model()->findAll(
                        $criteria,
                        array('order'=>'name')
                    ),
                    'id',
                    'name'
                ),
                array('empty'=>'--select--','class'=>'span5')
            ); 
    } else if(Yii::app()->user->checkAccess('manager')){
        echo CHtml::hiddenField('Product[customer_main_id]', Yii::app()->user->customer_id);
    }
    ?>

<?php
//TODO: This should not be in the view like this
$criteria = new CDbCriteria();
//TODO: Add multiple product types to the model?
//$criteria->addCondition('active=1');
$criteria->order = 'type ASC';
echo $form->dropDownListRow($model,'product_type_id',CHtml::listData(
    ProductType::model()->findAll(
        $criteria,
        array('order'=>'type')), 'id','type'),array('empty'=>'--select--','class'=>'span5'));


$criteria = new CDbCriteria(); //TODO: This should not be in the view like this
//$criteria->addCondition('active=1');
$criteria->order = 'description ASC';
echo $form->dropDownListRow($model,'frequency_id',CHtml::listData(
    Frequency::model()->findAll(
        $criteria,
        array('order'=>'description')), 'id','description'),array('empty'=>'--select--','class'=>'span5')); ?>


	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>128)); ?>

	<?php echo $form->textAreaRow($model,'description',array('class'=>'span5','maxlength'=>256)); ?>

    <?php echo $form->datepickerRow($model, 'specific_datetime',
        array('options'=>array(
            'format'=>'yyyy-mm-dd',
            'minDate'=>'-1'
        ),'class'=>'span5')
    ); ?>

	<?php echo $form->dropDownListRow($model,'special_event',array('0'=>'No','1'=>'Yes'),array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
