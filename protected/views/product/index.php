<?php 
Yii::app()->getClientScript()->registerScript('_script_id',"
		$('#inactive').change(function(){
			// alert($('#inactive').is(':checked'));
			is_checked = $('#inactive').is(':checked')?'1':'0';
			$('#products_grid').prepend('<span class=\"grid-view-loading\" style=\"float: left; height: 20px; width: 20px;\"></span>');
			$.ajax({
				url: '".Yii::app()->baseUrl."/product/GetProductsGrid/show_inactive/'+ is_checked,
				success: function(data){
					// alert(data);
					$('#div-products-grid').html(data);
				}
			});
		});
		
		"
,CClientScript::POS_READY );
?>
<?php
$this->breadcrumbs=array(
	'Products',
);

$this->menu=array(
	array('label'=>'Create Product','url'=>array('create')),
	array('label'=>'Manage Product','url'=>array('admin')),
);
?>

<h1>Products</h1>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','New Product'),
    'icon' =>'plus-sign white',
    'type'=>'primary',
    'url'=>array(
        'product/create',
    )));

echo CHtml::label('Show Inactive', 'inactive', array ('style'=>'display: inline; margin: 5px;'));
echo CHtml::checkBox('inactive', false, array ('style'=>'margin: 0px;'));
?>
<div id="div-products-grid">
<?php 
$this->renderPartial('_products_grid', array('dataProvider' => $dataProvider, 'show_inactive'=>0));
?>
</div>