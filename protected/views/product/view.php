<?php
$this->breadcrumbs=array(
    'Customers'=>array('/customer/index'),
	'Customer'=>array('customer/view','id'=>$model->customer_main_id),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Product','url'=>array('index')),
	array('label'=>'Create Product','url'=>array('create')),
	array('label'=>'Update Product','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Product','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Product','url'=>array('admin')),
);
?>

<h1>Viewing <?php echo $model->name; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','Add Components'),
    'icon' =>'plus-sign white',
    'type'=>'primary',
    'url'=>array(
        'component/create',
        'product_id'=>$model->id,
    )));
?>   <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','View Components'),
    'icon' =>'search white',
    'type'=>'primary',
    'url'=>array(
        'product/components',
        'id'=>$model->id,
    )));
?>    <?php

if ($model->getComponentEmails(true)) {
    $onclick ='js:bootbox.prompt("Enter emails (comma separated)",
        function(result) {
            if (result === null) {
            } else {
                $.ajax({
                  type: "POST",
                  url: "'.Yii::app()->baseUrl.'/product/sendTestEmails",
                  data: {
                    emails: result,
                    productid: '.$model->id.',
                    "' . Yii::app()->request->csrfTokenName . '": "' . Yii::app()->request->csrfToken . '"
                  }
                })
                  .done(function() {
                    bootbox.alert("Sending emails to <b>"+result+"</b>");
                });
            }
        });';

    $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Test Email Components',
        'icon' =>'envelope white',
        'type'=>'primary',
        'htmlOptions'=>array(
            'style'=>'margin-left:3px',
            'onclick'=>$onclick,
        ),
    ));
}

?>

<?php
if(Yii::app()->user->checkAccess('admin')){
    echo CHtml::link(Yii::t('core','<i class="icon-minus icon-white"></i> Delete Product'),array('delete','id'=>$model->id),array(
        'class'=>'btn btn-danger',
        'confirm'=>Yii::t('core','Are you sure you want to DELETE this PRODUCT and ALL COMPONENTS?'),
    ));
}
?>

<br/>
<br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
        'name',
        'description',
		array(
            'name'=>'Customer',
            'type'=>'raw',
            'value'=>CHtml::link(CHtml::encode($model->customerMain->name),
                     array('customer/view','id'=>$model->customer_main_id)),
        ),
        array(
            'name'=>'Product Type',
            'type'=>'raw',
            'value'=>CHtml::encode($model->productType->type),
        ),
        array(
            'name'=>'Frequency',
            'type'=>'raw',
            'value'=>CHtml::encode($model->frequency->description),
        ),
        array(
            'name'=>'Date Created',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->date_created, 'short'),
        ),/*
        array(
            'name'=>'Date Created',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->date_updated, 'short'),
        ),*/
	),
)); ?>
