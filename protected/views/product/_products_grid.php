<?php

$gridColumns = array(
		array('name'=>'name', 'header'=>'Product name'),
		array('name'=>'active', 'header'=>'Active?'),
		array(
				'htmlOptions' => array('nowrap'=>'nowrap'),
				'class'=>'bootstrap.widgets.TbButtonColumn',
		)
);

$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'products_grid',
		'ajaxUrl'=>Yii::app()->baseUrl.'/product/GetProductsGrid/show_inactive/'+ $show_inactive,
		'dataProvider'=>$dataProvider,
		'columns'=>$gridColumns,
)); ?>
