<?php
$this->pageTitle=Yii::app()->name;

// Display information messages
if (Yii::app()->params['news']['on']) {
    Yii::app()->user->setFlash(Yii::app()->params['news']['type'], "<strong>".Yii::app()->params['news']['bold']."</strong> ".
        Yii::app()->params['news']['text']);
    $this->widget('bootstrap.widgets.TbAlert');
}
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?> <sup>&reg;</sup></i></h1>

<p>Mobile Marketer is an automated customer relationship management system that ties directly to a state of the art
24-hour Sales Support Center. Mobile Marketer manages your prospects and connects you to the Sales Support Center via
cellular phone, PDA and computer. Mobile Marketer allows you to manage contact and prospect data, send personalized
    letters, literature and marketing materials, and to manage continuous marketing campaigns from your office, home,
    hotel room... even the golf course. You'll be more professional and more productive, saving valuable time and money
    all while dramatically increasing your sales funnel activity.</p>

<p>Since Mobile Marketer is compatible with virtually every customer relationship management application on the market, there is no need to recreate your existing database. You can be up and running within 72 hours of registration, and Mobile Marketer is so easy to use, you'll be an expert in less than a half hour. Best of all, the software is free! You pay only for the materials you send. What's more, Mobile Marketer is extremely cost effective. For example, the Sales Support Center will prepare, print and mail your custom sales letters for less than $2.00.
</p>
<p>
    Simply connect to Mobile Marketer, enter a new contact or select an existing one and choose the stored materials you want to send. The materials you select, including a personalized letter with a signature that's practically indistinguishable from an original, will be mailed by the Sales Support Center that day assuming your order is received before 5:00PM. Your prospect really cannot tell that you did not prepare the personalized correspondence yourself.
</p>
    <p>Mobile Marketer and the Sales Support Center manage your campaigns with both technology and a robust staff of sales support experts, writers and marketing specialists organized with one mission. This mission is to ensure that your marketing efforts never cease, that you are always in front of your prospect, that you are always in your prospects mind - and most importantly that your company is repeatedly in their accounts payable system. We do this, with utmost precision, at a price that's a generation beyond turnkey.
    </p>
<p>Mobile Marketer and the Sales Support Center manage your campaigns with both technology and a robust staff of sales support experts, writers and marketing specialists organized with one mission. This mission is to ensure that your marketing efforts never cease, that you are always in front of your prospect, that you are always in your prospects mind - and most importantly that your company is repeatedly in their accounts payable system. We do this, with utmost precision, at a price that's a generation beyond turnkey.
</p><p>However, once you register, you can also seamlessly use Mobile Marketer in parallel from your office computer, home computer, or virtually any other mobile device including, Blackberry, Palm and Pocket PC devices. Wherever you are, you can connect to your Sales Support Center. Because if you're selling, you're busy, - or you're not.
</p><p>
    Thank you for choosing Mobile Marketer and the Sales Support Center.      </p>

