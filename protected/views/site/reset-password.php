<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);

// Display information messages
if (Yii::app()->request->getQuery('logout') !== null) {
    Yii::app()->user->setFlash('success', '<strong>Logout Successful</strong> You\'ve successfully logged out!');
    $this->widget('bootstrap.widgets.TbAlert');
}
?>

<h1>Reset Password</h1>
<?php 
    if(Yii::app()->user->hasFlash('error')){
        ?>
<div class="flash-error">
    <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php
    }
?>
<?php 
    if(Yii::app()->user->hasFlash('success')){
        ?>
<div class="flash-success">
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php
    }
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="row buttons">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>'Reset'));?>
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
