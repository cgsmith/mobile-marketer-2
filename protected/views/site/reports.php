<?php
$this->pageTitle=Yii::app()->name . ' - Reports';
$this->breadcrumbs=array(
	'Reports',
);

// Display information messages
if (Yii::app()->request->getQuery('logout') !== null) {
    Yii::app()->user->setFlash('success', '<strong>Logout Successful</strong> You\'ve successfully logged out!');
    $this->widget('bootstrap.widgets.BootAlert');
}
?>

<h1>Reports</h1>
