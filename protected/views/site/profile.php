<?php
$this->breadcrumbs=array(
    'Profile',
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<div class="info">
<?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
    if(Yii::app()->user->hasFlash($key)) { ?>
    <div class="flash-<?php echo $key; ?>">
        <?php echo Yii::app()->user->getFlash($key); ?>
    </div>
    <?php }
}?>
</div>



<h1>User Profile - <?=$model->last_name?>, <?=$model->first_name?></h1>

<?php echo $this->renderPartial('_profileform', array('model'=>$model)); ?>