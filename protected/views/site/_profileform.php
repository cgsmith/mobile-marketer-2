<?php
/* @var $form TbActiveForm */
/* @var $this SiteController */
?>
<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'profile-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    //'focus'=>array($model,'first_name'),
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <fieldset>
        <legend>Personal Information</legend>
        <?php echo $form->textFieldRow($model,'first_name',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'last_name',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'title',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'email',array('size'=>45,'maxlength'=>45,'hint'=>'Your email address will be used as your login name.')); ?>
        <?php echo $form->passwordFieldRow($model,'password1',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->passwordFieldRow($model,'password2',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->dropDownListRow($model,'roles',Yii::app()->user->listRoles(),array('readonly'=>!Yii::app()->user->checkAccess('admin'),'disabled'=>!Yii::app()->user->checkAccess('admin'))); ?>
        <?php echo $form->dropDownListRow($model,'start_page',$model->startPages()); ?>
        <?php echo $form->dropDownListRow($model,'pagination',$model->leadPagination()); ?>
        <?php echo $form->checkBoxRow($model,'receive_reports'); ?>
    </fieldset>
    <fieldset>
        <legend>Contact Information</legend>
        <?php echo $form->textFieldRow(Main::model()->findByPk($model->customer_main_id),'name',array('size'=>45,'maxlength'=>45,'readonly'=>true)); ?>
        <?php echo $form->textFieldRow($model,'address1',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'address2',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'city',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->dropDownListRow($model,'state',CHtml::listData(States::model()->findAll(array('order'=>'name')), 'abbreviation','name'),array('empty'=>'--select--')); ?>
        <?php echo $form->textFieldRow($model,'zip',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'phone',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'mobile',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'fax',array('size'=>45,'maxlength'=>45)); ?>
    </fieldset>
    <fieldset>
        <legend>Image Information</legend>
        <img src="<?php echo (($model->headshot_file !== '') && ($model->headshot_file !== null)) ? Yii::app()->baseUrl.'/'.$model->headshot_file : Yii::app()->params['images']['headblank'];?>" style="border: 1px;" width="100px">
        <img src="<?php echo (($model->signature_file !== '') && ($model->signature_file !== null)) ? Yii::app()->baseUrl.'/'.$model->signature_file : Yii::app()->params['images']['sigblank'];?>" style="border: 1px;" width="200px"><br/><br/>
        <?php echo $form->fileFieldRow($model, 'headshot_file'); ?>
        <?php echo $form->fileFieldRow($model, 'signature_file'); ?>
    </fieldset>
    
    <fieldset>
        <legend>API Information</legend>
        <?php echo $form->textFieldRow($model,'api_username',array('size'=>45,'maxlength'=>32)); ?>
        
        <?php // echo $form->textFieldRow($model,'api_key',array('size'=>45,'maxlength'=>32)); ?>
        <div class="control-group ">
            <label for="User_api_key" class="control-label">Api Key</label>
            <div class="controls">
                <span id="span_user_api_key">
                <?php
//                    $this->widget(TbActiveForm::INPUT_INLINE, array(
//			'type' => TbInput::TYPE_TEXT,
//			'form' => $form,
//			'model' => $model,
//			'attribute' => 'api_key',
//			'data' => null,
//			'htmlOptions' => array('placeholder'=>'', 'readonly'=>'true'),
//                    ));
                $user_api_key = empty($model->api_key) ? '': $model->api_key;
                echo empty($user_api_key) ? 'Click &quotGenerate Key&quot button to create API key': $user_api_key;
                ?>
                </span>
                <?php echo CHtml::hiddenField('User[api_key]', $user_api_key, array('id'=>'User_api_key')); ?>
                
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button','loadingText'=>'Generating...','size'=>'mini', 'type'=>'primary','label'=>'Generate Key', 'htmlOptions'=>array('id'=>'generate_api_key'))); ?>
            </div>
        </div>
    </fieldset>

    <div class="form-actions">
        <?php
        $submitLabel = $model->isNewRecord ? 'Create' : 'Save';
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>$submitLabel));
        ?>
    </div>

    <?php $this->endWidget(); ?>
<?php    
Yii::app()->clientScript->registerScript(
    'getapikey',
    '$("#generate_api_key").on("click",function(e){
        var btn = $(this);
        btn.button("loading");
        $.ajax("'.$this->createAbsoluteUrl('user/generateapikey').'", {
            "type":"get",
            dataType:"json",
            success:function(data){
                $("#User_api_key").val(data);
                $("span#span_user_api_key").html(data);
            },
            complete:function(){
                btn.button("reset");
            },
        });
    });',
    CClientScript::POS_READY
);
?>

</div><!-- form -->