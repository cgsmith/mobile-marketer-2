<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);

// Display information messages
if (Yii::app()->request->getQuery('logout') !== null) {
    Yii::app()->user->setFlash('success', '<strong>Logout Successful</strong> You\'ve successfully logged out!');
    $this->widget('bootstrap.widgets.TbAlert');
}
?>
<div class="info">
<?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
    if(Yii::app()->user->hasFlash($key)) { ?>
    <div class="flash-<?php echo $key; ?>">
        <?php echo Yii::app()->user->getFlash($key); ?>
    </div>
    <?php }
}?>
</div>
<h1>Login</h1>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>

	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons" style = "text-align:center;">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>'Login'));?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('url'=>$this->createUrl('site/resetpassword'), 'buttonType'=>'link','type'=>'link','label'=>'Reset Password'));?>
        <?php //$this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link','type'=>'success','label'=>'Create Account','url'=>array('user/create')));?>
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
