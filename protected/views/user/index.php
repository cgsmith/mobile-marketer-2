<?php
$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>Users</h1>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','New User'),
    'icon' =>'user white',
    'type'=>'primary',
    'url'=>array(
        'user/create',
    ),
));
?>

<?php
if(Yii::app()->user->checkAccess('admin')){
    $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>Yii::t('core','Manage Users'),
    //    'icon' =>'white',
        'type'=>'primary',
        'url'=>array(
            'user/admin',
        ),
    ));
}
?>

<?php 
//$this->widget('zii.widgets.CListView', array(
//	'dataProvider'=>$dataProvider,
//	'itemView'=>'_view',
//));
$gridColumns = array(
    'first_name',
    'last_name',
    'title',
    'customer_main_id',
    //'addresses',
    'active:boolean',
    array(
        'htmlOptions' => array('nowrap'=>'nowrap'),
        'class'=>'bootstrap.widgets.TbButtonColumn',
//        'template'=>'{view} {update}',
        'template'=>'{view}',
    )
);
$this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider'=>$dataProvider,
    'template'=>"{items}",
    'columns'=>$gridColumns,
));
?>
