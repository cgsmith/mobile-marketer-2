<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>View User: <?php echo $model->first_name .' ' . $model->last_name; ?></h1>

<?php 
//$this->widget('zii.widgets.CDetailView', array(
//	'data'=>$model,
//	'attributes'=>array(
//		'id',
//		'customer_main_id',
//		'email',
//		'password',
//		'first_name',
//		'last_name',
//		'title',
//		'address1',
//		'address2',
//		'city',
//		'state',
//		'zip',
//		'mailstop',
//		'phone',
//		'mobile',
//		'fax',
//		'date_created',
//		'receive_reports',
//		'active',
//	),
//));

$this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		'customer_main_id',
		'email',
//		'password',
		'first_name',
		'last_name',
		'title',
		'address1',
		'address2',
		'city',
		'state',
		'zip',
		'mailstop',
		'phone',
		'mobile',
		'fax',
		'date_created',
		'receive_reports:boolean',
		'active:boolean',
	),
));
?>