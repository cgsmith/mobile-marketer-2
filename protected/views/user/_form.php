<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'user-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'focus'=>array(Main::model(),'name'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <fieldset>
        <legend>Company Information</legend>

        <?php echo (($model->customer_main_id == null) ? $form->textFieldRow(Main::model(),'name',array('size'=>45,'maxlength'=>45)) : ''); ?>
        <?php echo $form->hiddenField($model,'customer_main_id');?>
        <?php echo $form->textFieldRow($model,'address1',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'address2',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'city',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->dropDownListRow($model,'state',CHtml::listData(States::model()->findAll(array('order'=>'name')), 'abbreviation','name'),array('empty'=>'--select--')); ?>
        <?php echo $form->textFieldRow($model,'zip',array('size'=>45,'maxlength'=>45)); ?>
    </fieldset>
    <fieldset>
        <legend>Personal Information</legend>
        <?php echo $form->textFieldRow($model,'first_name',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'last_name',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'title',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'phone',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'mobile',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'fax',array('size'=>45,'maxlength'=>45)); ?>
        <?php echo $form->textFieldRow($model,'email',array('size'=>45,'maxlength'=>45,'hint'=>'Your email address will be used as your login name.')); ?>

    </fieldset>
    <div class="form-actions">
        <?php
        $submitLabel = $model->isNewRecord ? 'Create' : 'Save';
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>$submitLabel));
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->