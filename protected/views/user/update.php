<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'View User', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>
<?php
Yii::app()->clientScript->registerScript('setuserinactive', "
$(document).on('click','#setuserinactive',function() {
    if(!confirm('Are you sure you want to delete this item?')) return false;
});    
");
?>
<h1>Update User <?php echo $model->first_name. ' '. $model->last_name; ?></h1>
<?php if(Yii::app()->user->checkAccess('admin')){
    $this->widget('bootstrap.widgets.TbButton', array(
        'id' => 'setuserinactive',
        'label'=>Yii::t('core','Set Inactive'),
        'icon' =>'remove white',
        'type'=>'danger',
        'url'=>array(
            'user/delete',
            'id'=>$model->id,
        )));
} ?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>