<?php
$this->breadcrumbs=array(
	'Login'=>array('site/login'),
	'Create Account',
);

?>

<h1>Create User <?php if ($company!==null){ echo 'for '.$company->name;}?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'company'=>$company)); ?>