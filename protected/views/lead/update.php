<?php
$this->breadcrumbs=array(
	'Leads'=>array('index'),

    $model->last.', '.$model->first => array('view','id'=>$model->id),
	'Update',
);

?>

<h1>Update <?php echo $model->last; ?>, <?php echo $model->first;?> </h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>