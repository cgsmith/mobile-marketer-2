<?php
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/lead/activeCampaigns.js');

$this->breadcrumbs=array(
	'Leads'=>array('index')
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
                <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
            <?php }
    }?>
</div>

<h1>Active Campaigns for <?php echo $model->last; ?>, <?php echo $model->first; ?>
</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'grid_active_campaigns',
	'dataProvider'=>$history,
	'columns'=>array(
			'confirmation_number',
			'date_ordered',
			'prod',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template' => '{task_it}',
				'buttons'=>array(
						'task_it' => array(
								'label'=>'Cancel Order', // text label of the button
								'url'=>'$data["order_id"]',
								'click'=>'js:function(){$("#hid-order-id").val($(this).attr("href")); openCancelOrderDialog(); return false;}',
								'icon'=>'icon-remove',
								// 'imageUrl'=>'/path/to/copy.gif',  // image URL of the button. If not set or false, a text link is used
								// 'options' => array('class'=>'cancel', ), // HTML options for the button
						),
				),
			),
		)
)); ?>
<input id="hid-csrf" type="hidden" value="<?php echo Yii::app()->request->csrfToken; ?>"/>
<input id="hid-baseUrl" type="hidden" value="<?php echo Yii::app()->baseUrl; ?>"/>
<input id="hid-lead-id" type="hidden" value="<?php echo $model->id; ?>"/>
<input id="hid-order-id" type="hidden" />