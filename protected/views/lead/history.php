<?php

$this->breadcrumbs=array(
	'Leads'=>array('index')
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
                <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
            <?php }
    }?>
</div>

<h1>History for <?php echo $model->last; ?>, <?php echo $model->first; ?>
</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'dataProvider'=>$history,
)); ?>
