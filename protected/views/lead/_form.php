<?php
/* @var $model Lead */
?>
<div class="form">

<?php
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'lead-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'focus'=>array($model,'first'),
));
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldRow($model,'first',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'last',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'title',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'company',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'address1',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'address2',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'city',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->dropDownListRow($model,'state',CHtml::listData(States::model()->findAll(array('order'=>'name')), 'abbreviation','name'),array('empty'=>'--select--')); ?>
        <?php echo $form->textFieldRow($model,'zip',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'phone',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'mobile',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'fax',array('size'=>45,'maxlength'=>45)); ?>

    	<?php echo $form->textFieldRow($model,'email',array('size'=>45,'maxlength'=>45)); ?>

    <div class="control-group">
        <?php echo $form->label($model,'keywords',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php
            echo CHtml::checkBoxList('keywords',
                CJSON::decode($model->keywords),
                CHtml::listData($model->getKeywords(),'id','keyword'),
                array('style'=>'float: left')
            ); ?>
        </div>
    </div>


	<?php echo $form->textAreaRow($model,'notes',array('rows'=>6, 'cols'=>50)); ?>

	<div class="form-actions">
        <?php
        $submitLabel = $model->isNewRecord ? 'Create' : 'Update';
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>$submitLabel));
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset','label'=>'Reset'));
	    ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->