<?php /* @var $model Lead */ ?>
<div class="form">

<?php
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'lead-form',
    'method'=>'GET',
    'type'=>'horizontal',
    'focus'=>array($model,'first'),
));
?>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldRow($model,'first',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'last',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'title',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'company',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'address1',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'address2',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->textFieldRow($model,'city',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->dropDownListRow($model,'state',CHtml::listData(States::model()->findAll(array('order'=>'name')), 'abbreviation','name'),array('empty'=>'--select--')); ?>

		<?php echo $form->textFieldRow($model,'zip',array('size'=>45,'maxlength'=>45)); ?>

		<?php echo $form->textFieldRow($model,'phone',array('size'=>45,'maxlength'=>45)); ?>
		
		<?php echo $form->textFieldRow($model,'mobile',array('size'=>45,'maxlength'=>45)); ?>

		<?php echo $form->textFieldRow($model,'fax',array('size'=>45,'maxlength'=>45)); ?>

		<?php echo $form->textFieldRow($model,'email',array('size'=>45,'maxlength'=>45)); ?>

		<?php echo $form->textAreaRow($model,'notes',array('size'=>45,'maxlength'=>45)); ?>

        <?php echo $form->checkBoxListRow($model,'keywords',
                                        CHtml::listData($model->getKeywords(),'id','keyword')); ?>
        <?php echo $form->radioButtonListRow($model,'in_campaign',array(
                                            0=>'Currently NOT in a campaign',
                                            1=>'Currently in a campaign'));?>
        <?php echo $form->radioButtonListRow($model,'been_in_campaign',array(
                                            0=>'Has NOT been in a campaign',
                                            1=>'Has been in a campaign'));?>

	<div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>'Search'));
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset','label'=>'Reset'));
	    ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->