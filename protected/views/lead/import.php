<?php

$this->breadcrumbs=array(
    'Leads'=>array('index'),
    'Import',
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
            <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
            <?php }
    }?>
</div>

<h1>Import Leads </h1>

<?php
$keywords = '<strong>Your groups are:</strong><br/>';
foreach (CHtml::listData(Keyword::model()->findAll(array('condition'=>'customer_main_id=:id','params'=>array(':id'=>Yii::app()->user->customer_id))),'id','keyword') as $key => $val)
{
    $keywords .= $key. ' = ' . $val. '<br/>';
}

Yii::app()->user->setFlash('success', $keywords);
$this->widget('bootstrap.widgets.TbAlert', array(
    'block'=>false, // display a larger alert block?
    'fade'=>true, // use transitions?
    'closeText'=>false, // close link text - if set to false, no close link is displayed
));
?>
<p>You can import leads from a CSV file.  Your groups need to be represented by numbers.  <a href="/example-r2.csv">Download an example</a>
    to see how to properly format your leads for import.</p>
<p>
You can put a comma between groups if a lead should belong to multiple groups.
</p>

<div class="form">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'upload-form',
        'enableAjaxValidation'=>false,
        'method'=>'post',
        'type'=>'horizontal',
        'htmlOptions'=>array(
            'enctype'=>'multipart/form-data'
        )
    ));
    ?>
    <fieldset>
        <legend>
            <p class="note">Fields with <span class="required">*</span> are required.</p>
        </legend>

        <?php echo $form->errorSummary($model, 'Oops!!!', null, array('class'=>'alert alert-error span12')); ?>

        <div class="control-group">
            <div class="span4">
                <div class="control-group <?php if ($model->hasErrors('postcode')) echo "error"; ?>">
                    <?php echo $form->labelEx($model,'file'); ?>
                    <?php echo $form->fileField($model,'file'); ?>
                    <?php // echo $form->error($model,'file'); ?>
                </div>


            </div>
        </div>

        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>'UPLOAD')); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'icon'=>'remove', 'label'=>'Reset')); ?>
        </div>

    </fieldset>

    <?php $this->endWidget(); ?>

</div><!-- form -->