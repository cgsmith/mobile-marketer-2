<?php
$this->breadcrumbs=array(
	'Leads',
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
/* 
Yii::app()->clientScript->registerScript(
    'dunno',
    "
    $('.orderenable').change(function() {
        if ($('.orderenable:checked').length) {
            $('#order-btn').removeAttr('disabled');
        } else {
            $('#order-btn').attr('disabled', 'disabled');
        }
    });
    $('.orderdisable').change(function() {
        if ($('.orderdisable:checked').length) {
            $('#order-btn').attr('disabled', 'disabled');
        }
    });
    ",
    CClientScript::POS_READY
); */
?>
<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
                <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
            <?php }
    }?>
</div>


<h1><?php echo Yii::t('core','lead.Leads'); ?></h1>

<?php if(Yii::app()->user->hasFlash('error')){ ?>
<div class="flash-error">
    <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php } ?>

<p><?php echo Yii::t('core','lead.hint'); ?></p>
<?php


$this->widget('ext.selgridview.SelGridView', array(
    'id'=>'mygrid',
    'dataProvider'=>$dataProvider,
    'selectableRows'=>2,
    'columns'=>array(
        array(
            'class'=>'CCheckBoxColumn',
        ),
        array('name'=>'first', 'header'=>Yii::t('core','model.lead.first')),
        array('name'=>'last', 'header'=>Yii::t('core','model.lead.last')),
        array('name'=>'company', 'header'=>Yii::t('core','model.lead.company')),
        array('name'=>'city', 'header'=>Yii::t('core','model.lead.city')),
        array('name'=>'state', 'header'=>Yii::t('core','model.lead.state')),
        array('name'=>'zip', 'header'=>Yii::t('core','model.lead.zip')),
        array('name'=>'phone', 'header'=>Yii::t('core','model.lead.phone')),
        array('name'=>'email', 'header'=>Yii::t('core','model.lead.email')),
        array('name'=>'notes', 'header'=>Yii::t('core','model.lead.notes')),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view} {update}',
            'htmlOptions'=>array('style'=>'width: 35px'),
        ),
    ),
));
?>


<script type="text/javascript">
jQuery(function($) {
$("#order-btn").click(function(){
var selected = $("#mygrid").selGridView("getAllSelection");
$("#selected-keys").val("[" + selected.join(", ") + "]");
});

});
</script>
<?php echo CHtml::beginForm(array('orders/create')); ?>

<?php echo CHtml::hiddenField('selected','',array('id'=>'selected-keys')); ?>

<div id="submitfooter" style="position: fixed;
                bottom: 0;
                width: 100%;
                height: 70px; background-color: white;">
  <br/>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>Yii::t('core','lead.index.order_products'),
    'type'=>'success',
    'icon'=>'shopping-cart white',
    'htmlOptions'=>array('id'=>'order-btn'),
    )); ?>
&nbsp;&nbsp;
<?php /*$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>'Email Blast',
    'type'=>'primary',
    'icon'=>'envelope white',
    'htmlOptions'=>array('id'=>'email-btn','name'=>'email-blast'),
    )); */?>
</div>

<?php echo CHtml::endForm(); ?>
