<?php

$this->breadcrumbs=array(
	'Leads'=>array('index'),
	$model->last.', '.$model->first,
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
                <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
            <?php }
    }?>
</div>

<h1>Viewing <?php echo $model->last; ?>, <?php echo $model->first; ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','update'),
    'icon' =>'pencil white',
    'type'=>'primary',
    'url'=>array(
        'lead/update',
        'id'=>$model->id,
    )));
    ?>
    <?php echo CHtml::link('Order History',array('history','id'=>$model->id),array(
        'class'=>'btn btn-primary',
    )) ?>
    <?php echo CHtml::link('Active Campaigns',array('activeCampaigns','id'=>$model->id),array('class'=>'btn btn-primary', )) ?>
    <?php echo CHtml::link('Copy Lead',array('copy','id'=>$model->id),array('class'=>'btn btn-primary', )) ?>
    <?php echo CHtml::link(Yii::t('core','delete'),array('delete','id'=>$model->id),array(
        'class'=>'btn btn-danger',
        'confirm'=>Yii::t('core','confirm.delete'),
    )) ?>
    <?php echo CHtml::beginForm(array('orders/create'), 'post',array('style'=>'display:inline')); ?>
    <?php echo CHtml::hiddenField('selected',$_GET['id']); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label'=>Yii::t('core','lead.index.order_products'),
        'type'=>'success',
        'icon'=>'shopping-cart white',
        'htmlOptions'=>array('id'=>'order-btn'),
        'url'=>array('orders/verify'),
    )); ?>
    <?php echo CHtml::endForm(array('')); ?>
</h1>


<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'first',
		'last',
		'title',
		'company',
		'address1',
		'address2',
		'city',
		'state',
		'zip',
		'phone',
        'mobile',
		'fax',
		'email',
		'date_created',
		'date_updated',
        'keywords',
		'notes',
	),
)); ?>
