<?php
$this->breadcrumbs=array(
    'Order Products',
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);

Yii::app()->clientScript->registerScript(
    'dunno',
    "
    $('.orderenable').change(function() {
        if ($('.orderenable:checked').length) {
            $('#order-btn').removeAttr('disabled');
        } else {
            $('#order-btn').attr('disabled', 'disabled');
        }
    });
    $('.orderdisable').change(function() {
        if ($('.orderdisable:checked').length) {
            $('#order-btn').attr('disabled', 'disabled');
        }
    });
    ",
    CClientScript::POS_READY
);
?>

<div style = "font-weight:bold;">Order Products</div>

<?php echo CHtml::beginForm(array('orders/verify')); ?>
<?php

echo CHtml::hiddenField('selected',$_POST['selected']);
//$_POST['selected']; //JSON array of leads being ordered.
$type=null; //Used for determining split of categories
$first=true; //Used for ending table closures
foreach ($products as $product) {
    if ($product['type'] <> $type){
        if(!$first) {
            echo $this->endTable(); //Close prior table
        }
        $first = false;
        $type = $product['type'];
        echo $this->createTable($product['type']);
        echo $this->addTable($product,true);
    }

    echo $this->addTable($product);
}
echo $this->endTable();
?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>Yii::t('core','lead.index.order_products'),
    'type'=>'success',
    'icon'=>'shopping-cart white',
    'htmlOptions'=>array('id'=>'order-btn','disabled'=>'true'),
    'url'=>array('orders/verify'),
)); ?>