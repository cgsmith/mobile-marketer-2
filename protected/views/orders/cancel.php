<?php
Yii::app()->clientScript->registerScript('script-ensure-id', "
		$('#btn-cancel-order').click(function() {
			return window.confirm(this.title || 'Are you sure you want to cancel this order?');
		});
		", CClientScript::POS_READY);

$this->widget('bootstrap.widgets.TbAlert', array(
		'block'=>true, // display a larger alert block?
		'fade'=>true, // use transitions?
		'closeText'=>'x', // close link text - if set to false, no close link is displayed
		'alerts'=>array( // configurations per alert type
				'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'x'), // success, info, warning, error or danger
				'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'x'), // success, info, warning, error or danger
				'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'x'), // success, info, warning, error or danger
		),
));

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'inlineForm',
		'type'=>'inline',
		'htmlOptions'=>array('class'=>'well'),
));
?>
<div class="controls">
	<label class="control-label">Order Number</label> <input type="text"
		id="txt-order-number" name="txt-order-number" maxlength="45" size="45" />
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'label'=>Yii::t('core','menu.cancel_order'),
			'type'=>'danger',
			'icon'=>'icon-remove',
			'htmlOptions'=>array('id'=>'btn-cancel-order'),
			//'url'=>array('orders/cancel'),
	));
	?>
</div>
<?php $this->endWidget(); ?>
