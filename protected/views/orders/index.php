<?php
$this->breadcrumbs=array(
	'Orders',
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
                <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
            <?php }
    }?>
</div>


<h1>Orders</h1>
<?php if(Yii::app()->user->hasFlash('success')){ ?>
<div class="flash-success">
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php } ?>
<div style = "width:95%; margin-left:auto; margin-right:auto;">
<?php

$this->widget('ext.selgridview.SelGridView', array(
    'id' => 'mygrid',
    'dataProvider' => $dataProvider,
    'selectableRows' => 2,
    'columns' => array(
        array('name'=>'id','header'=> '#', 'visible'=>false),
        array('name'=>'confirmation_number','header'=> 'Confirmation Number'),
        array('name'=>'first','header'=> 'First Name'),
        array('name'=>'last','header'=> 'Last Name'),
        array('name'=>'title','header'=> 'Title'),
        array('name'=>'address1','header'=> 'Address 1'),
        array('name'=>'city','header'=> 'City'),
        array('name'=>'state','header'=> 'State'),
        array('name'=>'zip','header'=> 'Zip'),
//        array('name'=>'lead_email','header'=> 'Email'),
    )
));

//$gridColumns = array(
//    array('name'=>'lead_id', 'header'=>'#', 'visible'=>false),
//    array('name'=>'lead_first', 'header'=>'First Name'),
//    array('name'=>'lead_last', 'header'=>'Last Name'),
//    array('name'=>'lead_title', 'header'=>'Title'),
//    array(
//        'htmlOptions' => array('nowrap'=>'nowrap'),
//        'class'=>'bootstrap.widgets.TbButtonColumn',
//    )
//);

//$this->widget('bootstrap.widgets.TbGridView',array(
//    'dataProvider'=>$dataProvider,
//    'columns'=>$gridColumns,
//));

?>
</div>