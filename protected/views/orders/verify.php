<?php
/* @var $this ControllerOrder */
$this->breadcrumbs=array(
    'Verify Order',
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>

<h1>Verify Order</h1>
<p>Following users are included in the order.</p>
<p><strong>Product: </strong><?php echo $product->name; ?></p>
<?php echo CHtml::beginForm(array('orders/confirm')); ?>
<?php
echo CHtml::hiddenField('order_selected',$_POST['selected']);
echo CHtml::hiddenField('order_product_id',$product->id);
?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>Yii::t('core','lead.index.order_products'),
    'type'=>'success',
    'icon'=>'shopping-cart white',
    'htmlOptions'=>array('id'=>'order-btn'),
    'url'=>array('orders/verify'),
)); ?>
<?php echo CHtml::endForm(); ?>
<?php 
$this->widget('ext.selgridview.SelGridView', array(
//$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'mygrid',
    'dataProvider' => $order_customer_leads_list,
    'selectableRows' => 2,
    'beforeAjaxUpdate' => "function(id, options) {
        if(!options.url.match(/product_id/)){
            options.url += '&selected=".$_POST['selected']."';
            options.url += '&product_id=".$product->id."';
            var params = $.deparam.querystring(options.url);
            options.url = $.param.querystring(options.url, params);
        }
    } ",
    'columns' => array(
        array('name'=>'id','header'=> '#', 'visible'=>false),
        array('name'=>'first','header'=> 'First Name'),
        array('name'=>'last','header'=> 'Last Name'),
        array('name'=>'company','header'=> 'Company'),
    )
));

//$gridColumns = array(
//    array('name'=>'id','header'=> '#', 'visible'=>false),
//    array('name'=>'first','header'=> 'First Name'),
//    array('name'=>'last','header'=> 'Last Name'),
//    array('name'=>'title','header'=> 'Title'),
//    array(
//        'htmlOptions' => array('nowrap'=>'nowrap'),
//        'class'=>'bootstrap.widgets.TbButtonColumn',
//    )
//);
//
//$this->widget('bootstrap.widgets.TbGridView', array(
//    'dataProvider'=> $order_customer_leads_list,
//    'template'=>"{items}",
//    'columns'=>$gridColumns,
//));
?>