<?php
$this->breadcrumbs=array(
	'Email Blast Creation',
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
                <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
            <?php }
    }?>
</div>


<h1>Email Blast Creation</h1>
<?php if(Yii::app()->user->hasFlash('success')){ ?>
<div class="flash-success">
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php } ?>
<div style = "width:95%; margin-left:auto; margin-right:auto;">

    <?php /** @var BootActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'horizontalForm',
        'type'=>'horizontal',
    )); ?>

    <?php echo $form->redactorRow($model, 'email', array('rows'=>10)); ?>
    <?php $this->endWidget();?>
</div>