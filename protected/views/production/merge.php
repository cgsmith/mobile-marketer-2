<?php
$this->breadcrumbs=array(
    'Example Merge',
);
?>

<h2>Merge Demonstration</h2>

<?php $this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Merge Variables (click to expand)'=>$this->renderPartial('_mergevars',null,true),
    ),
    // additional javascript options for the accordion plugin
    'options'=>array(
        'animated'=>'bounceslide',
        'collapsible'=>'true',
        'active'=>'false',
    ),
));
?>

<?php echo CHtml::beginForm('http://localhost:8080/phpdocx_pro/test.php','POST',array('enctype'=>'multipart/form-data')); ?>



<?php echo CHtml::hiddenField('u.first',User::model()->findByPk(Yii::app()->user->id)->first_name); ?>
<?php echo CHtml::hiddenField('u.last',User::model()->findByPk(Yii::app()->user->id)->last_name); ?>
<?php echo CHtml::hiddenField('u.office',User::model()->findByPk(Yii::app()->user->id)->phone); ?>
<?php echo CHtml::hiddenField('u_cellphone',User::model()->findByPk(Yii::app()->user->id)->mobile); ?>
<?php echo CHtml::hiddenField('u.email',User::model()->findByPk(Yii::app()->user->id)->email); ?>
<?php echo CHtml::hiddenField('u.title',User::model()->findByPk(Yii::app()->user->id)->title); ?>
<?php echo CHtml::hiddenField('u.company',Customer::model()->findByPk(Yii::app()->user->customer_id)->name); ?>

<?php

$leads = Yii::app()->db->createCommand()
    ->select('*')
    ->from('order_leads')
    ->queryAll();

foreach ($leads as $lead){
    echo CHtml::hiddenField('l_first[]',$lead['lead_first']);
    echo CHtml::hiddenField('l_last[]',$lead['lead_last']);
    echo CHtml::hiddenField('l_title[]',$lead['lead_title']);
    echo CHtml::hiddenField('l_company[]',$lead['lead_company']);
    echo CHtml::hiddenField('l_address1[]',$lead['lead_address1']);
    echo CHtml::hiddenField('l_city[]',$lead['lead_city']);
    echo CHtml::hiddenField('l_state[]',$lead['lead_state']);
    echo CHtml::hiddenField('l_zip[]',$lead['lead_zip']);
}
?>

<p>Upload a document with the merge variables to see an example merge</p><br>
Template: <?php echo CHtml::fileField('docx'); ?><br/>
<br/><br/>
<?php echo CHtml::submitButton('Create'); ?>
<?php echo CHtml::endForm(); ?>


<?php echo CHtml::beginForm('http://localhost:8080/phpdocx_pro/pdf.php','POST',array('enctype'=>'multipart/form-data')); ?>
Template: <?php echo CHtml::fileField('docx'); ?><br/>
<?php echo CHtml::submitButton('Generate PDF'); ?>
<?php echo CHtml::endForm(); ?>