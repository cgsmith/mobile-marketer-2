<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data'=>array('firstName'=>'$FIRST_NAME$', 'lastName'=>'$LAST_NAME$', 'company'=>'$COMPANY$',
    'sig'=>'$SIGNATURE$ (place in alt. text - description field)',
    'head'=>'$HEAD$ (place in alt. text - description field)',
    ),
    'attributes'=>array(
        array('name'=>'firstName', 'label'=>'First name'),
        array('name'=>'lastName', 'label'=>'Last name'),
        array('name'=>'company', 'label'=>'Company'),
        array('name'=>'sig', 'label'=>'Signature'),
        array('name'=>'head', 'label'=>'Head Shot'),
    ),
));
?>
