<?php
/* @var $this ProductionController
 * @var $form CActiveForm
 * @var $email_content string
 * 
 **/
$this->breadcrumbs=array(
	'Production',
);
?>
<h1>Email Preview</h1>
<div id="preview-container">
    <?php echo $email_content ?>
</div>
<?php 
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'email-send-form',
    'type'=>'horizontal',
));
echo CHtml::hiddenField('send');
?>
<div class="form-actions">
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Send Email')); ?>
</div>
<?php $this->endWidget(); ?>