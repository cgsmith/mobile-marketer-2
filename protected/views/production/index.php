<?php
/* @var $this ProductionController
 * @var $dataProvider CActiveDataProvider
 * @var $form CActiveForm
 * @var $model Production
 * 
 **/
$this->breadcrumbs=array(
	'Production',
);

Yii::app()->clientScript->registerScript('search', "
$('.date-form form').submit(function(){
	$.fn.yiiGridView.update('orders-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('lastChecked', "
        var lastChecked = null;

        $(document).ready(function() {
            var \$chkboxes = $('.printed');
            \$chkboxes.click(function(e) {
                if(!lastChecked) {
                    lastChecked = this;
                    return;
                }

                if(e.shiftKey) {
                    var start = \$chkboxes.index(this);
                    var end = \$chkboxes.index(lastChecked);

                    \$chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).attr('checked', lastChecked.checked);

                }

                lastChecked = this;
            });
        });

");

if(Yii::app()->user->hasFlash('success')){
    echo '<div class="flash-success">'.Yii::app()->user->getFlash('success').'</div>';
}

if(Yii::app()->user->hasFlash('error')){
    echo '<div class="flash-error">'.Yii::app()->user->getFlash('error').'</div>';
}
?>
<div class="date-form">

<?php echo CHtml::beginForm(); ?>

From:
<?php
$this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'Date[from]',
    // additional javascript options for the date picker plugin
    'options'=>array(
        'showAnim'=>'fold',
    ),
    'value'=>$date['from'],
    'htmlOptions'=>array(
        'style'=>'height:20px;',
        
    ),
));
?>
To:
<?php
$this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'Date[to]',
    // additional javascript options for the date picker plugin
    'options'=>array(
        'showAnim'=>'fold',
    ),
    'value'=>$date['to'],
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));
?>
 
<?php
$this->widget('bootstrap.widgets.TbButton',array(
    'buttonType'=>'submit',
	'label' => 'Search',
    'icon' => 'search white',
    'type' => 'primary',
    'size'=>'small',
));
?>

<?php echo CHtml::endForm(); ?>
</div>

<?php
//Yii::app()->clientScript->registerScript('markasprinted', "
//    
//");
?>
<?php
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'production-form',
    'enableAjaxValidation'=>true,
    'type'=>'horizontal',
));

echo $form->dropDownListRow($model, 'filter', $model->getFilterOptions(), array('multiple'=>false));

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'orders-grid',
    'dataProvider' => $dataProvider,
    'selectableRows' => 2,
    'columns' => array(
        array(
//            'class' => 'CCheckBoxColumn',
//            'name' => 'selected',
//            'id' => 'selected',
            'type' => 'raw',
            'value' => array($model, 'getCheckBox'),
//            'value' => 'CHtml::textField("sortOrder[$data->menuId]",$data->sortOrder,array("style"=>"width:50px;"))',
//            <input type="checkbox" name="selected[]" id="selected_1" value="18-65-7405" class="select-on-check">
        ),
        array('name'=>'id', 'header' => 'ID'),
        array('name'=>'product_name', 'header' => 'Product'),
        array('name'=>'product_type', 'header' => 'Product Type'),
//        array('name'=>'product_curr_seq_number', 'header' => 'Current Sequence'),
        array('name'=>'lead_first_name', 'header' => 'First Name'),
        array('name'=>'lead_last_name', 'header' => 'Last Name'),
        array('name'=>'component_name', 'header' => 'Component'),
        array('name'=>'component_type', 'header' => 'Component Type'),
        array('name'=>'component_type_id', 'visible'=>false),
        array('name'=>'component_sequence', 'header' => 'Component Sequence'),
//        'frequency_days',
        array('name'=>'date_ordered', 'header' => 'Date Ordered'),
        array(
            'name' => 'date_production',
            'header' => 'Date Production',
//            'value' => array($model, 'getProductionDate'),
        ),
        array('name'=>'customer_name', 'header' => 'Customer'),
        array('name'=>'user', 'header' => 'User'),
		// No need for download as pdf as required @ 02/06/2014 so commented
/*         array(
            'name' => 'action',
            'header' => 'Action',
            'value' => array($model, 'getProduceLink'),
            'type' => 'html',
        ) */
    )
));
?>
<?php // echo CHtml::ajaxSubmitButton('',array('production/ajaxupdate'), array(),array("style"=>"display:none;")); ?>

<?php  
//    echo CHtml::ajaxSubmitButton(
//        'Mark as Printed',
//        array('production/ajaxupdate','act'=>'doprinted'), 
//        array('success'=>'reloadGrid'),
//        array('class'=>'btn btn-primary'));

    ?>
    <div  style="text-align: center;">
<?php   $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Download Selected as ZIP')); ?>

<?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'button',
            'label'=>Yii::t('core','Mark selected as printed'),
            'type'=>'primary',
            'icon'=>'pencil white',
            'htmlOptions'=>array('id'=>'markasprinted'),
            'url'=>array('production/ajaxupdate', 'act'=>'doprinted'),
        ));
?>

    </div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
//$('.printed').on('click', $('table.items'), function(e){
//    $.ajax('<?php echo $this->createAbsoluteUrl('production/ajaxupdate', array('act'=>'confirm')); ?>',{
//        'dataType':'json',
//        'type':'post',
//        data:{selected:$(this).val(), YII_CSRF_TOKEN:$('input[name=YII_CSRF_TOKEN]').val()},
//        success:function(data){
//            if(data.success){
//                reloadGrid();
//            }
//        }
//    });
//});
$('button#markasprinted').on('click',function(e){
        var thisbutton = this;
        $(thisbutton).attr('disabled','disabled');
        var selected_orders = $('input.printed').filter(':checked');
	if(selected_orders.length==0){
            e.preventDefault();
            alert('Please select production items first.');
            return false;
        } else {
            var selected = [];
            $.each(selected_orders, function(i,v){
//                console.log(i+': '+v.value+'<br />');
                selected.push(v.value);
            });
            $.ajax('<?php echo $this->createAbsoluteUrl('production/ajaxupdate', array('act'=>'markasprinted')); ?>',{
                'dataType':'json',
                'type':'post',
                data:{selected:selected.join(','), YII_CSRF_TOKEN:$('input[name=YII_CSRF_TOKEN]').val()},
                success:function(data){
                    if(data.success){
                        $.fn.yiiGridView.update('orders-grid');
                    }
                },
                error:function(jqXHR, status, thrown ){
                    alert('Failed to perform the action. Retry later.');
                },
                complete: function(jqXHR,status){
                    $(thisbutton).removeAttr('disabled');
                }
            });
        }
	return true;
    });
    
$('select#Production_filter').on('change',function(e){
    $.fn.yiiGridView.update('orders-grid', {
        data:{filter:$(this).val()}
    });
	if($('select#Production_filter').val() == 1)	//printed
		$('#markasprinted').attr("disabled", "disabled");
	else
		$('#markasprinted').removeAttr("disabled");
});
$(document).on('change','.printed',function(){
if($('button#markasprinted').is(':disabled') && $('select#Production_filter').val() !=1) 
	$('#markasprinted').removeAttr('disabled');
});
</script>