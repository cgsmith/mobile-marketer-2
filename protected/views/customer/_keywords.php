<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Chris
 * Date: 11/8/12
 * Time: 1:01 PM
 * To change this template use File | Settings | File Templates.
 */

$keyword=new Keyword();
echo CHtml::beginForm(array('customer/createkeyword'));
echo CHtml::activeLabel($keyword,'keyword');
echo CHtml::activeTextField($keyword,'keyword',array('maxlength'=>255));
echo CHtml::hiddenField('customer_main_id',$data->id);
?>

<?php
echo CHtml::submitButton('Add Keyword'); ?>
Keywords can be seperated by commas

<?php

$currentKeywords = Yii::app()->db->createCommand()
    ->select('id,keyword')
    ->from('customer_keyword')
    ->where('active=1 AND customer_main_id=:id',array(':id'=>$data->id))
    ->queryAll();
echo '<h4>Current Keywords</h4>';

foreach ($currentKeywords as $curKeyword) {
    echo '<br>'.$curKeyword['keyword'].' ';
    $this->widget('bootstrap.widgets.TbButton',array(
        'label'=>'Delete',
        'size'=>'mini',
        'url'=>Yii::app()->createUrl("customer/deletekeyword",array("id"=>$curKeyword['id']))
    ));
}

echo CHtml::endForm();
