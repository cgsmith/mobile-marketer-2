<?php /* $var $user User  */ ?>
<div class="form">

    <?php
    
    Yii::app()->clientScript->registerScriptFile(
        Yii::app()->baseUrl . '/js/addAddress.js',
        CClientScript::POS_END
    );
    
    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'new-customer-form',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>array($model,'name'),
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <?php //echo count($users); foreach ($users as $user){ echo $form->errorSummary($user); }
          //echo $form->errorSummary($users); ?>
    <?php //echo $form->errorSummary($address); ?>

    <?php echo $form->textFieldRow($model,'name'); ?>
    <fieldset>
        <legend>Billing Address
            <?php 
//            $this->widget('bootstrap.widgets.TbButton', array(
//                'type'=>'success',
//                'buttonType'=>'button',
//                'size'=>'mini',
//                'icon'=>'plus-sign white',
//                'label'=>'Add an Address',
//                'htmlOptions'=>array(
//                    'id'=>'cloneAddress1',
//                ),
//            ));
            ?>
        </legend>
        <?php if(isset($update)){
            $model->addresses = $address; //  ugly fix.
        foreach ($model->addresses as $address){ $i = $address->id; ?>
        <div class="MainAddress">
            <?php echo $form->textFieldRow($address,"[$i]address1",array('size'=>50,'maxlength'=>50, 'value'=>$address->address1));?>
            <?php echo $form->textFieldRow($address,"[$i]address2",array('size'=>50,'maxlength'=>50, 'value'=>$address->address2));?>
            <?php echo $form->textFieldRow($address,"[$i]city",array('size'=>50,'maxlength'=>50, 'value'=>$address->city));?>
            <?php echo $form->dropDownListRow($address,"[$i]state",CHtml::listData(States::model()->findAll(array('order'=>'name')), 'abbreviation','name'),array('empty'=>'--select--')); ?>
            <?php echo $form->textFieldRow($address,"[$i]zip",array('size'=>50,'maxlength'=>50, 'value'=>$address->zip));?>
            <?php echo CHtml::hiddenField("Address[$i][id]", $address->id); ?>
        </div>
        <hr />
        <?php }
        } else { ?>
            <div class="MainAddress">
            <?php echo $form->textFieldRow($address,"address1",array('size'=>50,'maxlength'=>50));?>
            <?php echo $form->textFieldRow($address,"address2",array('size'=>50,'maxlength'=>50));?>
            <?php echo $form->textFieldRow($address,"city",array('size'=>50,'maxlength'=>50));?>
            <?php echo $form->dropDownListRow($address,"state",CHtml::listData(States::model()->findAll(array('order'=>'name')), 'abbreviation','name'),array('empty'=>'--select--')); ?>
            <?php echo $form->textFieldRow($address,"zip",array('size'=>50,'maxlength'=>50));?>
        </div>
        <?php } ?>
        <div class="AltAddress">
        </div>
    </fieldset>

    <fieldset>
        <legend>Management
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'type'=>'success',
            'buttonType'=>'button',
            'size'=>'mini',
            'icon'=>'plus-sign white',
            'label'=>'Add a Manager',
            'htmlOptions'=>array(
                'id'=>'cloneManager',
            ),
        ));?></legend>
        <p><?php $this->widget('bootstrap.widgets.TbLabel', array('type'=>'info','label'=>'Note'));?> The management team
            will be assigned a 'manager' role.</p>
        <?php
//        print_r($users);
        $count = 0;
        foreach($users as $i => $user){
            if(isset($user->id)) $i = $user->id;
            $count++;
        ?>
        <div class="manager-user-fields">
            <?php if($count>1){ ?><button class="btn btn-mini btn-danger remove-user-form"><i class="icon-white icon-remove white"></i></button><?php } ?>
            <?php echo $form->textFieldRow($user,'['.$i.']email',array('size'=>45,'maxlength'=>45, 'class'=>'user_email')); ?>
            <?php echo $form->textFieldRow($user,'['.$i.']first_name',array('size'=>45,'maxlength'=>45, 'class'=>'user_first_name')); ?>
            <?php echo $form->textFieldRow($user,'['.$i.']last_name',array('size'=>45,'maxlength'=>45)); ?>
            <?php echo $form->textFieldRow($user,'['.$i.']title',array('size'=>45,'maxlength'=>45)); ?>
            <?php if(isset($update)) echo CHtml::hiddenField("User[$i][id]", $user->id, array('class'=>'useridhidden')); ?>
            <div class="control-group">
                <?php echo $form->label($user,'phone',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('CMaskedTextField',array(
                        'model'=>$user,
                        'attribute'=>'['.$i.']phone',
                        'mask'=>'(999) 999-9999? x99999'
                    )); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->label($user,'mobile',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('CMaskedTextField',array(
                        'model'=>$user,
                        'attribute'=>'['.$i.']mobile',
                        'mask'=>'(999) 999-9999'
                    )); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->label($user,'fax',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('CMaskedTextField',array(
                        'model'=>$user,
                        'attribute'=>'['.$i.']fax',
                        'mask'=>'(999) 999-9999'
                    )); ?>
                </div>
            </div>
        </div>
        <hr />
        <?php } ?>
        
        <?php 
        // Its for newly added users at update form
        if(isset($update) && !empty($newusers)){
        $count = 0;
        foreach($newusers as $i => $user){
            if(isset($user->id)) $i = $user->id;
            $count++;
        ?>
        <div class="manager-user-fields">
            <button class="btn btn-mini btn-danger remove-user-form"><i class="icon-white icon-remove white"></i></button>
            <?php echo $form->textFieldRow($user,'['.$i.']email',array('size'=>45,'maxlength'=>45, 'class'=>'user_email')); ?>
            <?php echo $form->textFieldRow($user,'['.$i.']first_name',array('size'=>45,'maxlength'=>45, 'class'=>'user_first_name')); ?>
            <?php echo $form->textFieldRow($user,'['.$i.']last_name',array('size'=>45,'maxlength'=>45)); ?>
            <?php echo $form->textFieldRow($user,'['.$i.']title',array('size'=>45,'maxlength'=>45)); ?>
            <?php if(isset($update)) echo CHtml::hiddenField("User[$i][id]", $user->id, array('class'=>'useridhidden')); ?>
            <div class="control-group">
                <?php echo $form->label($user,'phone',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('CMaskedTextField',array(
                        'model'=>$user,
                        'attribute'=>'['.$i.']phone',
                        'mask'=>'(999) 999-9999? x99999'
                    )); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->label($user,'mobile',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('CMaskedTextField',array(
                        'model'=>$user,
                        'attribute'=>'['.$i.']mobile',
                        'mask'=>'(999) 999-9999'
                    )); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->label($user,'fax',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('CMaskedTextField',array(
                        'model'=>$user,
                        'attribute'=>'['.$i.']fax',
                        'mask'=>'(999) 999-9999'
                    )); ?>
                </div>
            </div>
        </div>
        <hr />
        <?php } } ?>
    </fieldset>
    <?php if(isset($update)){ ?>
<!--    <input type="hidden" name="addnew" id="addnew" />-->
    <?php } ?>
    <div class="form-actions">
        <?php
        $submitLabel = $model->isNewRecord ? 'Create' : 'Update';
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>$submitLabel));
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset','label'=>'Reset'));
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
