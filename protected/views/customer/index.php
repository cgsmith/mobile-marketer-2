<?php
$this->breadcrumbs=array(
	'Organizations',
);

$this->menu=array(
	array('label'=>'Create Organization','url'=>array('create')),
	array('label'=>'Manage Organization','url'=>array('admin')),
);
?>

<h1>Organizations</h1>
<?php if(Yii::app()->user->hasFlash('success')){ ?>
<div class="flash-success">
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php } ?>
<?php 
$this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','New Organization'),
    'icon' =>'plus-sign white',
    'type'=>'primary',
    'url'=>array(
        'customer/create',
    )
));
?>
<?php
$gridColumns = array(
    'name',
    //'addresses',
    'active:boolean',
    array(
        'htmlOptions' => array('nowrap'=>'nowrap'),
        'class'=>'bootstrap.widgets.TbButtonColumn',
        'template'=>'{view} {update}',
    )
);

$this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider'=>$dataProvider,
    'template'=>"{items}",
    'columns'=>$gridColumns,
));
?>