<?php

$criteria = new CDbCriteria;
$criteria->order = 'name ASC';
$criteria->addCondition('customer_main_id='.$data->id);
$criteria->addCondition('active=1');
$dataProvider= new CActiveDataProvider('Product',array(
    'criteria'=>$criteria,
));
$gridColumns = array(
    'name',
    'description',

    array(
        'htmlOptions' => array('nowrap'=>'nowrap'),
        'class'=>'bootstrap.widgets.TbButtonColumn',
        'template'=>'{view} {update}',
        'buttons'=>array(
            'view'=>array(
                'url'=>'Yii::app()->createUrl("/product/view",array("id"=>$data->id))',
            ),
            'update'=>array(
                'url'=>'Yii::app()->createUrl("/product/update",array("id"=>$data->id))',
            )
        )
    )
);

$this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider'=>$dataProvider,
    //'template'=>"{items}",
    'columns'=>$gridColumns,
));