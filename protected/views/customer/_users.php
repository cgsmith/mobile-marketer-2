<?php

$criteria = new CDbCriteria;
$criteria->order = 'last_name ASC';
$criteria->addCondition('customer_main_id='.$data->id.' AND active=1');
$dataProvider= new CActiveDataProvider('User',array(
    'criteria'=>$criteria,
));
$gridColumns = array(
    'first_name',
    'last_name',
    'email',
    'title',
    'active:boolean',
    array(
        'htmlOptions' => array('nowrap'=>'nowrap'),
        'class'=>'bootstrap.widgets.TbButtonColumn',
        'template'=>'{update} {login}',
        'buttons'=>array(
            'update'=>array(
                'url'=>'Yii::app()->createUrl("/user/update",array("id"=>$data->id))',
            ),
            'login'=>array(
                'url'=>'Yii::app()->createUrl("/site/impersonate",array("id"=>$data->id))',
            	'visible'=>'$data->id!=Yii::app()->user->id &&  stristr($data->roles, "admin")==false',
            ),
        )
    )
);

$this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider'=>$dataProvider,
    //'template'=>"{items}",
    'columns'=>$gridColumns,
));