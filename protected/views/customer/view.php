<?php

// First character messed up on Customer List - Products Drop down.
define('__PADDING__', '&nbsp;&nbsp;&nbsp;&nbsp;');

$this->breadcrumbs=array(
    'Organizations'=>array('customer/index'),
    $model->name,
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
                <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
            <?php }
    }?>
</div>

<h1><?php echo $model->name; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','update'),
    'icon' =>'pencil white',
    'type'=>'primary',
    'url'=>array(
        'customer/update',
        'id'=>$model->id,
    )));?>

<?php
//if(Yii::app()->user->checkAccess('admin')){
    $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'New User',
        'icon'=>'user white',
        'type'=>'primary',
        'url'=>array(
            'user/create',
            'id'=>$model->id,
        )
    ));
//}
?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
'label'=>'New Product',
'icon'=>'tags white',
'type'=>'primary',
'url'=>array(
'product/create',
'id'=>$model->id,
)));?>

<?php
if(Yii::app()->user->checkAccess('admin')){
    if ($model->active) {
        echo CHtml::link(Yii::t('core','<i class="icon-minus icon-white"></i> Set Inactive'),array('delete','id'=>$model->id),array(
            'class'=>'btn btn-danger',
            'confirm'=>Yii::t('core','Are you sure you want to delete this customer?'),
        ));
    } else {
        echo CHtml::link(Yii::t('core','<i class="icon-plus icon-white"></i> Set Active'),array('active','id'=>$model->id),array(
            'class'=>'btn btn-success',
            'confirm'=>Yii::t('core','Are you sure you want to activate this customer?'),
        ));
    }
}
?>
<br/>
<br/>
<?php
$panels = array(
            __PADDING__.'Products'=>$this->renderPartial('_products',$model,true),
            __PADDING__.'Keywords'=>$this->renderPartial('_keywords',$model,true),
        );
//if(Yii::app()->user->checkAccess('admin')){
    $panels = array_merge($panels, array(__PADDING__.'Users'=>$this->renderPartial('_users',$model,true), __PADDING__.'Special Events'=>''));
//}
$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>$panels,
    'options'=>array(
        'collapsible'=>true,
        'active'=>false
    )
));
?>