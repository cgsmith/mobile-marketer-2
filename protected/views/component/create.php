<?php
$this->breadcrumbs=array(
	'Components'=>array('index'),
	'Create',
);

?>

<h1>Create Components for <?php echo $product->name;?></h1>
<?php
if(Yii::app()->user->hasFlash('notice')) { ?>
<div class="flash-notice"><?php echo Yii::app()->user->getFlash('notice'); ?></div>
<?php } ?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>