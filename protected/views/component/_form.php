<?php 
/* @var $this ComponentController */
/* @var $model Component */
//    $form_params = array('id'=>$model->id);
    $formsettings = array(
	'id'=>'component-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    );
    if(isset($ajax)){
        $formsettings = array_merge($formsettings, array('action'=>$this->createUrl('/product/components', array('id'=>$model->product_id))));
    }
    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',$formsettings);
?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>
	<p class="help-block"><small>&#8224;Postage is setup in the configuration.</small></p>

	<?php echo $form->errorSummary($model); ?>

<?php
$gridColumns = array(
    array('name'=>'sequence', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 30px')),
    array('name'=>'name', 'header'=>'Name'),
    array('name'=>'component_type_id', 'header'=>'Type'),
    array(
        'htmlOptions' => array('nowrap'=>'nowrap'),
        'class'=>'bootstrap.widgets.TbButtonColumn',
        'viewButtonUrl'=>null,
        'updateButtonUrl'=>null,
        'deleteButtonUrl'=>null,
    )
);

?>
    <?php echo $form->hiddenField($model,'product_id'); ?>

<?php
//TODO: This should not be in the view like this
$criteria = new CDbCriteria();
$criteria->order = 'type ASC';?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>150)); ?>

    <?php echo $form->dropDownListRow($model,'component_type_id',CHtml::listData(
            ComponentType::model()->findAll($criteria,
                array('order'=>'type')), 'id','type'),
                array('empty'=>'--select--','class'=>'span5')); ?>

    <label for="Component_sequence" class="required">Sequence <span class="required">*</span></label>
    <input class="span5" type="number" name="Component[sequence]" min="0" step=".01" value="<?php echo $model->sequence;?>">

    <?php echo $form->textAreaRow($model,'description',array('class'=>'span5','maxlength'=>150)); ?>

    <div id="document_file_wrapper"><?php echo $form->fileFieldRow($model,'document_id',array('class'=>'span5')); ?>

        <?php 
        if (Yii::app()->controller->id == 'product' || Yii::app()->controller->id == 'component') {
            $this->widget('bootstrap.widgets.TbButton', array(
                'label'=>Yii::t('core','Source'),
                'size'=>'mini',
                'url'=>array(
                    'component/document',
                    'document_id'=>$model->document_id,
                )));

            $this->componentMergeButton($model->product->customer_main_id,$model->id);
        }
        ?>
		<?php if($model->document_id !=null):?>
			<div class="row-fluid">
				<div id='file-info' class="span8" style="text-align: center;">
					File Uploaded:<span class="label label-info"><?php echo $model->componentDocument->name?></span>
				</div>
			</div>
	<?php endif;?>
    </div>
    <div id="content_email_wrapper" style="display:none;">
    <?php echo $form->labelEx($model,'email_content'); ?>
    <?php $this->widget('ImperaviRedactorWidget', array(
        // you can either use it for model attribute
        'model' => $model,
        'attribute' => 'email_content',
    ));
    ?>
    <?php echo $form->error($model,'email_content'); ?>
    </div>

    <?php echo $form->dropDownListRow($model,'settings_postage_id',CHtml::listData(
            SettingsPostage::model()->findAll(
                array('order'=>'name')),'id','name'),
                array('empty'=>'--select--','class'=>'span3')); ?>

	<?php echo $form->textFieldRow($model,'labor_cost',array('class'=>'span1','maxlength'=>6)); ?>

	<?php echo $form->textFieldRow($model,'material_cost',array('class'=>'span1','maxlength'=>6)); ?>

	<?php echo $form->textFieldRow($model,'postage_override',array('class'=>'span1','maxlength'=>6)); ?>

	<?php echo $form->textFieldRow($model,'price',array('class'=>'span1','maxlength'=>6)); ?>
        <?php if(isset($model->id)) echo CHtml::hiddenField('Component[id]', $model->id); ?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
        <?php echo CHtml::link(Yii::t('core','delete'),array('delete','id'=>$model->id),array(
            'class'=>'btn btn-danger',
            'confirm'=>'Deleting this component will effect orders that are placed with it! Are you sure you want to delete? This cannot be undone.',
        )) ?>
	</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    $('#Component_component_type_id').on('change', function(e){
        if($(this).val()=='8'){
            $('#document_file_wrapper').hide();
            $('#content_email_wrapper').show();
        } else {
            $('#document_file_wrapper').show();
            $('#content_email_wrapper').hide();
        }
    });
    <?php if($model->getComponentType()=='8'){ ?>
        $('#document_file_wrapper').hide();
        $('#content_email_wrapper').show();
    <?php } ?>
</script>