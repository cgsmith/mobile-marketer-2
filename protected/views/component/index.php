<?php
$this->breadcrumbs=array(
	'Components',
);

$this->menu=array(
	array('label'=>'Create Component','url'=>array('create')),
	array('label'=>'Manage Component','url'=>array('admin')),
);
?>

<h1>Components</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
