<?php
/* @var $this UserReportsController */
/* @var $form TbActiveForm */
/* @var $model UseremailReports */
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'userreports-email-form',
    'enableAjaxValidation'=>true,
    'type'=>'horizontal',
));
?>
<h2>Mobile Marketer Reports</h2>
<div id="report-dates-container">
    <?php $date = array('from'=> date('m/d/Y', time()-(7*24*60*60)), 'to'=>date('m/d/Y', time())); ?>
    <div class="control-group ">
        <label for="Date_from" class="control-label">Start Date: </label>
        <div class="controls">
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'Date[from]',
                // additional javascript options for the date picker plugin
                'options'=>array(
                    'showAnim'=>'fold',
                ),
                'value'=>$date['from'],
                'htmlOptions'=>array(
                    'style'=>'height:20px;'
                ),
            ));
            ?>
        </div>
    </div>

    <div class="control-group ">
        <label for="Date_to" class="control-label">End Date: </label>
        <div class="controls">
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'name'=>'Date[to]',
                // additional javascript options for the date picker plugin
                'options'=>array(
                    'showAnim'=>'fold',
                ),
                'value'=>$date['to'],
                'htmlOptions'=>array(
                    'style'=>'height:20px;'
                ),
            ));
            ?>
        </div>
    </div>
</div>
<div class="control-group ">
    <label class="control-label">&nbsp;</label>
    <div class="controls">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Show')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    $('form#userreports-email-form').on('submit', function(e){
        e.preventDefault();
            var from = new Date($('input#Date_from').val());
            var to = new Date($('input#Date_to').val());
            var date_from = from.valueOf()/1000;
            var date_to = to.valueOf()/1000;



            var report_url = '<?php echo $this->createAbsoluteUrl('userreports/useremail') ?>/date_from/'+date_from+"/date_to/"+date_to;
            window.open(report_url, 'Report', 'fullscreen=yes,width=600,height=400,scrollbars=yes');

    });
</script>