<?php

/**
 * This class allows for the replacement of variables within a pptx file
 * 
 * @category   Phpdocx
 * @package    Batch Processing
 * @copyright  Copyright (c) 2009-2013 Narcea Producciones Multimedia S.L.
 *             (http://www.2mdc.com)
 * @license    http://www.phpdocx.com/wp-content/themes/lightword/pro_license.php
 * @version    2013.03.25
 * @link       http://www.phpdocx.com
 * @since      2013.03.25
 */

require_once dirname(__FILE__) . '/CreateDocx.inc';

class PPTXUtilities
{
    /**
     * @var array
     * @access private
     */
    private $_notesArray = array();
    /**
     * @var bool
     * @access private
     */
    private $_parseNotes = false;
    /**
     * @var DOMDocument
     * @access private
     */
    private $_pptxContentTypesDOM;
    /**
     * @var array
     * @access private
     */
    private $_pptxSlidesRelsDOM = array();
    /**
     * @var array
     * @access private
     */
    private $_pptxSlidesRelsXML = array();
    /**
     * @var array
     * @access private
     * @static
     */
    private static $_pptxSlidesDOM = array();
    /**
     * @var array
     * @access private
     * @static
     */
    private static $_pptxSlidesNotesDOM = array();
    /**
     * @var array
     * @access private
     * @static
     */
    private static $_pptxSlidesNotesXML = array();
    /**
     * @var array
     * @access private
     * @static
     */
    private static $_pptxSlidesXML = array();
    /**
     * @var array
     * @access private
     */
    private $_slidesArray = array();
    /**
     * @var array
     * @access private
     */
    private $_templateContent = array();
    /**
     * @var string
     * @access private
     * @static
     */
    private static $_templateSymbol = '$';
    /**
     * @var ZipArchive
     * @access private
     */
    private $_templateZip;

 
 
    /**
     * Class constructor
     * @param string $pptx
     * @param array $options, 
     * Values:
     * 'parseNotes' (bool)
     * 'templateSymbol' (string)
     */
    public function __construct($pptx, $options = array()) 
    {
        if(isset($options['templateSymbol'])){
            self::$_templateSymbol = $options['templateSymbol'];
        }
        if( isset($options['parseNotes']) && $options['parseNotes'] === true){
            $this->_parseNotes = true;
        }
        $this->_templateContent = array();
        $this->extractTemplateFiles($pptx);
        //var_dump($this->_templateContent);
        
        //Extract the required files from the ppt zip folder
        //pptx xml.rels
        $this->_pptxRelsDOM = simplexml_load_string($this->_templateContent['ppt/_rels/presentation.xml.rels']);
        $this->_pptxRelsDOM->registerXPathNamespace('rels', 'http://schemas.openxmlformats.org/package/2006/relationships');
        $this->_slidesArray = $this->getSlidesArray($this->_pptxRelsDOM);
        
        //slides(n).xml
        self::$_pptxSlidesXML = array();
        $this->_pptxSlidesRelsXML = array();
        self::$_pptxSlidesDOM = array();
        $this->_pptxSlidesRelsDOM = array();
        foreach($this->_slidesArray as $key => $slide){
            self::$_pptxSlidesXML[$key] = $this->_templateContent['ppt/' . $slide];     
            //extract the slide name from the path
            $nameArray = explode('/', $slide);
            $slideName = substr($nameArray[1], 0 , -4);
            $this->_pptxSlidesRelsXML[$key] = $this->_templateContent['ppt/slides/_rels/' . $slideName . '.xml.rels'];
        }
        if($this->_parseNotes){
          $this->_pptxContentTypesDOM = simplexml_load_string($this->_templateContent['[Content_Types].xml']);
          $this->_pptxContentTypesDOM->registerXPathNamespace('ct', 'http://schemas.openxmlformats.org/package/2006/content-types');
          $query = '//ct:Override[@ContentType="application/vnd.openxmlformats-officedocument.presentationml.notesSlide+xml"]';
          $slideNotes = $this->_pptxContentTypesDOM->xpath($query);
          foreach($slideNotes as $note){
            $path = $note['PartName'];
            $path = substr($path, 1);
            self::$_pptxSlidesNotesXML[$path] = $this->_templateContent[$path];
            $this->_notesArray[] = $path;
          }
        }
    }
    /**
     * Class destructor
     */
    public function __destruct() 
    {
        
    }
    
    /**
     * Generates a pptx out of the parsed files
     *
     * @access public
     * @param array $docxContents
     * @param string $path
     * @return void
     */
    public function createPPTX($path)
    {
        if(file_exists($path)){
            PhpdocxLogger::logger('You are trying to overwrite an existing file', 'info');
        }
        try {
            $zipDocx = new ZipArchive();
            $createZip = $zipDocx->open($path . '.pptx', ZipArchive::CREATE);
            if ($createZip !== true) {
                throw new Exception('Error trying to generate a pptx form template: please, check the path and/or writting permissions');
            }                                           
        }
        catch (Exception $e) {
            PhpdocxLogger::logger($e->getMessage(), 'fatal');
        }
        
        foreach(self::$_pptxSlidesDOM as $key => $dom){
            $this->_templateContent['ppt/' . $this->_slidesArray[$key]] = $dom->asXML();
        }
        
        if($this->_parseNotes){
            foreach($this->_notesArray as $noteNumber => $notePath){
               $this->_templateContent[$notePath] = self::$_pptxSlidesNotesDOM[$notePath]->asXML(); 
            }
        }
        
        //Insert all files in zip
        foreach ($this->_templateContent as $key => $value) {
            $zipDocx->addFromString($key, $value);
        }   
        //Close zip
        $zipDocx->close();
        
    }
   
    /**
     * This is the main class method that does all the needed manipulation to
     * replace all variables in the pptx template
     * @access public
     * @example ../examples/easy/ReplaceVariablesPPTX.php
     * @param string $type can be text, list, table, image or chart
     * @param array $data an array with variable names and values
     * @return void
     */
    public function replacePPTXTemplateVariable($type, $data)            
    {      
        switch ($type){
            case 'text':
                $this->replaceTextVariable($data);
                break;
            case 'table':
                $this->replaceTableVariable($data);
                break;
            case 'list':
                $this->replaceListVariable($data);
                break;
            case 'image':
                $this->replaceImageVariable($data);
                break;
         }   
  
    }
    
    /**
     * Extracts all the contents from the template file
     *
     * @access private
     * @param string $template
     * @return void
     */
    private function extractTemplateFiles($template)
    {
        $this->_templateZip = new ZipArchive();
        try {
            $openTemplate = $this->_templateZip->open($template);
            if ($openTemplate !== true) {
                throw new Exception('Error while opening the template: please, check the path');
            }                                           
        }
        catch (Exception $e) {
            PhpdocxLogger::logger($e->getMessage(), 'fatal');
        }
        // read each file and create a new array of contents
        for ($i = 0; $i < $this->_templateZip->numFiles; $i++) {
                $this->_templateContent[$this->_templateZip->getNameIndex($i)] = 
                $this->_templateZip->getFromName($this->_templateZip->getNameIndex($i));
            }
    }
        
    /**
     * Generates an array with the slides names
     *
     * @access private
     * @param SimpleXML $relsDOM
     * @return array
     */
    private function getSlidesArray($relsDOM)
    {
        $slidesArray = array();
        $query = '//rels:Relationship[@Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide"]';
        $slideRels = $relsDOM->xpath($query);
        foreach($slideRels as $slide){
           $slidesArray[] = (string) $slide['Target']; 
        }
        return $slidesArray;
    }   
    
    /**
     * Prepares a single PHPDocX variable for substitution
     *
     * @access private
     * @param string $var
     * @param string $content
     * @return string
     */
    private function repairSingleVariable($var, $content)
    {
        $documentSymbol = explode(self::$_templateSymbol, $content);
		foreach ($documentSymbol as $documentSymbolValue) {
			$tempSearch = trim(strip_tags($documentSymbolValue));			
			if ($tempSearch == $var) {
				$content = str_replace(	$documentSymbolValue, $var, $content);
			}
		}
        return $content;
    }
    
    /**
     * Replaces the chart data
     *
     * @access public
     * @param $slideNumber (int)
     * @param array $chartData which key is the slide number to replace and the values is an array of values
     * WARNING: we are assuming there is only one chart per slide. 
     * @return void
     */
    public function replaceChartData($slideNumber, $chartData)
    {
        $presentationDOM = simplexml_load_string($this->_templateContent['ppt/presentation.xml']);
        $presentationDOM->registerXPathNamespace('a', 'http://schemas.openxmlformats.org/drawingml/2006/main');
        $presentationDOM->registerXPathNamespace('p', 'http://schemas.openxmlformats.org/presentationml/2006/main');
        $presentationDOM->registerXPathNamespace('r', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships');
        //Get the slide list
        $query = '//p:sldIdLst/p:sldId';
        $slideList = $presentationDOM->xpath($query);
        if($slideNumber > count($slideList) || $slideNumber < 1 ){
            PhpdocxLogger::logger('There is no such slide number', 'fatal');
        }
        $slideId = (string) $slideList[$slideNumber - 1]->attributes('http://schemas.openxmlformats.org/officeDocument/2006/relationships')->id;
        //With the slide Id get the slide name
        $relsDOM = simplexml_load_string($this->_templateContent['ppt/_rels/presentation.xml.rels']);
        $relsDOM->registerXPathNamespace('rels', 'http://schemas.openxmlformats.org/package/2006/relationships');
        $query = '//rels:Relationship[@Id="' . $slideId . '"]';
        $nodeSlide = $relsDOM->xpath($query);
        $slideName = (string) $nodeSlide[0]['Target'];
        $slideNameArray = explode('/', $slideName);
        $slideName = substr($slideNameArray[1], 0, -4);
        //Get the info about the corresponding chart
        $relsChartDOM = simplexml_load_string($this->_templateContent['ppt/slides/_rels/' . $slideName . '.xml.rels']);
        $relsChartDOM->registerXPathNamespace('rels', 'http://schemas.openxmlformats.org/package/2006/relationships');
        $query = '//rels:Relationship[@Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart"]';
        $relsChartNodes = $relsChartDOM->xpath($query); 
        if(is_array($relsChartNodes) && count($relsChartNodes) > 0){          
            $chartName = substr( (string) $relsChartNodes[0]['Target'], 3);
            $chartId = substr( (string) $relsChartNodes[0]['Id'], 3);
            //Part of this code is taken from DocxUtilities
            $domChart = new DOMDocument();
            $domChart->loadXML($this->_templateContent['ppt/' . $chartName]);

            $xmlWP = $domChart->getElementsByTagNameNS(
                'http://schemas.openxmlformats.org/drawingml/2006/chart',
                'plotArea'
            );
            $nodePlotArea = $xmlWP->item(0);

            foreach ($nodePlotArea->childNodes as $node){
                if(strpos($node->nodeName, 'Chart') !== false){
                    list($namespace, $type) = explode(':', $node->nodeName);
                    break;
                }
            }
            $graphic = CreateChartFactory::createObject($type);
            $onlyData = $graphic->prepareData($chartData);

            $tags = $graphic->dataTag();

            $xpath = new DOMXPath($domChart);
            $xpath->registerNamespace('c', 'http://schemas.openxmlformats.org/drawingml/2006/chart'); 
            $i = 0;
            foreach($tags as $tag){
                $query = '//c:' . $tag . '/c:numRef/c:numCache/c:pt/c:v';
                $xmlGraphics = $xpath->query($query, $domChart);
                foreach ($xmlGraphics as $entry) {
                    $entry->nodeValue = $onlyData[$i];
                    $i++;
                }
            }
            $chartXml = $domChart->saveXML();
            $this->_templateContent['ppt/' . $chartName] = $chartXml;
            $chartNameArray = explode('/', $chartName);
            $shortChartName = substr($chartNameArray[1], 0, -4);
            
            $charRelsDOM = simplexml_load_string($this->_templateContent['ppt/charts/_rels/' . $shortChartName . '.xml.rels']);
            $charRelsDOM->registerXPathNamespace('rels', 'http://schemas.openxmlformats.org/package/2006/relationships');
            $query = '//rels:Relationship[@Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/package"]';
            $xslxNodes = $charRelsDOM->xpath($query);
            $originalXSLX = (string) $xslxNodes[0]['Target'];
            $originalXSLX = substr($originalXSLX, 3);
            unset($this->_templateContent['ppt/' . $originalXSLX]);
            $xslxNodes[0]['Target'] = '../embeddings/datos'. $chartId .'.xlsx';
            $this->_templateContent['ppt/charts/_rels/' . $shortChartName . '.xml.rels'] = $charRelsDOM->asXML();
                    
            //prepare the new excel file
            $excel = $graphic->getXlsxType();

            if($excel->createXlsx('datos'. $chartId .'.xlsx', $chartData)){
                $this->_templateContent['ppt/embeddings/datos'. $chartId.'.xlsx']= file_get_contents('datos'. $chartId .'.xlsx');
            }
            unlink('datos'. $chartId.'.xlsx');
        }
    }
    
    /**
     * Replaces the images associated to an array of variable
     *
     * @access private
     * @param array $data
     * @return void
     */
    private function replaceImageVariable($data)
    {
       foreach($data as $var => $val){
            $this->replaceSingleImageVariable($var, $val);
       } 
    }

    /**
     * Replaces a list of variables by arrays of data
     *
     * @access private
     * @param array $data
     * @return void
     */
    private function replaceListVariable($data)
    {
         foreach($data as $var => $val){
            $this->replaceSingleListVariable($var, $val);
        } 
    }
    
        
    /**
     * Replaces a single image associated to a variable
     *
     * @access private
     * @param string $var
     * @param string $val
     * @return void
     */
    private function replaceSingleImageVariable($var, $val)
    { 
        $search = self::$_templateSymbol . $var . self::$_templateSymbol;
        $query = '//p:pic[p:nvPicPr/p:cNvPr[@descr="' . $search . '"]]';
        foreach(self::$_pptxSlidesXML as $key => $slide){
            self::$_pptxSlidesDOM[$key] = simplexml_load_string(self::$_pptxSlidesXML[$key]);
            //$ns = self::$_pptxSlidesDOM[$key]->getNamespaces(true);
            //var_dump($ns);
            self::$_pptxSlidesDOM[$key]->registerXPathNamespace('a', 'http://schemas.openxmlformats.org/drawingml/2006/main');
            self::$_pptxSlidesDOM[$key]->registerXPathNamespace('r', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships');
            self::$_pptxSlidesDOM[$key]->registerXPathNamespace('p', 'http://schemas.openxmlformats.org/presentationml/2006/main');
            //Ther is a bug in simplexml with namspaces that are declared but not used in the XML file
            @$imageNodes = self::$_pptxSlidesDOM[$key]->xpath($query);
            if(is_array($imageNodes) && count($imageNodes)> 0){
                $image = dom_import_simplexml($imageNodes[0]);
                $blip = $image->getElementsByTagNameNS('http://schemas.openxmlformats.org/drawingml/2006/main', 'blip')->item(0);
                $id = $blip->getAttribute('r:embed');
                $query = '//rels:Relationship[@Id="' . $id . '"]';
                $dom = simplexml_load_string($this->_pptxSlidesRelsXML[$key]);
                $dom->registerXPathNamespace('rels', 'http://schemas.openxmlformats.org/package/2006/relationships');
                $imagePath = $dom->xpath($query);
                $path = (string) $imagePath[0]['Target'];
                 $path = substr($path, 3);
                $this->_templateContent['ppt/' . $path] = file_get_contents($val);
            }
        }
    }
        
    /**
     * Replaces a single variable by a list of items
     *
     * @access private
     * @param string $var
     * @param string $val
     * @return void
     */
    private function replaceSingleListVariable($var, $val)
    {
        $search = self::$_templateSymbol . $var . self::$_templateSymbol;
        self::$_pptxSlidesDOM = array();
        foreach(self::$_pptxSlidesXML as $key => $slide){
            self::$_pptxSlidesXML[$key] = $this->repairSingleVariable($var, self::$_pptxSlidesXML[$key]);
            self::$_pptxSlidesDOM[$key] = simplexml_load_string(self::$_pptxSlidesXML[$key]);
      
            $query = '//a:p[a:r/a:t[text()[contains(., "' . $search . '")]]]';
            $foundNodes = self::$_pptxSlidesDOM[$key]->xpath($query);
            foreach($foundNodes as $node){
               $domNode = dom_import_simplexml($node);
               foreach($val as $key2 => $value){
                   $newNode = $domNode->cloneNode(true);
                   $textNodes = $newNode->getElementsBytagName('t');
                   foreach($textNodes as $text){
                       $sxText = simplexml_import_dom($text);
                       $strNode = (string) $sxText;
                       $strNode = str_replace($search, $value, $strNode);
                       $sxText[0] = $strNode;
                   }
                   $domNode->parentNode->insertBefore($newNode, $domNode);
               }
               $domNode->parentNode->removeChild($domNode);
            }
            self::$_pptxSlidesXML[$key] = self::$_pptxSlidesDOM[$key]->saveXML();
        }   
    }
    
    /**
     * Do the actual substitution of the variable for its corresponding text
     *
     * @access private
     * @param string $var
     * @param array $val
     * @return void
     */
    private function replaceSingleTextVariable($var, $val)
    {
        $search = self::$_templateSymbol . $var . self::$_templateSymbol;
        self::$_pptxSlidesDOM = array();
        foreach(self::$_pptxSlidesXML as $key => $slide){
            self::$_pptxSlidesXML[$key] = $this->repairSingleVariable($var, self::$_pptxSlidesXML[$key]);
            self::$_pptxSlidesDOM[$key] = simplexml_load_string(self::$_pptxSlidesXML[$key]);
            $query = '//a:t[text()[contains(., "' . $search . '")]]';
            $foundNodes = self::$_pptxSlidesDOM[$key]->xpath($query);
            foreach($foundNodes as $node){
                $strNode = (string) $node;
                $strNode = str_replace($search, $val, $strNode);
                $node[0] = $strNode;
            }
            self::$_pptxSlidesXML[$key] = self::$_pptxSlidesDOM[$key]->saveXML();
        }
        if($this->_parseNotes){
            self::$_pptxSlidesNotesDOM = array();
            foreach(self::$_pptxSlidesNotesXML as $key => $slide){
                self::$_pptxSlidesNotesXML[$key] = $this->repairSingleVariable($var, self::$_pptxSlidesNotesXML[$key]);
                self::$_pptxSlidesNotesDOM[$key] = simplexml_load_string(self::$_pptxSlidesNotesXML[$key]);
                $query = '//a:t[text()[contains(., "' . $search . '")]]';
                $foundNodes = self::$_pptxSlidesNotesDOM[$key]->xpath($query);
                foreach($foundNodes as $node){
                    $strNode = (string) $node;
                    $strNode = str_replace($search, $val, $strNode);
                    $node[0] = $strNode;
                }
                self::$_pptxSlidesNotesXML[$key] = self::$_pptxSlidesNotesDOM[$key]->saveXML();
            }
        }
    }
    
    /**
     * Replaces the variables within a table
     *
     * @access private
     * @param array $data
     * @return void
     */ 
    private function replaceTableVariable($data)
    {
        $varKeys = array();
        
        //Get the variable names from the first row
        foreach($data[0] as $var => $varData){
            $varKeys[] = $var;
        }
        //Prepare the query
        $search = array();
        for($j = 0; $j < count($varKeys); $j++){
            $search[$j] = self::$_templateSymbol . $varKeys[$j] . self::$_templateSymbol;
        }
        $queryArray = array();
        for($j = 0; $j < count($search); $j++){
            $queryArray[$j] = '//a:tr[a:tc/a:txBody/a:p/a:r/a:t[text()[contains(., "' . $search[$j] . '")]]]';
        }
        $query = join(' | ', $queryArray);
        self::$_pptxSlidesDOM = array();
        foreach(self::$_pptxSlidesXML as $key => $slide){
            //Prepare each of the variable we have to replace in each slide
            for($j = 0; $j < count($varKeys); $j++){
                self::$_pptxSlidesXML[$key] = $this->repairSingleVariable($varKeys[$j], self::$_pptxSlidesXML[$key]);
            }
            self::$_pptxSlidesDOM[$key] = simplexml_load_string(self::$_pptxSlidesXML[$key]);
            
            $foundNodes = self::$_pptxSlidesDOM[$key]->xpath($query);
            $rowNumber = 0;
            foreach($data as $key2 => $rowValue){
                foreach($foundNodes as $node){
                   $domNode = dom_import_simplexml($node);
                   if($rowNumber == 0){
                      $referenceNode = $domNode;
                   } 
                   $rowNumber++;
                   //$vals = explode('##', $rowValue);
                   $newNode = $domNode->cloneNode(true);
                   $textNodes = $newNode->getElementsBytagName('t');
                   foreach($textNodes as $text){
                       for($k = 0; $k < count($search); $k++){
                           $sxText = simplexml_import_dom($text);
                           $strNode = (string) $sxText;
                           $strNode = str_replace($search[$k], $rowValue[$varKeys[$k]], $strNode);
                           $sxText[0] = $strNode;
                       }
                   }
                   $domNode->parentNode->insertBefore($newNode, $referenceNode);
               }
            }
  
            //Remove the original nodes
            foreach($foundNodes as $node){
                   $domNode = dom_import_simplexml($node);
                   $domNode->parentNode->removeChild($domNode);
            }
            
            self::$_pptxSlidesXML[$key] = self::$_pptxSlidesDOM[$key]->saveXML();
        }
    }
    
    /**
     * Loops over the text variables to replace
     *
     * @access private
     * @param array $data
     * @return void
     */
    private function replaceTextVariable($data)
    {
        foreach($data as $var => $val){
            $this->replaceSingleTextVariable($var, $val);
        }
    }
        
}
