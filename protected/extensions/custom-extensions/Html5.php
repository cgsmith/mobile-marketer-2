<?php 
/**
 * 
 * Html5 extends the CHtml helper class to allow you to override 
 * the HTML INPUT type attribute directly in the html options parameter.
 * This class can be modified for more Html5 features. 
 *
 */
class Html5 extends CHtml
{
	public static function textField($name,$value='',$htmlOptions=array()){
		self::clientChange('change',$htmlOptions);
		$type = isset($htmlOptions['type']) && !empty($htmlOptions['type']) ? $htmlOptions['type'] : "text";
		if($model->isAttributeRequired($attribute))
				$htmlOptions['required']='required';
		$htmlOptions['placeholder']=$model->getAttributeLabel($attribute);
		return self::inputField($type,$name,$value,$htmlOptions);
	}
	
	public static function passwordField($name,$value='',$htmlOptions=array()){
		self::clientChange('change',$htmlOptions);
		$type = isset($htmlOptions['type']) && !empty($htmlOptions['type']) ? $htmlOptions['type'] : "password";
		if($model->isAttributeRequired($attribute))
				$htmlOptions['required']='required';
		$htmlOptions['placeholder']=$model->getAttributeLabel($attribute);
		return self::inputField($type,$name,$value,$htmlOptions);
	}

	public static function activeTextField($model,$attribute,$htmlOptions=array()){
		self::resolveNameID($model,$attribute,$htmlOptions);
		self::clientChange('change',$htmlOptions);
		$type = isset($htmlOptions['type']) && !empty($htmlOptions['type']) ? $htmlOptions['type'] : "text";
		if($model->isAttributeRequired($attribute))
				$htmlOptions['required']='required';
		$htmlOptions['placeholder']=$model->getAttributeLabel($attribute);
		return self::activeInputField($type,$model,$attribute,$htmlOptions);
	}
}