<?php

/**
 * Sign
 *
 * @category   Phpdocx
 * @package    loader
 * @copyright  Copyright (c) 2009-2011 Narcea Producciones Multimedia S.L.
 *             (http://www.2mdc.com)
 * @license    http://www.phpdocx.com/wp-content/themes/lightword/pro_license.php
 * @version    2011.08.17
 * @link       http://www.phpdocx.com
 * @since      Class available since Release 2.0
 */
class Sign
{
    private $byterange_string = '/ByteRange[0 ********** ********** **********]';

    private $signature_data = array();

    public function setSignature($signing_cert='', $private_key='', $private_key_password='', $extracerts='', $cert_type=2, $info=array())
    {
        // to create self-signed signature: openssl req -x509 -nodes -days 365000 -newkey rsa:1024 -keyout tcpdf.crt -out tcpdf.crt
        // to export crt to p12: openssl pkcs12 -export -in tcpdf.crt -out tcpdf.p12
        // to convert pfx certificate to pem: openssl
        //     OpenSSL> pkcs12 -in <cert.pfx> -out <cert.crt> -nodes
        $this->sign = true;
        ++$this->n;
        $this->sig_obj_id = $this->n; // signature widget
        ++$this->n; // signature object ($this->sig_obj_id + 1)
        $this->signature_data = array();
        if (strlen($signing_cert) == 0) {
            $signing_cert = 'file://'.dirname(__FILE__).'/tcpdf.crt';
            $private_key_password = 'tcpdfdemo';
        }
        if (strlen($private_key) == 0) {
            $private_key = $signing_cert;
        }
        $this->signature_data['signcert'] = $signing_cert;
        $this->signature_data['privkey'] = $private_key;
        $this->signature_data['password'] = $private_key_password;
        $this->signature_data['extracerts'] = $extracerts;
        $this->signature_data['cert_type'] = $cert_type;
        $this->signature_data['info'] = $info;
    }

    public function signPdf($file) {
        $pdfdoc = file_get_contents($file);
        // remove last newline
        $pdfdoc = substr($pdfdoc, 0, -1);
        // remove filler space
        $byterange_string_len = strlen($this->byterange_string);
        // define the ByteRange
        $byte_range = array();
        $byte_range[0] = 0;
        $byte_range[1] = strpos($pdfdoc, $this->byterange_string) + $byterange_string_len + 10;
        $byte_range[2] = $byte_range[1] + $this->signature_max_length + 2;
        $byte_range[3] = strlen($pdfdoc) - $byte_range[2];
        $pdfdoc = substr($pdfdoc, 0, $byte_range[1]).substr($pdfdoc, $byte_range[2]);
        // replace the ByteRange
        $byterange = sprintf('/ByteRange[0 %u %u %u]', $byte_range[1], $byte_range[2], $byte_range[3]);
        $byterange .= str_repeat(' ', ($byterange_string_len - strlen($byterange)));
        $pdfdoc = str_replace($this->byterange_string, $byterange, $pdfdoc);
        // write the document to a temporary folder
        $tempdoc = tempnam('tmppdf_');
        $f = fopen($tempdoc, 'wb');
        $pdfdoc_length = strlen($pdfdoc);
        fwrite($f, $pdfdoc, $pdfdoc_length);
        fclose($f);
        // get digital signature via openssl library
        $tempsign = tempnam('tmpsig_');
        if (empty($this->signature_data['extracerts'])) {
            openssl_pkcs7_sign($tempdoc, $tempsign, $this->signature_data['signcert'], array($this->signature_data['privkey'], $this->signature_data['password']), array(), PKCS7_BINARY | PKCS7_DETACHED);
        } else {
            openssl_pkcs7_sign($tempdoc, $tempsign, $this->signature_data['signcert'], array($this->signature_data['privkey'], $this->signature_data['password']), array(), PKCS7_BINARY | PKCS7_DETACHED, $this->signature_data['extracerts']);
        }
        unlink($tempdoc);
        // read signature
        $signature = file_get_contents($tempsign);
        unlink($tempsign);
        // extract signature
        $signature = substr($signature, $pdfdoc_length);
        $signature = substr($signature, (strpos($signature, "%%EOF\n\n------") + 13));
        $tmparr = explode("\n\n", $signature);
        $signature = $tmparr[1];
        unset($tmparr);
        // decode signature
        $signature = base64_decode(trim($signature));
        // convert signature to hex
        $signature = current(unpack('H*', $signature));
        $signature = str_pad($signature, $this->signature_max_length, '0');
        // Add signature to the document
        $this->buffer = substr($pdfdoc, 0, $byte_range[1]).'<'.$signature.'>'.substr($pdfdoc, $byte_range[1]);
        $this->bufferlen = strlen($this->buffer);
        $f = fopen($name, 'wb');
        fwrite($f, $this->getBuffer(), $this->bufferlen);
        fclose($f);
    }
}
