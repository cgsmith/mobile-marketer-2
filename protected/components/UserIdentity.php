<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    protected $_user;

	/**
	 * Authenticates a user. Checks submitted passwords against database
	 * @return boolean whether authentication succeeds.
	 */
    public function authenticate()
    {
        $record = User::model()->findByAttributes(array('email'=>$this->username, 'active'=>1));
        $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
        $isRecoveryRequest = ($ph->CheckPassword($this->password, $record->reset_password) && $record->reset_password!=null && strtotime($record->reset_password_expiry)>=time());
        if($record===null) {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        } elseif ((md5($this->password) !== $record->password) && (!$ph->CheckPassword($this->password,$record->password)) && (!$ph->CheckPassword($this->password, $record->reset_password) || $record->reset_password==null || strtotime($record->reset_password_expiry)<time())) {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        } else if(!$this->isCustomerActive ($record)){
            throw new CHttpException(403, Yii::t('core', 'Your company is locked out. Contact <strong>'.Yii::app()->params['company'].'</strong> at <strong>'.Yii::app()->params['phone'].'</strong> or <strong>'.Yii::app()->params['web'].'</strong> for more information.'));
        }else{
            //Is this a vanilla hash? Old hash?
            if($record->password{0}!=='$') {
                $record->password = $ph->HashPassword($this->password);
                $record->save(false);
            }
            $this->_id = $record->id; //User id
            $this->setUserStates($record);
            $this->setState('impersonate',false);
            if($isRecoveryRequest){
                Yii::app()->user->setFlash('notice', Yii::t('core', 'You have successfully logged in with your recovery password. You can change your password below.'));
                $this->setState('start_page', 'site/profile/'.$record->id);
            } else {
                $this->setState('start_page', $record->start_page); //Used for start page
            }

            $record->saveAttributes(array(
                'reset_password_expiry' => null,
                'reset_password' => null,
            ));
            if(!isset($record->start_page))
                $this->setState('start_page', 'site/index');
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    protected function setUserStates($record)
    {
        $this->setState('customer_id',$record->customer_main_id); //Customer id
        $this->setState('name', $record->first_name); //First name used for greeting
        $this->setState('company', $record->customerMain->name); //Company for greeting
        $this->setState('roles', $record->roles); //Used for permissions
        $this->setState('pagination', $record->pagination); //Used for pagination on list pages
    }

    protected function logInUser($user,$cancel,$original)
    {
        if($user)
        {
            $this->_user = $user;
            $this->_id=$this->_user->id;
            $this->setUserStates($user);
            if (!$cancel) {
                $this->setState('impersonate',true);
                $this->setState('originalId',$original);
            } else{
                $this->setState('impersonate',false);
            }
            $this->errorCode=self::ERROR_NONE;
        }
    }

    public static function impersonate($userId,$cancel)
    {
        $ui = null;
        $original = Yii::app()->user->id;
        $user = User::model()->findByPk($userId);
        if($user)
        {
            $ui = new UserIdentity($user->email, "");
            $ui->logInUser($user,$cancel,$original);
        }
        return $ui;
    }

    public static function impersonateCancel($userId)
    {
        $ui = null;
        $user = User::model()->findByPk($userId);
        if($user)
        {
            $ui = new UserIdentity($user->email, "");
            $ui->logInUser($user);
        }
        return $ui;
    }

    public function getId()
    {
        return $this->_id;
    }
    
    public function isCustomerActive($user){
        $sql = "SELECT * FROM customer_main WHERE active=1 AND id=:customerId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(':customerId', $user->customer_main_id, PDO::PARAM_INT);
        return $command->execute()==1;
    }
}