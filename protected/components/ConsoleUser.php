<?php
class ConsoleUser extends CApplicationComponent implements IWebUser {
    public $primaryKey;
    private $admin;

    public function init() {
        parent::init();

        if (!isset($this->primaryKey))
            throw new Exception('You must set the "primary key" of the user
                to execute the console application.');

        $user = User::model()->findByPk($this->primaryKey);

        if ($user) {
            if (strstr('admin',$user->roles) == false) { // Check if multiple roles are available
                throw new Exception('User does not have an Admin role.');
            }
            $this->admin = $user;
        }
        else
            throw new Exception('Could not find Admin User to execute console application.');
    }

    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params=array()) {
        return true;
    }
    
    /**
     * Returns a value indicating whether the user is a guest (not authenticated).
     * @return boolean whether the user is a guest (not authenticated)
     */
    public function getIsGuest() {
        return false;
    }
    
    /**
     * Returns a value that uniquely represents the identity.
     * @return mixed a value that uniquely represents the identity (e.g. primary key value).
     */
    public function getId() {
        return $this->admin->id;
    }

    /**
     * Returns the display name for the identity (e.g. username).
     * @return string the display name for the identity.
     */

    public function getName() {
        return $this->admin->username;
    }

    public function loginRequired(){
        
    }

}