<?php
class WebUser extends CWebUser
{
    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params=array())
    {
        if (empty($this->id)) { // Not identified => no rights
            return false;
        }

        $role = $this->getState("roles"); // Get role of user
        if ($role === 'admin') {
            return true; // admin role has access to everything
        }

        if (strstr($operation,$role) !== false) { // Check if multiple roles are available
            return true;
        }

        return ($operation === $role);// allow access if the operation request is the current user's role
    }

    /**
     * Returns a list of roles to assist with drop down lists on forms
     *
     * @return array List of roles used in the database
     */
    public function listRoles()
    {
        return array('user'=>'User','manager'=>'Manager','prod'=>'Production','admin'=>'Administrator');
    }

    /**
     * @return array flash message keys array
     */
    public function getFlashKeys()
    {
        $counters=$this->getState(self::FLASH_COUNTERS);
        if(!is_array($counters)) return array();
        return array_keys($counters);
    }
    
    public function getCustomerId()
    {
        if(isset($this->customer_id)){
            return $this->customer_id;
        } else {
            return false;
        }
    }

    public function getImpersonate()
    {
        if (isset($this->impersonate) && !empty($this->impersonate)) {
            return $this->impersonate;
        }
    }

    public function getCompany()
    {
        if(isset($this->company) && !empty($this->company)) {
            return $this->company;
        } else {
            return false;
        }
    }
}