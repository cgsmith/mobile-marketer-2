<?php

/**
 * This is the model class for Production Controller.
 *
 */
class Production extends CModel {

    const DOWNLOAD_ZIP = 1;
    const DOWNLOAD_DOCX = 2;
    const NOT_PRINTED = 0;
    const PRINTED = 1;
    public $download_type = self::DOWNLOAD_ZIP;
    public $filter = self::NOT_PRINTED;

//    public function getProductionDate($data,$row){
//        $frequency_days = json_decode($data['frequency_days']);
//        return $production_date = date('m-d-Y',strtotime($data['date_ordered'])+ ($frequency_days[(int)$data['component_sequence']-1] * 24*60*60));
//    }

    public function getProduceLink($data, $row){
//        return '<a target="_blank" href="'.Yii::app()->createUrl('production/produce', array('lead_id'=>$data['lead_id'], 'order_id'=>$data['id'], 'component_id'=>$data['component_id'])).'">Download PDF</a>';
        echo $this->createProduceLink($data);
//        return CHtml::link("Download PDF", Yii::app()->createUrl('production/produce', array('lead_id'=>$data['lead_id'], 'order_id'=>$data['id'], 'component_id'=>$data['component_id'])), array('target'=>'_blank'));
    }

    public function getCheckBox($data, $row){
//        echo '<a target="_blank" href="'.$this->createProduceLink($data).'"><input class="printed" type="checkbox" name="selected[]" id="selected_'.$row.'" value="'.$data['selected'].'" class="select-on-check"></a>';
        echo '<input class="printed" type="checkbox" name="Production[selected][]" id="Production_selected_'.$row.'" value="'.$data['selected'].'" class="select-on-check">';
    }

    protected function createProduceLink($data){
        $text = 'Download PDF';
        if($data['component_type_id']==Component::TYPE_EMAIL){
            $link = Yii::app()->createUrl('production/sendemail', array('lead_id'=>$data['lead_id'], 'order_id'=>$data['id'], 'component_id'=>$data['component_id']));
            $text = 'Send Email';
        } else {
            $link = Yii::app()->createUrl('production/produce', array('lead_id'=>$data['lead_id'], 'order_id'=>$data['id'], 'component_id'=>$data['component_id']));

        }
        return '<a target="_blank" href="'.$link.'">'.$text.'</a>';
    }

    public function getFilterOptions(){
        return array(self::NOT_PRINTED=>'Not Printed', self::PRINTED=>'Printed');
    }


    public function attributeNames(){
        return array();
    }

    public function attributeLabels() {
        return array(
            'id' => 'Order_ID',
            'product_name' => 'Product_name',
            'lead_first_name' => 'First Name',
            'lead_last_name' => 'Last Name',
            'component_name' => 'Component Name',
            'component_sequence' => 'Component Sequence',
            'date_ordered' => 'Date Ordered',
            'product_curr_seq_number' => 'Current Sequence Number',
            'download_type' => 'Download As: ',
            'filter' => 'Filter: ',
        );
    }

    public static function listComponentsOrdered($filter=self::NOT_PRINTED, $component_type=Component::TYPE_ALL, $component_type_not_in=array()){
        $conditions = array();
        if(!Yii::app()->user->checkAccess('admin')){
            $conditions[] = " cu.id = ".Yii::app()->user->id . " ";
        }
        if($component_type!=Component::TYPE_ALL){
            $conditions[] = " c.component_type_id = " . $component_type . " ";
        }
        if(!empty($component_type_not_in)){
            $conditions[] = " c.component_type_id not in (" . implode(',',$component_type_not_in) . ') ';
        }
        $conditions[] = " ol.active = 1 ";
        if($filter==self::PRINTED){
            $sql = "SELECT
                        cl.first AS lead_first_name,
                        cl.last AS lead_last_name,
                        cl.company AS lead_company,
                        c.id AS component_id,
                        c.name AS component_name,
                        c.component_type_id AS component_type_id,
                        ct.type AS component_type,
                        c.sequence AS component_sequence,
                        p.name AS product_name,
                        pt.type AS product_type,
                        f.days AS frequency_days,
                        o.id AS id,
                        o.confirmation_number AS confirmation_number,
                        o.date_ordered AS date_ordered,
                        o.curr_seq_number AS product_curr_seq_number,
                        op.lead_id AS lead_id,
                        cu.first_name AS customer_user_firstname,
                        cu.last_name AS customer_user_lastname,
                        cm.name AS customer_name
                        
                    FROM
                        order_printed op
            			LEFT JOIN `order_leads` ol ON op.lead_id=ol.id 
            			LEFT JOIN `order` o ON op.order_id=o.id
                        LEFT JOIN customer_lead cl ON cl.id = op.lead_id
                        LEFT JOIN component c ON c.id = op.component_id                        
                        LEFT JOIN component_type ct ON ct.id = c.component_type_id
                        LEFT JOIN product p ON p.id = o.product_id
                        LEFT JOIN frequency f ON f.id = p.frequency_id
                        LEFT JOIN product_type pt ON pt.id = p.product_type_id                        
                        LEFT JOIN customer_user cu ON cu.id = o.customer_user_id                        
                        LEFT JOIN customer_main cm ON cm.id = cu.customer_main_id
                    ";
            if(!empty($conditions)){
                $sql .= " WHERE ".  implode(" AND ", $conditions);
            }
        } else {
        	 $sql = "
	        	SELECT
		        	cl.first AS lead_first_name,
		        	cl.last AS lead_last_name,
		        	cl.company AS lead_company,
		        	c.id AS component_id,
		        	c.name AS component_name,
		        	c.component_type_id AS component_type_id,
		        	ct.type AS component_type,
		        	c.sequence AS component_sequence,
		        	p.name AS product_name,
		        	pt.type AS product_type,
		        	f.days AS frequency_days,
		        	o.id AS id,
		        	o.confirmation_number AS confirmation_number,
		        	o.date_ordered AS date_ordered,
		        	o.curr_seq_number AS product_curr_seq_number,
		        	ol.lead_id AS lead_id,
		        	cu.first_name AS customer_user_firstname,
		        	cu.last_name AS customer_user_lastname,
		        	cm.name AS customer_name
	        	FROM
		        	`order` o
		        	LEFT JOIN customer_user cu ON cu.id = o.customer_user_id
		        	LEFT JOIN customer_main cm ON cm.id = cu.customer_main_id
		        	LEFT JOIN product p ON p.id = o.product_id
		        	LEFT JOIN frequency f ON f.id = p.frequency_id
		        	LEFT JOIN order_leads ol ON o.id = ol.order_id
		        	LEFT JOIN product_type pt ON pt.id = p.product_type_id
		        	LEFT JOIN customer_lead cl ON ol.lead_id = cl.id
		        	LEFT JOIN component c ON c.product_id = p.id
		        	LEFT JOIN component_type ct ON ct.id = component_type_id
		        WHERE
        			NOT EXISTS (SELECT NULL FROM order_printed WHERE o.id = order_id AND c.id = component_id AND ol.lead_id = lead_id) 
        	 		";
            if(!empty($conditions)){
                $sql .= " AND ".  implode(" AND ", $conditions);
            }
        }
        //echo $sql;exit;
        return Yii::app()->db->createCommand($sql)->queryAll();
    }
    /**
     * Marks Order component printed and adds record to history and billing table.
     * It then looks if the last component was marked make order inactive.
     *
     * @param integer $order_id
     * @param integer $component_id
     * @param integer $lead_id
     * @return bool
     */

    public function markAsPrinted($order_id, $component_id, $lead_id){
        $success = true;
        $time = time();
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $transaction = $connection->beginTransaction();
        $success &= (bool)$command->insert('order_printed', array(
            'order_id' => $order_id,
            'component_id' => $component_id,
            'lead_id' => $lead_id,
            'date_printed' => date('Y-m-d H:i:s', $time),
        ));
        $order_printed_id = Yii::app()->db->getLastInsertID();
        $success &= (bool)$this->addToBillingAndHistory($order_printed_id, $order_id, $component_id, $lead_id);
        if($success){
            $transaction->commit();
            // checks for the last component marked printed, then make order inactive
            if($this->lastComponent($component_id, $order_id)){
                $this->makeOrderInactive($order_id);
            }
            return true;
        } else {
            $transaction->rollback();
        }
        return false;
    }
    /**
     * Checks if all the order components are printed.
     *
     * @param integer $component_id
     * @param integer $order_id
     * @return bool
     */
    private function lastComponent($component_id, $order_id){
        $component = Component::model()->with(array('product'))->findByPk($component_id);
//        $last_component = Component::model()->last($component->product_id)->find();
//        $product_components = Product::model()->with(array('components'))->findByPk($component->product_id);
        // check if all other components related with this order and product are printed.
//        $all_order_components = Component::model()->findAll(array('product_id'=>$component->product_id));
        $lastComponent = Yii::app()->db->createCommand("SELECT max(sequence)  FROM `component` WHERE `product_id` = ".$component->product_id)->queryScalar();
        $currentSequenceNumber = Yii::app()->db->createCommand("SELECT curr_seq_number FROM `order` WHERE id=".$order_id)->queryScalar(); 
//        echo 'all: '.$all_order_components;
//        echo 'printed:' .$order_components_printed;
        return ($currentSequenceNumber >= $lastComponent);
    }

    /**
     *
     * @param integer $order_id
     * @return bool
     */
    private function makeOrderInactive($order_id){
        return (bool)Order::model()->updateByPk($order_id, array('active'=>0));
    }

    /**
     * Adds to the billing and historical tables
     *
     * @param integer $order_printed_id
     * @param integer $order_id
     * @param integer $component_id
     * @param integer $lead_id
     * @return bool
     */
    public function addToBillingAndHistory($order_printed_id,$order_id,$component_id,$lead_id) {
        $success = true;
        $connection = Yii::app()->db;
        //add to component history
        $cmdComponent = $connection->createCommand();
        $component = Component::model()->with(array('componentDocument', 'componentType', 'postage'))->findByPk($component_id);
        $success &= (bool)$cmdComponent->insert('order_history_components', array(
            'order_printed_id'=>$order_printed_id,
            'seq_number'=>$component->sequence,
            'component_link'=>($component->component_type_id==Component::TYPE_EMAIL)?'':$component->componentDocument->url,
            'component_name'=>$component->name,
            'component_type'=>$component->componentType->type,
        ));

        //add to history lead
        $cmdLead = $connection->createCommand();
        $lead = Lead::model()->findByPk($lead_id);
        $success &= (bool)$cmdLead->insert('order_history_leads', array(
            'order_printed_id'=>$order_printed_id,
            'first'=>$lead->first,
            'last'=>$lead->last,
            'title'=>$lead->title,
            'company'=>$lead->company,
            'address1'=>$lead->address1,
            'address2'=>$lead->address2,
            'city'=>$lead->city,
            'state'=>$lead->state,
            'zip'=>$lead->zip,
            'phone'=>$lead->phone,
            'fax'=>$lead->fax,
            'email'=>$lead->email,
        ));

        //add to history leads
        $cmdUser = $connection->createCommand();
        $order = Order::model()->with(array('customerUser', 'customerUser.customerMain'))->findByPk($order_id);
        $user = $order->customerUser;
        $success &= (bool)$cmdUser->insert('order_history_user', array(
            'order_printed_id'=>$order_printed_id,
            'first'=>$user->first_name,
            'last'=>$user->last_name,
            'title'=>$user->title,
            'company'=>$user->customerMain->name,
            'address1'=>$user->address1,
            'address2'=>$user->address2,
            'city'=>$user->city,
            'state'=>$user->state,
            'zip'=>$user->zip,
            'mailstop'=>$user->mailstop,
            'phone'=>$user->phone,
            'fax'=>$user->fax,
            'email'=>$user->email,
            'portrait'=>$user->headshot_file,
            'signature'=>$user->signature_file,
        ));

        //add to history leads
        $cmdBilling = $connection->createCommand();
        $success &= (bool)$cmdBilling->insert('order_billing', array(
            'order_id'=>$order->id,
            'order_printed_id'=>$order_printed_id,
            'labor'=>$component->labor_cost,
            'material_cost'=>$component->material_cost,
            'postage'=>isset($component->postage) ? $component->postage->cost : '',
            'sale_price'=>$component->price,
            'quantity'=>'',
            'total'=>'',
        ));

        // update order curr_sequence
        $cmdCurrseq = $connection->createCommand();
        // $success &= (bool)Order::model()->updateByPk($order->id, array('curr_seq_number'=>$component->sequence));
        Order::model()->updateByPk($order->id, array('curr_seq_number'=>$component->sequence));
        return $success;
    }

    public static function getMergedDocx($component_file_path, $new_filename, $order, $component, $lead){
        $user = $order->customerUser;
        $user_info[] = array(
            'fn'=>$user->first_name,
            'ln'=>$user->last_name,
            't'=>$user->title,
            'co'=>$user->customerMain->name,
            'a1'=>$user->address1,
            'a2'=>$user->address2,
            'c'=>$user->city,
            's'=>$user->state,
            'z'=>$user->zip,
            'p'=>$user->phone,
            'm'=>$user->mobile,
            'f'=>$user->fax,
            'e'=>$user->email,
        );
        $lead_info[] = array(
            'fn'=>$lead->first,
            'ln'=>$lead->last,
            't'=>$lead->title,
            'co'=>$lead->company,
            'a1'=>$lead->address1,
            'a2'=>$lead->address2,
            'c'=>$lead->city,
            's'=>$lead->state,
            'z'=>$lead->zip,
            'p'=>$lead->phone,
            'f'=>$lead->fax,
            'e'=>$lead->email,
            'notes'=>$lead->notes,
        );


        $mergedFile = $component->mergeFields($component_file_path, $user_info, $lead_info);

        $headshot = dirname(Yii::app()->basePath).DIRECTORY_SEPARATOR.$user->headshot_file;
        $signature = dirname(Yii::app()->basePath).DIRECTORY_SEPARATOR.$user->signature_file;
        $pictures = array();
        if(file_exists($headshot)){
            $pictures['headshot'] = $headshot;
        }
        if(file_exists($signature)){
            $pictures['signature'] = $signature;
        }
        $component->changePictures($mergedFile, $pictures);

        // order_id-product_id-component_id-lead_id.timestamp
//        $new_filename = $order->id.'-'.$order->product->id.'-'.$component->id.'-'.$lead->id.'-'.$time.'docx';
        $destination = dirname(Yii::app()->basePath). DIRECTORY_SEPARATOR . 'components'.DIRECTORY_SEPARATOR.'docx'.DIRECTORY_SEPARATOR.'ordered'.DIRECTORY_SEPARATOR.$new_filename.'.docx';
        copy($mergedFile, $destination);
        unlink($mergedFile); //remove file out of /tmp

        return $destination;
    }

    public static function getMergedDocxAsSingleFile(Array $orders, $download_type=self::DOWNLOAD_ZIP){
        $test = false;
        $time = time();
        $DS = DIRECTORY_SEPARATOR;
        $path_to_components = dirname(Yii::app()->basePath) . $DS . 'components';
        //$merged_filename = $path_to_components.$DS.'docx'.$DS.'ordered'.$DS.'merged'.$DS.'mergedWord'.'-'.$time.'.docx';
        $merged_file_path = $path_to_components.$DS.'docx'.$DS.'ordered'.$DS.'merged';


        $files = array();

        /**
         * Heavy lifting is done here.  This is where the documents are merged.
         */

        foreach($orders as $order){
            list($order_id, $component_id, $lead_id) = explode('-', $order);

            $order = Order::model()->with(array('customerUser', 'product'))->findByPk($order_id); //Pull order detail
            $lead = Lead::model()->findByPk($lead_id); //Pull lead information
            $component = Component::model()->with(array('componentDocument','componentType'))->findByPk($component_id); //Pull Component detail
            if($order==null || $lead==null || $component==null || $component->componentDocument==null) { //Throw error if null
                //cannnot use the exception in Model
                //throw new CHttpException(404, Yii::t('core', 'Cannot find info relted with this order.'));
                Yii::app()->user->setFlash('error', Yii::t('core', 'Cannot find info relted with this order.'));
                return false;
            }
  if ($test) { echo __LINE__.'<br>';
            $arr = get_defined_vars();
            print_r($arr);
        }

            if($component->component_type_id!=Component::TYPE_EMAIL){ //If component is not an EMAIL
                $component_file_path = dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . $component->componentDocument->url;
                if(!file_exists($component_file_path)){
//                    throw new CHttpException(404, Yii::t('core', 'File not found.'));
                    Yii::app()->user->setFlash('error', Yii::t('core', 'File not found for the component id: '. $component->id));
                    return false;
                }
                $new_filename = $lead->first.' '.$lead->last.' - '.$order->id.'-'.$order->product->id.'-'.$component->id.'-'.$lead->id.'-'.$time;
                $new_filename = preg_replace("([^\w\s\d\-_~,;:\[\]\(\]]|[\.]{2,})", '', $new_filename);
                $mergedFile = self::getMergedDocx($component_file_path, $new_filename, $order, $component, $lead);
                $files[] = array('path'=>$mergedFile, 'name'=>$new_filename.'.docx','type'=>$component->componentType->type);
  if ($test) { echo __LINE__.'<br>';
            $arr = get_defined_vars();
            print_r($arr);
        }

            }

        }

        // Download as Single Docx file. Currently is buggy due to the PHPDocx implementation.
        
		// No need for .docx files as required @ 02/06/2014 so commented
  /*       if($download_type==self::DOWNLOAD_DOCX){
            $blank = $merged_file_path.$DS.'blank.docx';
            // Create Merged File
            $count_files = count($files);

            Component::registerPhpDocx();
            $myOptions = array('mergeType' => 0);
            $docx_filename = '';
            $docx_filepath = '';
            for($i=0; $i<$count_files; $i++) {

                $docx_filename = 'mergedWord'.'-'.$time.$i.'.docx';
                $docx_filepath = $merged_file_path.$DS.$docx_filename;

                $newDocx = new MergeDocx();
                $newDocx->merge(
                    $files[$i]['path'],
                    $blank,
                    $docx_filepath,
                    $myOptions
                );
                if($i>0) unlink($blank);
                $blank = $docx_filepath;
            }
            header('Content-Type:application/octet-stream');
            header('Content-disposition: attachment; filename='.$docx_filename);
            header('Content-Length: ' . filesize($docx_filepath));
            readfile($docx_filepath);

            // Download as Zip
        } else if($download_type==self::DOWNLOAD_ZIP){ */

        if ($test) { echo __LINE__.'<br>';}
        if($download_type==self::DOWNLOAD_ZIP){

            if ($test) { echo __LINE__.'<br>';}
            $zip = new ZipArchive;
            $zip_filename = date('ymd-His').'-'. rand(1, 99).'.zip';
            $zip_filepath = $merged_file_path.$DS.$zip_filename;
            $res = $zip->open($zip_filepath, ZipArchive::OVERWRITE);
            if ($res === TRUE) {

                if ($test) { echo __LINE__.'<br>';}
                foreach($files as $file){
                    $zip->addFile($file['path'], $file['type'].'/'.$file['name']);
                }
                $zip->close();
                //            Yii::app()->request->xSendFile($zip_filename,array("mimeType"=>"application/zip","terminate"=>true));
                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename='.$zip_filename);
                header('Content-Length: ' . filesize($zip_filepath));
                readfile($zip_filepath);
                foreach ($files as $file) {
                    unlink($file['path']);
                }

                if ($test) { echo __LINE__.'<br>';}
            } else {
                throw new CHttpException(500, Yii::t('core', 'Some Critical error occured. Unable to compress files, please try again.'));
            }
        } else {
            throw new CHttpException(500, Yii::t('core', 'Inavlid download type selected.'));
        }
    }

    public static function getComponentsOrdered($date_from, $date_to, $filter=self::NOT_PRINTED, $component_type=  Component::TYPE_ALL){
        error_reporting(E_ERROR);
        $list_data = array();
        $components_data = self::listComponentsOrdered($filter, $component_type);
        foreach($components_data as $key => $data){
            $frequency_days = json_decode($data['frequency_days']);
            $production_date_timestamp = strtotime($data['date_ordered'])+ ($frequency_days[(int)$data['component_sequence']-1] * 24*60*60);
            $data['production_date_timestamp'] = $production_date_timestamp;
            $data['date_production'] = date('m-d-Y',$production_date_timestamp);
            if(!empty($date_from) && !empty($date_to)){
                if($production_date_timestamp<strtotime($date_from) || $production_date_timestamp>=strtotime($date_to)){
                    continue;
                }
            }
            $data['user'] = $data['customer_user_firstname'].' '.$data['customer_user_lastname'];
            // order-component-lead
            $data['selected'] = $data['id'].'-'.$data['component_id'].'-'.$data['lead_id'];
            $list_data[] = $data;
        }

        return $list_data;
    }

    public static function sendTestEmail($product_id,$component_id,$emails){
        $lead = Lead::model()->findByPk(1);

        $component = Component::model()->with(array('componentDocument'))->findByPk($component_id);
        $attachments = Yii::app()->db->createCommand()
            ->select('component.name,url,component_document.name AS path')
            ->from('component')
            ->join('component_document','document_id = component_document.id')
            ->where('component_type_id = 12 AND active = 1 AND product_id = :pid AND sequence BETWEEN :start AND :end',
                array(':pid'=>$product_id,
                    ':start'=>$component->sequence,
                    ':end'=>(ceil($component->sequence + 1) - 0.01)
                )
            )
            ->queryAll();

            //error out if components are not found
            if($component==null){
                throw new CHttpException(404, Yii::t('core', 'Cannot find info related with this order.'));
            }
            $user = array( //Just use the first lead as the user as well
                'fn'=>$lead->first,
                'ln'=>$lead->last,
                't'=>$lead->title,
                'co'=>$lead->company,
                'a1'=>$lead->address1,
                'a2'=>$lead->address2,
                'c'=>$lead->city,
                's'=>$lead->state,
                'z'=>$lead->zip,
                'p'=>$lead->phone,
                'f'=>$lead->fax,
                'e'=>$lead->email,
            );
            $l = array(
                'fn'=>$lead->first,
                'ln'=>$lead->last,
                't'=>$lead->title,
                'co'=>$lead->company,
                'a1'=>$lead->address1,
                'a2'=>$lead->address2,
                'c'=>$lead->city,
                's'=>$lead->state,
                'z'=>$lead->zip,
                'p'=>$lead->phone,
                'f'=>$lead->fax,
                'e'=>$lead->email,
                'notes'=>$lead->notes,
            );
            $email_content = $component->processEmail($user, $l); //Merge the HTML documents for Emails

            $message = new YiiMailMessage;
            $message->from = array('noreply@mobilemarketer.net');
            $message->to = $emails;
            $message->subject = $component->name;
            $message->setBody($email_content, 'text/html');
            // live
            foreach ($attachments as $attachment) {
                $ext = explode('.', $attachment['path']);
                $message->attach(Swift_Attachment::fromPath($attachment['url'])->setFilename($attachment['name'].'.'.$ext[1]));
            }

            Yii::app()->mail->send($message);

    }

    public static function sendEmail($order_id, $component_id, $lead_id){
        $order = Order::model()->with(array('customerUser','customerUser.customerMain', 'product'))->findByPk($order_id);
        $lead = Lead::model()->findByPk($lead_id);
		
		if ($lead->email != '') {
        $component = Component::model()->with(array('componentDocument'))->findByPk($component_id);
        $attachments = Yii::app()->db->createCommand()
            ->select('component.name,url,component_document.name AS path')
            ->from('component')
            ->join('component_document','document_id = component_document.id')
            ->where('component_type_id = 12 AND active = 1 AND product_id = :pid AND sequence BETWEEN :start AND :end',
                array(':pid'=>$order->product->id,
                    ':start'=>$component->sequence,
                    ':end'=>(ceil($component->sequence + 1) - 0.01)
                )
            )
            ->queryAll();


        if($order==null || $lead==null || $component==null){
            throw new CHttpException(404, Yii::t('core', 'Cannot find info related with this order.'));
        }
        $u = $order->customerUser;
        $user = array(
            'fn'=>$u->first_name,
            'ln'=>$u->last_name,
            't'=>$u->title,
            'co'=>$u->customerMain->name,
            'a1'=>$u->address1,
            'a2'=>$u->address2,
            'c'=>$u->city,
            's'=>$u->state,
            'z'=>$u->zip,
            'p'=>$u->phone,
            'm'=>$u->mobile,
            'f'=>$u->fax,
            'e'=>$u->email,
        );
        $l = array(
            'fn'=>$lead->first,
            'ln'=>$lead->last,
            't'=>$lead->title,
            'co'=>$lead->company,
            'a1'=>$lead->address1,
            'a2'=>$lead->address2,
            'c'=>$lead->city,
            's'=>$lead->state,
            'z'=>$lead->zip,
            'p'=>$lead->phone,
//                'm'=>$lead->mobile,
            'f'=>$lead->fax,
            'e'=>$lead->email,
            'notes'=>$lead->notes,
        );

        $email_content = $component->processEmail($user, $l);
//            $recipients = array($l->email);
        $recipients = array('chris@cgsmith.net'); // TODO: remove this and uncomment above line
        $recipientsName = array($lead->first . ' '. $lead->last);
        $message = new YiiMailMessage;
        $message->from = array($u->email => $u->first_name. ' ' .$u->last_name);
        $message->to = [$lead->email => $lead->first. ' ' .$lead->last];
        //$message->to = $recipients;
        $message->subject = $component->name;
        $message->setBody($email_content, 'text/html');
        // live
        foreach ($attachments as $attachment) {
            $ext = explode('.', $attachment['path']);
		echo realpath($attachment['url'])."\n";
            $message->attach(Swift_Attachment::fromPath(realpath($attachment['url']))->setFilename($attachment['name'].'.'.$ext[1]));
        }
		
        //TEST
        //$message->attach(Swift_Attachment::fromPath('components/9-38-1364341027.pdf')->setFilename('Dr Gary Wolfram Invitation.pdf'));
        // local faisal
//        $message->attach(Swift_Attachment::fromPath(dirname(Yii::app()->basePath).DIRECTORY_SEPARATOR.'components/pdf/7-94-1357914240.pdf')->setFilename('LPWI Newsletter and Annual Convention Invite.pdf'));
        return Yii::app()->mail->send($message);
		
		}
		return;
		
    }
}
