<?php

/**
 * This is the model class for table "component".
 *
 * The followings are the available columns in table 'component':
 * @property integer $id
 * @property integer $product_id
 * @property integer $component_type_id
 * @property string $email_content
 * @property integer $settings_postage_id
 * @property integer $sequence
 * @property string $name
 * @property string $description
 * @property string $date_created
 * @property string $date_updated
 * @property integer $document_id
 * @property string $labor_cost
 * @property string $material_cost
 * @property string $price
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property OrderStatus $product
 * @property ComponentType $componentType
 * @property ComponentDocument $componentDocument
 * 
 */
class Component extends CActiveRecord
{
        const TYPE_ALL = 0;
        const TYPE_EMAIL = 8;
        const ON_DEMAND_ATTACHMENT = 12;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Component the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'component';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('product_id, component_type_id, name, labor_cost, material_cost, price', 'required'),
                array('product_id, settings_postage_id, component_type_id, document_id', 'numerical', 'integerOnly'=>true),
                array('name, description', 'length', 'max'=>150),
                array('labor_cost, material_cost, price', 'length', 'max'=>6),
                array('email_content', 'emailContentValidator'),
                array('sequence, postage_override', 'safe'), //Make the sequence safe for "Decimals" TODO: Is this proper?
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, product_id, component_type_id, sequence, name, description, date_created, date_updated, document_id, labor_cost, material_cost, price, active', 'safe', 'on'=>'search'),
            );
	}
        
        public function emailContentValidator($attribute, $params){
            if($this->component_type_id==self::TYPE_EMAIL && empty($this->$attribute)){
                $this->addError($attribute, 'Email content can not be empty.');
            }
        }
        public function documentValidator($attribute, $params){
            if($this->component_type_id!=self::TYPE_EMAIL && empty($this->$attribute)){
                $this->addError($attribute, 'Select a document to upload.');
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'componentType' => array(self::BELONGS_TO, 'ComponentType', 'component_type_id'),
			'componentDocument' => array(self::BELONGS_TO, 'ComponentDocument', 'document_id'),
			'postage' => array(self::BELONGS_TO, 'SettingsPostage', 'settings_postage_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'component_type_id' => 'Component Type',
			'email_content' => 'Email Content',
			'settings_postage_id' => 'Preset Postage',
			'name' => 'Component Name/Email Subject',
			'description' => 'Description',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
			'document_id' => 'Document',
			'labor_cost' => 'Labor Cost',
			'material_cost' => 'Material Cost',
			'price' => 'Sale Price',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('component_type_id',$this->component_type_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('document_id',$this->document_id);
		$criteria->compare('labor_cost',$this->labor_cost,true);
		$criteria->compare('material_cost',$this->material_cost,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
	public function changePictures($mergeFile, $pictures){
		error_reporting(E_ALL);
		self::registerPhpDocx();
        
		$docx = new CreateDocx();
            
		$docx->addTemplate($mergeFile);
        if(isset($pictures['headshot'])){ 
        	$docx->addTemplateImage('headshot', $pictures['headshot']);
		}

        if(isset($pictures['signature'])){
        	$docx->addTemplateImage('signature', $pictures['signature']);
		}

        $docx->createDocx(str_replace('.docx', '', $mergeFile));
                
        spl_autoload_unregister(array('AutoLoader','load'));
        spl_autoload_register(array('YiiBase','autoload'));
	}
        
        public function addRedPreviewBox($mergeFile){
        	return ; // TODO: remove this linbe after upgrading to 3.3
        	error_reporting(E_ERROR);
        	self::registerPhpDocx();
        	$docx = new CreateDocx();
        	spl_autoload_register(array('YiiBase','autoload'));
        	
        	/* $html = '<div style="border: 1px solid RED; float: right; width: 300px;">'.
						$this->product->name. '<br />'.
						$this->description.
					'</div>';
        	$docx->embedHTML($html, array('parseDivsAsPs'=>true)); */
        	$format = array(
        			'b' => 'on',
        			'font' => 'Arial',
        			'color' => 'red'
        	);
        	
        	$text = $this->product->name . ' / ' . $this->description;
        	
        	$boxParameters = array(
        			'width' => 8,
        			'height' => 2,
        			'border_color'=>'#ff0000',
        			'fillcolor' => '#ffffff',
        			'textWrap' => 'square',
        			'padding_left' => 0.5,
        			'padding_top' => 0.5,
        			'padding-right' => 0.5,
        			'padding-bottom' => 0.5,
        			'margin_top' => 9,
        			'margin_left' => 9,
   	     			//'border' => 4,
    		    	//'border_color' => 'green'
        	);
        	
        	$docx->addTextBox(
        			array(
        					'text' => $text,
        					'format' => $format
        			),
        			$boxParameters
        	);
        	
 	  		//$docx->addDOCX(array('pathDOCX'=>$mergeFile)); // 3_3
 	  		$docx->addDOCX($mergeFile);
 	  		$docx->createDocx(str_replace('.docx', '', $mergeFile));        	
        }
        
        public static function registerPhpDocx(){
            Yii::import('application.extensions.*');
            // require_once 'phpdocx_pro_3_3/classes/AutoLoader.inc';
            spl_autoload_unregister(array('YiiBase','autoload'));
            require_once 'phpdocx_pro/classes/AutoLoader.php';
            spl_autoload_register(array('AutoLoader','load'));
        }

    /**
     * Inserts an attached document into the database.
     * @param $component_file File path or location
     * @param $model
     * @param bool $attachment Set to true if this document is not going to be merged
     */
    public function processDocument($component_file, $model, $attachment=false){
            $componentId = $model->getPrimaryKey();
            $compDocModel = new ComponentDocument; //Instantiate a new model for the component document
            $fileTime = time();
            $componentSave = 'components/'.$model->product_id.'-' //Where document will be stored and saved
                              .$componentId.'-'
                              .$fileTime.'.'.$component_file->getExtensionName();
            $conversionSave = $model->product_id.'-' //What the converted document will be called
                              .$componentId.'-'
                              .$fileTime.'.pdf';
            $component_file->saveAs($componentSave); //Save the file to the path
            chmod($componentSave,0755);

            //Set the model's attributes
            $compDocModel->url = $componentSave;
            $compDocModel->name = $component_file->getName();
            if (!$compDocModel->save()) {
                print_r($compDocModel->getErrors()); //TODO: Route error properly!
                exit;
            }

            //Update the component with the document model ID
            $model->document_id = $compDocModel->getPrimaryKey();
            if (!$model->save()) {
                print_r($model->getErrors());  //TODO: err printing
                exit;
            }

            if (!$attachment) // If the document is an attachment then do not perform a merge (could be a PDF or other)
            {
            //Merge the file with 'john doe' information TODO: This should be sent off to BG process

            $user[] = $this->getSampleUser();
            $lead[] = $this->getSampleLead();

            $mergeFile = $this->mergeFields($componentSave, $user, $lead);

                $headshot = dirname(Yii::app()->basePath).DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'headblank.jpg';
                $signature = dirname(Yii::app()->basePath).DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'yoursignature.png';
                $pictures = array();
                if(file_exists($headshot)){
                    $pictures['headshot'] = $headshot;
                }
                if(file_exists($signature)){
                    $pictures['signature'] = $signature;
                }
                $model->changePictures($mergeFile, $pictures);
                //Send the file off to get a PDF made
                //TODO: This task should be sent to a background process
                $pdffile = $this->createPDF($mergeFile,'components/pdf/',$conversionSave,'493212372');
                $compDocModel->pdf_url = $pdffile;
                $compDocModel->save();
                unlink($mergeFile);
            }
        }
        
        public function mergeFields($template_file, Array $user, Array $lead){
            require_once('components/tbs_class_php5.php');
            require_once('components/tbs_plugin_opentbs.php');

            //Instantiate new class
            $tbs = new clsTinyButStrong;
            $tbs->NoErr = true;
            $tbs->Plugin(TBS_INSTALL,OPENTBS_PLUGIN);

            $tbs->LoadTemplate($template_file);
            $tbs->MergeBlock('l',$lead);
            $tbs->MergeBlock('u',$user);
            $mergeFile = dirname(Yii::app()->basePath).DIRECTORY_SEPARATOR.'components/merge.docx';

            $tbs->Show(OPENTBS_FILE, $mergeFile);
            chmod($mergeFile,0755);
            return $mergeFile;
        }

        public function parseHeader($header='') {
            $resArr = array();
            $headerArr = explode("\n",$header);
            foreach ($headerArr as $key => $value) {
                $tmpArr=explode(": ",$value);
                if (count($tmpArr)<1) continue;
                $resArr = array_merge($resArr, array($tmpArr[0] => count($tmpArr) < 2 ? "" : $tmpArr[1]));
            }
            return $resArr;
        }

        /**
         * Converts the file into a PDF so it is viewable from the order screen
         * @param $file string being converted
         * @param $savePath string it will save to
         * @param $saveFile string the filename is going to be
         * @param $apiKey string for convertAPI
         * @return bool|string bool false if no conversion, or return save string
         */ 
        public function createPDF($file,$savePath,$saveFile,$apiKey) {
            try {
                $postData = array('OutputFileName'=>$saveFile,
                                  'ApiKey'=>$apiKey,
    //                              'file'=>"@".$_SERVER['DOCUMENT_ROOT'].$file,
                                  'file'=>"@".$file,
                                  'InputFormat'=>'docx');
                $ch = curl_init("http://do.convertapi.com/word2pdf");
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch,CURLOPT_HEADER,1);
                curl_setopt($ch,CURLOPT_POST,1);
                curl_setopt($ch,CURLOPT_POSTFIELDS,$postData);

                if (($result = curl_exec($ch))=== false){
                    die('Curl error: '.curl_error($ch));
                }
                $headers = curl_getinfo($ch);
                $header=$this->parseHeader(substr($result,0,$headers["header_size"]));
                $body=substr($result, $headers["header_size"]);

                curl_close($ch);
                if (0 < $headers['http_code'] && $headers['http_code'] < 400) {
                    // Check for Result = true
                    if (in_array('Result',array_keys($header)) ?  !$header['Result']=="True" : true) {
                        die("Something went wrong with Conversion service."); //TODO: Route to error log
                        return false;
                    }

                    $fp = fopen($savePath.$saveFile, "wbx");
                    fwrite($fp, $body);

                    return $savePath.$saveFile;
                }else {
                    die( "Exception Message : ".$result .".<br />Status Code :".$headers['http_code'].".<br />"); //TODO : route to error log
                    return false;
                }
            }catch (Exception $e) {
                die("Exception Message :".$e.Message."</br>");
                return false;
            }
        }
        
        public function getComponentType(){
            return $this->component_type_id;
        }

        
        public function getPreview(){
            $user = $this->getSampleUser();
            $lead = $this->getSampleLead();
            return $this->processEmail($user, $lead);
        }
        
        public function processEmail($user, $lead){
            $email_content = html_entity_decode($this->email_content);
            foreach($user as $i => $v){
                $email_content = str_replace('[u.'.$i.']', $v, $email_content);
            }
            foreach($lead as $i => $v){
                $email_content = str_replace('[l.'.$i.']', $v, $email_content);
            }
            return $email_content;
        }

        public function setUserVariables($userModel)
        {
            return array(
                'fn'=>$userModel->first_name,
                'ln'=>$userModel->last_name,
                't'=>$userModel->title,
                'co'=>$userModel->customerMain->name,
                'a1'=>$userModel->address1,
                'a2'=>$userModel->address2,
                'c'=>$userModel->city,
                's'=>$userModel->state,
                'z'=>$userModel->zip,
                'p'=>$userModel->phone,
                'm'=>$userModel->mobile,
                'f'=>$userModel->fax,
                'e'=>$userModel->email,
            );
        }
        public function setLeadVariables($leadModel)
        {
            return array(
                'fn'=>$leadModel->first,
                'ln'=>$leadModel->last,
                't'=>$leadModel->title,
                'co'=>$leadModel->company,
                'a1'=>$leadModel->address1,
                'a2'=>$leadModel->address2,
                'c'=>$leadModel->city,
                's'=>$leadModel->state,
                'z'=>$leadModel->zip,
                'p'=>$leadModel->phone,
                'f'=>$leadModel->fax,
                'e'=>$leadModel->email,
                'notes'=>$leadModel->notes,
            );
        }

        private function getSampleUser(){
            return array(
                'fn'=>'User-First',
                'ln'=>'User-Last',
                't'=>'User-Title',
                'co'=>'Your Company, Inc.',
                'a1'=>'Address 1',
                'a2'=>'Address 2',
                'c'=>'City',
                's'=>'ST',
                'z'=>'12345',
                'p'=>'(262) 754-8712',
                'm'=>'(262) 555-MOBI',
                'f'=>'(262) 555-1FAX',
                'e'=>'youremail@yourcompany.com',
            );
        }
        
        private function getSampleLead(){
            return array(
                'fn'=>'First',
                'ln'=>'Last',
                't'=>'Lead-Title',
                'co'=>'Lead Company, Inc.',
                'a1'=>'123 Main Street',
                'a2'=>'Suite A',
                'c'=>'New Berlin',
                's'=>'WI',
                'z'=>'53151-2232',
                'p'=>'(262) 754-8712',
                'm'=>'(262) 555-MOBI',
                'f'=>'(262) 555-1FAX',
                'e'=>'lead@theiremail.com',
                'notes'=>'Lead notes here',);
        }
        
        public function last($product_id){
            $this->getDbCriteria()->mergeWith(array(
                'condition'=>"product_id=:product_id",
                'params' => array(':product_id'=>$product_id),
                'order'=>'t.id DESC, t.sequence DESC',
                'limit'=>1,
            ));
            return $this;
        }
}
