<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property string $confirmation_number
 * @property integer $customer_user_id
 * @property string $customer_first
 * @property string $customer_last
 * @property string $customer_title
 * @property string $customer_company
 * @property string $customer_address1
 * @property string $customer_address2
 * @property string $customer_city
 * @property string $customer_state
 * @property string $customer_zip
 * @property string $customer_phone
 * @property string $customer_mobile
 * @property string $customer_fax
 * @property string $customer_email
 * @property string $customer_mailstop
 * @property string $customer_signature_file
 * @property string $customer_headshot_file
 * @property string $date_created
 * @property integer $frequency_id
 * @property string $frequency_description
 * 
 * @property User $customerUser
 */
class Order extends CActiveRecord
{
    public $checkBoxes = array(3);//Product types that should have checkboxes instead of radio buttons.
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('confirmation_number, customer_first, customer_last, customer_title, customer_company, customer_address1, customer_address2, customer_city, customer_state, customer_zip, customer_phone, customer_mobile, customer_fax, customer_email, customer_mailstop, customer_signature_file, customer_headshot_file, date_created, frequency_description', 'required'),
//			array('customer_user_id, frequency_id', 'numerical', 'integerOnly'=>true),
//			array('confirmation_number, frequency_description', 'length', 'max'=>64),
//			array('customer_first, customer_last, customer_title, customer_company, customer_address1, customer_address2, customer_city, customer_state, customer_zip, customer_phone, customer_mobile, customer_fax, customer_mailstop', 'length', 'max'=>45),
//			array('customer_email', 'length', 'max'=>128),
//			array('customer_signature_file, customer_headshot_file', 'length', 'max'=>256),
//			// The following rule is used by search().
//			// Please remove those attributes that should not be searched.
//			array('id, confirmation_number, customer_user_id, customer_first, customer_last, customer_title, customer_company, customer_address1, customer_address2, customer_city, customer_state, customer_zip, customer_phone, customer_mobile, customer_fax, customer_email, customer_mailstop, customer_signature_file, customer_headshot_file, date_created, frequency_id, frequency_description', 'safe', 'on'=>'search'),
                    
                    array('date_ordered, product_id', 'required'),
                    array('customer_user_id, frequency_id, product_id, active', 'numerical', 'integerOnly'=>true),
                    array('confirmation_number', 'length', 'max'=>64),
                    array('curr_seq_number', 'length', 'max'=>6),
                    array('completed,confirmation_number, curr_seq_number, active', 'safe'),
                    array('id, confirmation_number, customer_user_id, date_ordered, frequency_id, product_id, curr_seq_number, active, completed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'customerUser' => array(self::BELONGS_TO, 'User', 'customer_user_id'),
                    'frequency' => array(self::BELONGS_TO, 'Frequency', 'frequency_id'),
                    'orderLeads' => array(self::HAS_MANY, 'OrderLeads', 'order_id'),
                    'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'confirmation_number' => 'Confirmation Number',
			'customer_user_id' => 'Customer User',
			'customer_first' => 'Customer First',
			'customer_last' => 'Customer Last',
			'customer_title' => 'Customer Title',
			'customer_company' => 'Customer Company',
			'customer_address1' => 'Customer Address1',
			'customer_address2' => 'Customer Address2',
			'customer_city' => 'Customer City',
			'customer_state' => 'Customer State',
			'customer_zip' => 'Customer Zip',
			'customer_phone' => 'Customer Phone',
			'customer_mobile' => 'Customer Mobile',
			'customer_fax' => 'Customer Fax',
			'customer_email' => 'Customer Email',
			'customer_mailstop' => 'Customer Mailstop',
			'customer_signature_file' => 'Customer Signature File',
			'customer_headshot_file' => 'Customer Headshot File',
			'date_created' => 'Date Created',
			'frequency_id' => 'Frequency',
			'frequency_description' => 'Frequency Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('confirmation_number',$this->confirmation_number,true);
		$criteria->compare('customer_user_id',$this->customer_user_id);
		$criteria->compare('customer_first',$this->customer_first,true);
		$criteria->compare('customer_last',$this->customer_last,true);
		$criteria->compare('customer_title',$this->customer_title,true);
		$criteria->compare('customer_company',$this->customer_company,true);
		$criteria->compare('customer_address1',$this->customer_address1,true);
		$criteria->compare('customer_address2',$this->customer_address2,true);
		$criteria->compare('customer_city',$this->customer_city,true);
		$criteria->compare('customer_state',$this->customer_state,true);
		$criteria->compare('customer_zip',$this->customer_zip,true);
		$criteria->compare('customer_phone',$this->customer_phone,true);
		$criteria->compare('customer_mobile',$this->customer_mobile,true);
		$criteria->compare('customer_fax',$this->customer_fax,true);
		$criteria->compare('customer_email',$this->customer_email,true);
		$criteria->compare('customer_mailstop',$this->customer_mailstop,true);
		$criteria->compare('customer_signature_file',$this->customer_signature_file,true);
		$criteria->compare('customer_headshot_file',$this->customer_headshot_file,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('frequency_id',$this->frequency_id);
		$criteria->compare('frequency_description',$this->frequency_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}