<?php

/**
 * This is the model class for table "customer_lead".
 *
 * The followings are the available columns in table 'customer_lead':
 * @property integer $id
 * @property integer $customer_main_id
 * @property integer $customer_user_id
 * @property string $first
 * @property string $last
 * @property string $title
 * @property string $company
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property string $email
 * @property string $date_created
 * @property string $date_updated
 * @property string $notes
 * @property integer $in_campaign
 * @property integer $been_in_campaign
 * @property integer $active
 * @property string $keywords
 *
 * The followings are the available model relations:
 * @property User $customerUser
 * @property Main $customerMain
 */
class Lead extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Lead the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_lead';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            //TODO: create custom validation to allow first and last OR title and company
			array('address1, city, state, zip', 'required', 'on'=>'create'),
			array('id, customer_main_id, customer_user_id', 'numerical', 'integerOnly'=>true),
			array('first, last, address2, city, state, zip, fax, keywords', 'length', 'max'=>45),
			array('address1, phone, mobile', 'length', 'max'=>64),
			array('company, email', 'length', 'max'=>75),
			array('title', 'length', 'max'=>86),
            //array('email','email','checkMX'=>true, 'on'=>'create,update'),
			array('date_created, date_updated, notes', 'safe'),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('first, last, title, company, address1, address2, city, state, zip, phone, mobile, fax, email, date_created, date_updated, notes, keywords, in_campaign, been_in_campaign', 'safe', 'on'=>'search'),
		);
	}

    public function beforeSave()
    {
        $this->first = trim($this->first);
        $this->last = trim($this->last);
        return parent::beforeSave();
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customerUser' => array(self::BELONGS_TO, 'User', 'customer_user_id'),
			'customerMain' => array(self::BELONGS_TO, 'Main', 'customer_main_id'),
            //'keywords'     => array(self::MANY_MANY, 'Keyword','customer_keyword(id, keyword)','index'=>'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'customer_main_id' => 'Customer Main ID',
			'customer_user_id' => 'Customer User',
			'first' => Yii::t('core','model.lead.first'),
			'last' => Yii::t('core','model.lead.last'),
			'title' => Yii::t('core','model.lead.title'),
			'company' => Yii::t('core','model.lead.company'),
			'address1' => Yii::t('core','model.lead.address1'),
			'address2' => Yii::t('core','model.lead.address2'),
			'city' => Yii::t('core','model.lead.city'),
			'state' => Yii::t('core','model.lead.state'),
			'zip' => Yii::t('core','model.lead.zip'),
			'phone' => Yii::t('core','model.lead.phone'),
			'mobile' => Yii::t('core','model.lead.mobile'),
			'fax' => Yii::t('core','model.lead.fax'),
			'email' => Yii::t('core','model.lead.email'),
			'date_created' => Yii::t('core','model.lead.date_created'),
			'date_updated' => Yii::t('core','model.lead.date_updated'),
			'notes' => Yii::t('core','model.lead.notes'),
            'in_campaign' => Yii::t('core','model.lead.in_campaign'),
            'been_in_campaign' => Yii::t('core','model.lead.been_in_campaign'),
            'active' => Yii::t('core','model.lead.active'),
            'keywords' => Yii::t('core','model.lead.keywords')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('customer_main_id',$this->customer_main_id);
		$criteria->compare('customer_user_id',$this->customer_user_id);
		$criteria->compare('first',$this->first,true);
		$criteria->compare('last',$this->last,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('phone',$this->mobile,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('notes',$this->notes,true);
        $criteria->compare('in_campaign',$this->in_campaign);
        $criteria->compare('been_in_campaign',$this->been_in_campaign);
        $criteria->compare('active',$this->active);
        $criteria->compare('keywords',$this->keywords);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getKeywords(){
            return Keyword::model()->findAll(array(
                        'condition'=>'customer_main_id=:id AND active=1',
                        'params'=>array(':id'=>Yii::app()->user->customer_id)
                    ));
        }
}