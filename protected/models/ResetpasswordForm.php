<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ResetpasswordForm extends CFormModel
{
        private $_user;
        public $email;
        
        private $_reset_password;
        
        private $_hashed_reset_password;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('email', 'required'),
			// rememberMe needs to be a boolean
			// password needs to be authenticated
			array('email', 'verify'),
		);
	}
        
        public function verify($attribute,$params) {
            if(!$this->hasErrors())  // we only want to authenticate when no other input errors are present
            {
                $user = User::model()->findByAttributes(array('email'=>$this->$attribute));
                if(null==$user) {
                    $this->addError('email','No user found under this email.');
                } else {
                    $this->_user = $user;
                }
            }
	}
        
        public function sendEmail(){
            $pmessage = '<h2>Password Reset</h2>';
            $pmessage .= 'Hello,<br><br>Someone requested a password reset for your account.  If this was not you please contact us
            as soon as possible.';
            $pmessage .= '<br><br>The following temporary password will expire within an hour.<br /><br />';
            $pmessage .= '<strong>Password: </strong>'. $this->_reset_password;

            $message = new YiiMailMessage;
            $message->view = 'email';
            $message->subject = "Mobile Marketer Password Reset";
            $params = array('content' => $pmessage);
            $message->setBody($params, 'text/html');
            $message->addTo($this->email);
            $message->from = Yii::app()->params['adminEmail'];
            Yii::app()->mail->send($message);
        }
        
        public function prepareResetPassword(){
            $this->_reset_password = $this->createTemporaryPassword();
            $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
            $this->_hashed_reset_password = $ph->HashPassword($this->_reset_password);
//            $ph->CheckPassword($this->password,$record->password)
            
        }
        
        private function createTemporaryPassword(){
            $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $total_characters = strlen($characters);
            $string = '';
            for ($i = 0; $i < 10; $i++) {
                 $string .= $characters[rand(0, $total_characters - 1)];
            }
            return $string;
        }

        /**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'email'=>'Email Address',
		);
	}
        
        public function saveResetPassword(){
            $this->prepareResetPassword();
            $reset_password_expiry = time()+ 86400; //24*60*60
            $this->_user->saveAttributes(array('reset_password'=>$this->_hashed_reset_password, 'reset_password_expiry'=>  date("Y-m-d H:i:s", $reset_password_expiry)));
        }
}
