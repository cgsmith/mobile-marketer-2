<?php

class LeadImport extends CFormModel
{
    public $file;

    public function rules()
    {
        return array(
            array(
                'file',
                'file',
                'types'=>'csv',
                'maxSize'=>1024 * 1024 * 2, //2MB
                'tooLarge'=>'The file was larger than 2MB. Please upload a smaller file.',
                'allowEmpty'=>false
            )
        );
    }

    public function attributeLabels()
    {
        return array(
            'file' => 'Select file',
        );
    }
    
    function lookup_keywords($keywords) {
        foreach ($keywords as $lookup) {
            $output = Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('customer_keyword')
                    ->where('customer_main_id=:mainid AND keyword=:keyword', array(':mainid'=>Yii::app()->user->customer_id,':keyword'=>$lookup))
                    ->queryRow();

            if (($output == false) || ($output == null)){
                    $keyword=new Keyword;
                    $keyword->keyword = $lookup;
                    $keyword->customer_main_id = Yii::app()->user->customer_id;
                    //$keyword->active = 0;
                    $keyword->save();
                    $json[] = $keyword->getPrimaryKey();
            } else {
                    $json[] = $output['id'];
            }
        }
        return json_encode($json);
    }
    
    public function isKeywordbelongsToCustomer($keyword_id, $active=true){
        $customer_id = Yii::app()->user->customer_id;
        $sql = "SELECT id FROM customer_keyword WHERE id=:keywordId AND customer_main_id=:customerId ";//AND active=:active";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(':keywordId', $keyword_id, PDO::PARAM_INT);
        $command->bindValue(':customerId', $customer_id, PDO::PARAM_INT);
        //$command->bindValue(':active', (int)$active, PDO::PARAM_INT);
        return $command->execute()==1;
    }
}