<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property integer $customer_main_id
 * @property integer $product_type_id
 * @property integer $frequency_id
 * @property string $name
 * @property string $description
 * @property string $specific_datetime
 * @property string $date_created
 * @property string $date_updated
 * @property integer $active
 * @property integer $special_event
 * @property integer $billed
 *
 * The followings are the available model relations:
 * @property CustomerMain $customerMain
 * @property ProductType $productType
 * @property Frequency $frequency
 */
class Product extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_main_id, product_type_id, frequency_id, name', 'required'),
			array('customer_main_id, product_type_id, frequency_id,  special_event, billed', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>128),
			array('description', 'length', 'max'=>256),
			array('specific_datetime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_main_id, product_type_id, frequency_id, name, description, specific_datetime, date_created, date_updated, active, special_event, billed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'customerMain' => array(self::BELONGS_TO, 'Customer', 'customer_main_id'),
                    'productType' => array(self::BELONGS_TO, 'ProductType', 'product_type_id'),
                    'frequency' => array(self::BELONGS_TO, 'Frequency', 'frequency_id'),
                    'components' => array(self::HAS_MANY, 'Component', 'product_id', 'order'=>'sequence ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_main_id' => 'Customer Main',
			'product_type_id' => 'Product Type',
			'frequency_id' => 'Frequency',
			'name' => 'Name',
			'description' => 'Description',
			'specific_datetime' => 'Specific Datetime',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
			'active' => 'Active',
			'special_event' => 'Special Event',
			'billed' => 'Billed',
            'imgurl' => 'Image URL',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('customer_main_id',$this->customer_main_id);
		$criteria->compare('product_type_id',$this->product_type_id);
		$criteria->compare('frequency_id',$this->frequency_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('specific_datetime',$this->specific_datetime,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('special_event',$this->special_event);
		$criteria->compare('billed',$this->billed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
    public function addComponent($component){
        $component->product_id = $this->id;

        $component->active = true;

        if (!$component->save()) { //Save the model and then get the component ID
           print_r($component->getErrors()); //TODO: DO NOT print errors!
            exit;
        }
        $componentId = $component->getPrimaryKey(); //The component ID is used for updates and file naming

        $component_file = CUploadedFile::getInstance($component,'document_id'); //Get uploaded file instance
        if (!empty($component_file)) {
            $model->processDocument($component_file, $component);
            $this->redirect(array('product/components','id'=>$component->product_id));
        }

        return $component->save();
    }

    public function updateComponent($component){
        $component->attributes=$_POST['Component'];
        return $component>save();
    }

    public function isUserbelongsToProduct(){
        $sql = "SELECT * FROM product WHERE customer_main_id=:customerId AND id=:productId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(':productId', $this->id, PDO::PARAM_INT);
        $command->bindValue(':customerId', Yii::app()->user->customer_id, PDO::PARAM_INT);
        return $command->execute()==1;
    }

    public function getComponentEmails($returnBoolean=false) {
        $component_type = 8; //email id
        $sql = "SELECT id FROM component WHERE product_id = :productid AND component_type_id = :componenttype";
        $components = Yii::app()->db->createCommand()
            ->select('id')
            ->from('component')
            ->where('product_id=:productid AND component_type_id=:componenttype',
                array(':productid'=>$this->id,':componenttype'=>$component_type))
            ->queryAll();
        if ($returnBoolean) { return $components>=1;} //return true if items exist as email
        return $components;
    }
}