<?php

/**
 * This is the model class for table "customer_user".
 *
 * The followings are the available columns in table 'customer_user':
 * @property integer $id
 * @property integer $customer_main_id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $title
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $mailstop
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property string $date_created
 * @property string $signature_file
 * @property string $headshot_file
 * @property string $start_page
 * @property integer $receive_reports
 * @property integer $pagination
 * @property string $roles
 * @property string $api_username
 * @property string $api_password
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Lead[] $leads
 * @property Main $customerMain
 */

//TODO: Validate username is email address
//TODO: When a new user is created, populate keywords for company with defaults
class User extends CActiveRecord
{
    public $password1;
    public $password2;

    /**
     * Updates the user password
     */
    public function beforeSave()
    {
        if(!empty($this->password1) && $this->password1 == $this->password2) {
            $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
            $this->password = $ph->HashPassword($this->password1);
        }
        return parent::beforeSave();
    }
    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

            array('email, api_username, api_key','unique'),
            
            array('address1, city, state, zip, first_name, last_name, phone, password', 'required'),
            array('email', 'required', 'on'=>'insert, update'), // added later
            array('email', 'safe', 'on'=>'delete'), // added later
            array('id, customer_main_id, receive_reports, active, pagination', 'numerical', 'integerOnly'=>true),
            array('email, first_name, last_name, title, address1, address2, city, zip, mailstop', 'length', 'max'=>45),
            array('date_created, start_page, mobile, fax, api_username, api_key', 'safe'),
                    
            //TODO: VERIFY EMAIL IN PRODUCTION
            //array('email','email','checkMX'=>true),
            //changePassword Scenario
            array('password1, password2', 'required', 'on'=>'changePassword'),
            array('password1, password2', 'length', 'min'=>4),
            array('password2', 'compare', 'compareAttribute'=>'password1', 'on'=>'changePassword'),
            array('headshot_file', 'file', 'types'=>'jpg,png,JPG,PNG','allowEmpty'=>true ,'wrongType'=>'Only jpg,png allowed'),
            array('signature_file', 'file', 'types'=>'jpg,png,JPG,PNG','allowEmpty'=>true ,'wrongType'=>'Only jpg,png allowed'),
            //TODO EMails need to be emails for validation in PRODUCTION
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('email, first_name, last_name, title, address1, address2, city, state, zip, mailstop, phone, mobile, fax, date_created, receive_reports, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'leads' => array(self::HAS_MANY, 'Lead', 'customer_user_id'),
                    'customerMain' => array(self::BELONGS_TO, 'Customer', 'customer_main_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
                    'id' => 'ID',
                    'customer_main_id' => 'Customer Main',
                    'email' => 'Email',
                    'password' => 'Password',
                    'first_name' => 'First Name',
                    'last_name' => 'Last Name',
                    'title' => 'Title',
                    'address1' => 'Address1',
                    'address2' => 'Address2',
                    'city' => 'City',
                    'state' => 'State',
                    'zip' => 'Zip',
                    'mailstop' => 'Mailstop',
                    'phone' => 'Phone',
                    'mobile' => 'Mobile',
                    'fax' => 'Fax',
                    'date_created' => 'Date Created',
                    'start_page' => 'Starting Page',
                    'receive_reports' => 'Receive Weekly Email Reports',
                    'pagination' => 'Leads Per Page',
                    'active' => 'Active',
                    'roles' => 'Role',
                    'headshot_file' => 'Portrait File',

                    'password1' => 'Password',
                    'password2' => 'Confirm Password',
                    
                    'api_username' => 'API Username',
                    'api_key' => 'Api Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('customer_main_id',$this->customer_main_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('mailstop',$this->mailstop,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('receive_reports',$this->receive_reports);
        $criteria->compare('roles',$this->roles,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @return array Returns array of start page options for users.  Used in drop down list.
     */
    public function startPages()
    {
        $a = Yii::app()->user->checkAccess('prod');
        if ($a) {
            $array = array(
                'production/index'=>'Production',
                'site/index'=>'Home Page',
            );
        } else {
            $array = array(
                'lead/create'=>'Create Lead',
                'lead/lookup'=>'Lookup Lead',
                'lead/index'=>'Lead List',
                'site/reports'=>'Reports',
                'site/index'=>'Home Page',
            );
        }
        return $array;
    }

    /**
     * @return array Returns array of leads allowed to paginate on lead page (must be integer)
     */
    public function leadPagination()
    {
        $array = array(
            '10'=>'10',
            '25'=>'25',
            '50'=>'50',
            '100'=>'100',
        );
        return $array;
    }
}