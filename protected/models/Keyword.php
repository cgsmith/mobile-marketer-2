<?php

/**
 * This is the model class for table "customer_keyword".
 *
 * The followings are the available columns in table 'customer_keyword':
 * @property integer $id
 * @property integer $customer_main_id
 * @property string $keyword
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Main $customerMain
 */
class Keyword extends CActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Keyword the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_keyword';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_main_id', 'required'),
			array('customer_main_id, active', 'numerical', 'integerOnly'=>true),
			array('keyword', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_main_id, keyword, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customerMain' => array(self::BELONGS_TO, 'Main', 'customer_main_id'),
            'customerLead' => array(self::BELONGS_TO, 'Lead', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_main_id' => 'Customer Main',
			'keyword' => 'Keyword',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('customer_main_id',$this->customer_main_id);
		$criteria->compare('keyword',$this->keyword,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}