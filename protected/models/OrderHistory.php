<?php

/**
 * This is the model class for table "order_history".
 *
 */
class OrderHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		/*return array(
            array('date_ordered, product_id', 'required'),
       	);*/
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'Order Number',
			'seq_number' => 'Sequence Number',
			'date_produced' => 'Date Produced',
		);
	}
}