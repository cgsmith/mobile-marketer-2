<?php

/**
 * Api class.
 * Api is the data structure for keeping
 * Api logic. It is used by the 'ApiController'.
 */
class Api extends CFormModel {
    public static function generateApiKey($user){
        $user = User::model()->findByPk($user->id);
        return md5($user->id .$user->customer_main_id . $user->email . Yii::app()->params['apiSalt'].  rand(1, 99));
    }
}
