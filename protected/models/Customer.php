<?php

/**
 * This is the model class for table "customer_main".
 *
 * The followings are the available columns in table 'customer_main':
 * @property integer $id
 * @property string $name
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Keyword[] $keywords
 * @property Lead[] $leads
 * @property User[] $users
 */
class Customer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Customer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_main';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('name','required'),
            array('name','unique'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>90),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'keywords' => array(self::HAS_MANY, 'Keyword', 'customer_main_id'),
			'leads' => array(self::HAS_MANY, 'Lead', 'customer_main_id'),
			'users' => array(self::HAS_MANY, 'User', 'customer_main_id'),
			'addresses' => array(self::HAS_MANY, 'Address', 'customer_main_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Organization Name',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);
            $criteria->compare('name',$this->name,true);
            $criteria->compare('active',$this->active);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
	}
        
        public static function getCustomerLeads($customer_user_list){
            $command = Yii::app()->db->createCommand();
            return $command->select('id, first, last, title, company, address1, address2, city, state, zip, phone, fax, email')
                    ->from('customer_lead')
                    ->where(array('in', 'id', $customer_user_list))
                    ->queryAll();
        }
        
        public function isUserAllowed(){
            $sql = "SELECT * FROM customer_user WHERE customer_main_id=:customerId AND id=:userId";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(':customerId', $this->id, PDO::PARAM_INT);
            $command->bindValue(':userId', Yii::app()->user->getId(), PDO::PARAM_INT);
            return $command->execute()==1;
        }
        
        public static function isOwner(){
            
        }
}