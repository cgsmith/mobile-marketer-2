<?php

class PostTest extends WebTestCase
{
    public $fixtures=array(
        'posts'=>'Post',
    );

    public function testShow()
    {
        $this->open('post/1');
        // verify the sample post title exists
        $this->assertTextPresent($this->posts['sample1']['title']);
        // verify comment form exists
        $this->assertTextPresent('Leave a Comment');
    }

}