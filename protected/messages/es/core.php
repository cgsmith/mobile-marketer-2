<?php
return array(
    'Hello' => 'Hola',
    'delete'=> 'Borrar',
    'update'=> 'Actualiza',

    'model.lead.first' => 'Primero Nombre',
    'model.lead.last' => 'Apellido',
    'model.lead.title'=> 'Título',
    'model.lead.company' =>'Empressa',
    'model.lead.address1' => 'Dirección 1',
    'model.lead.address2' => 'Dirección 2',
    'model.lead.city' => 'Ciudad',
    'model.lead.state' => 'Estado',
    'model.lead.zip' => 'ZIP',
    'model.lead.phone' => 'Teléfono',
	'model.lead.mobile' => 'm�vil',
    'model.lead.fax' =>'Máquina de faxear',
    'model.lead.email' => 'Correo electrónico',
    'model.lead.date_created' => 'Fecha creada',
    'model.lead.date_updated' => 'Fecha actualizada',
    'model.lead.notes' => 'Notas',
    'model.lead.in_campaign' => 'En de una campaña',
    'model.lead.been_in_campaign' => 'Ha estado en campaña',
    'model.lead.active' => 'Activo',
    'model.lead.keywords' => 'Grupos',

    'confirm.delete' => '¿Estás seguro de de que desea eliminar esta persona?',

    'lead.Leads' => 'Personas',
    'lead.hint' => 'Puede seleccionar varias personas mientras se cambia a otra página.',
    'lead.index.order_products' => 'Productos Orden',

    //MENU
    'menu.leads'=> 'Personas',
    'menu.new' => 'Nuevo',
    'menu.lookup'=>'Buscar',
    'menu.list'=>'Lista',
    'menu.admin'=>'Administración',
    'menu.campaigns'=>'Campañas',
    'menu.reports'=>'Reportes',
	'menu.cancel_order' => 'Cancelar pedido',
    'menu.users'=>'Usuarios',
    'menu.create'=>'Crear',
    'menu.production'=>'Producción',
    'menu.administration'=>'Administración',
    'menu.login'=>'Acceder',
    'menu.logout'=>'Desconectar',
    'menu.profile'=>'Perfil',


);