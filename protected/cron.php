<?php

/**
 * Entry script for the cron
 */
// defined('YII_DEBUG') or define('YII_DEBUG',true);
// including Yii
require_once(dirname(__FILE__).'/../../yii/framework/yii.php');
// a seperate config file
$configFile=dirname(__FILE__).'/config/cron.php';

// creating and running console application
Yii::createConsoleApplication($configFile)->run();
?>
