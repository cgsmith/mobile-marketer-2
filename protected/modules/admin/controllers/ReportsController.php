<?php

class ReportsController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index', 'view', 'useremail', 'userstatus', 'billing'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex() {
            $this->render('index');
    }
    
    public function actionUseremail(){
        $model = new UseremailReports();
        if(isset($_GET['date_from']) && isset($_GET['date_to'])){
            $type = $_GET['type'];
            $customer_user_id = '';
            $for = $_GET['for'];
            if($for==UseremailReports::FORCUSTOMER){
                $customer_user_id = $_GET['customer'];
            } else if($for==UseremailReports::FORUSER){
                $customer_user_id = $_GET['user'];
            }
            $model->printReport(array('from'=>$_GET['date_from'], 'to'=>$_GET['date_to']), $type, $for, $customer_user_id);
        }
        
    }
    public function actionBilling(){
        $model = new BillingReports();
        if(isset($_GET['date_from']) && isset($_GET['date_to'])){
            $type = $_GET['type'];
            $model->printReport(array('from'=>$_GET['date_from'], 'to'=>$_GET['date_to']), $type);
        } else {
            echo "Invalid data submitted";
        }
//        $date = array('from'=> date('m/d/Y', time()), 'to'=>date('m/d/Y', time()+(7*24*60*60)));
//        $this->renderPartial('billing', array('date'=>$date));
    }
    public function actionUserstatus(){
        $model = new UserstatusReports;
        if(isset($_GET['type'])){
            $model->printReport($_GET['type']);
        } 
        else {
//            $this->renderPartial('userstatus', array('model'=>$model));
            echo "Invalid data submitted";
        }
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Postage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Reports::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Postage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='postage-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
