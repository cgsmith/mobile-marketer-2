<?php
/* @var $this PostageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Admin' => 'admin/index',
    'Postages',
);

$this->menu=array(
	array('label'=>'Create Postage', 'url'=>array('create')),
	array('label'=>'Manage Postage', 'url'=>array('admin')),
);
?>

<h1>Postages</h1>

<?php 
$this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','New Postage'),
    'icon' =>'plus-sign white',
    'type'=>'primary',
    'url'=>array(
        'postage/create',
    )
));
$gridColumns = array(
    'name',
    'cost',
    array(
        'htmlOptions' => array('nowrap'=>'nowrap'),
        'class'=>'bootstrap.widgets.TbButtonColumn',
        'template'=>'{view} {update}',
    )
);

$this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider'=>$dataProvider,
    'template'=>"{items}",
    'columns'=>$gridColumns,
));
?>
