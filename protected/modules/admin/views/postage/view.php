<?php
/* @var $this PostageController */
/* @var $model Postage */

$this->breadcrumbs=array(
	'Postages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Postage', 'url'=>array('index')),
	array('label'=>'Create Postage', 'url'=>array('create')),
	array('label'=>'Update Postage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Postage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Postage', 'url'=>array('admin')),
);
?>

<h1>View Postage #<?php echo $model->id; ?></h1>

<?php 
$this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','update'),
    'icon' =>'pencil white',
    'type'=>'primary',
    'url'=>array(
        'postage/update',
        'id'=>$model->id,
    )));?>

<?php 
$this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','New Postage'),
    'icon' =>'plus-sign white',
    'type'=>'primary',
    'url'=>array(
        'postage/create',
    )
));
?>

<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'label'=>Yii::t('core','View All Postages'),
    'icon' =>'search white',
    'type'=>'primary',
    'url'=>array(
        'postage/index',
    )));
?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'cost',
	),
)); ?>

<?php 
//$this->widget('zii.widgets.CDetailView', array(
//	'data'=>$model,
//	'attributes'=>array(
//		'id',
//		'name',
//		'cost',
//	),
//));
?>
