<?php
/* @var $this PostageController */
/* @var $model Postage */

$this->breadcrumbs=array(
	'Postages'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Postage', 'url'=>array('index')),
	array('label'=>'Create Postage', 'url'=>array('create')),
	array('label'=>'View Postage', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Postage', 'url'=>array('admin')),
);
?>

<h1>Update Postage <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>