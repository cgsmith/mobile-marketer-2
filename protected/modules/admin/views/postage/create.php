<?php
/* @var $this PostageController */
/* @var $model Postage */

$this->breadcrumbs=array(
	'Postages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Postage', 'url'=>array('index')),
	array('label'=>'Manage Postage', 'url'=>array('admin')),
);
?>

<h1>Create Postage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>