<?php
/* @var $this ReportsController
 * @var $model BillingReports
 * @var $form TbActiveForm
 **/
?>
<?php

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'billing-report-form',
    'enableAjaxValidation'=>true,
    'type'=>'horizontal',
));
?>

<?php $date = array('from'=> date('m/d/Y', time()-(7*24*60*60)), 'to'=>date('m/d/Y', time())); ?>
<div class="control-group ">
    <label for="Date_from" class="control-label">Start Date: </label>
    <div class="controls">
<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'Date[from]',
    // additional javascript options for the date picker plugin
    'options'=>array(
        'showAnim'=>'fold',
    ),
    'value'=>$date['from'],
    'htmlOptions'=>array(
		'id'=>'billing_date_from',
        'style'=>'height:20px;'
    ),
));
?>
    </div>
</div>

<div class="control-group ">
    <label for="Date_to" class="control-label">End Date: </label>
    <div class="controls">
<?php
$this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'Date[to]',
    // additional javascript options for the date picker plugin
    'options'=>array(
        'showAnim'=>'fold',
    ),
    'value'=>$date['to'],
    'htmlOptions'=>array(
		'id'=>'billing_date_to',
        'style'=>'height:20px;'
    ),
));
?>
    </div>            
</div>

<?php echo $form->checkboxRow($model, 'type'); ?>

<div class="control-group ">
    <label class="control-label">&nbsp;</label>
    <div class="controls">
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Show')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
$('form#billing-report-form').on('submit', function(e){
    e.preventDefault();
    var from = new Date($('input#billing_date_from').val());
    var to = new Date($('input#billing_date_to').val());
    var date_from = from.valueOf()/1000;
    var date_to = to.valueOf()/1000;
    var type = $('#BillingReports_type:checked');
    if(type.length>0){
        type = type.val();
    } else {
        type = 0;
    }
    window.open('<?php echo $this->createAbsoluteUrl('reports/billing') ?>/date_from/'+date_from+"/date_to/"+date_to+'/type/'+type, 'Report', 'fullscreen=yes,width=600,height=400,scrollbars=yes');
});
</script>
