<?php
/* @var $this PostageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Admin' => 'admin/index',
    'Reports',
);
?>
<h1>Reports</h1>

<?php
//$this->widget('zii.widgets.jui.CJuiTabs',array(
//    'tabs'=>array(
////        'User Email'=>array('id'=>'user-email', 'url'=>array('reports/useremail')),
//        'User Email'=>array('id'=>'user-email', 'content'=>$this->renderPartial('useremail', array('model'=>new UseremailReports), false, true)),
//        'Billing'=>array('id'=>'billing','content'=>'Billing Reports'),
//        'User States'=>array('id'=>'user-states', 'ajax'=>array('reports/userstatus')),
//    ),
//    // additional javascript options for the tabs plugin
//    'options'=>array(
//        'collapsible'=>true,
//    ),
//));


$this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs', // 'tabs' or 'pills'
    'tabs'=>array(
        array('label'=>'User Email', 'content'=>$this->renderPartial('useremail', array('model'=>new UseremailReports()), true), 'active'=>true),
        array('label'=>'Billing', 'content'=>$this->renderPartial('billing', array('model'=>new BillingReports()), true), 'active'=>false),
        array('label'=>'User Status', 'content'=>$this->renderPartial('userstatus', array('model'=>new UserstatusReports()), true), 'active'=>false),
    ),
));

//$this->widget('bootstrap.widgets.TbMenu', array(
//    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
//    'stacked'=>false, // whether this is a stacked menu
//    'items'=>array(
//        array('label'=>'User Email', 'url'=>'#', 'active'=>true),
//        array('label'=>'Billing', 'url'=>'#'),
//        array('label'=>'User Status', 'url'=>'#'),
//    ),
//));
?>
