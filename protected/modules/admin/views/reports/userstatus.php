<?php
/* @var $this ReportsController */
/* @var $form TbActiveForm */
/* @var $model UserstatusReports */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'userstatus-report-form',
    'type'=>'horizontal',
    'action'=> $this->createUrl('reports/userstatus'),
));
//echo '<form action="userstatus-report-form" class="form-horizontal" method="post" action="'.$this->createUrl('reports/userstatus').'">';
//$form->radio($model, 'for');
echo $form->checkboxList($model, 'type', $model->getTypes(), array('class'=>'report-type'));
$this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Show'));
//echo '<form>';
$this->endWidget();
?>
<script type="text/javascript">
$('form#userstatus-report-form').on('submit', function(e){
    e.preventDefault();
    var checkboxes = $('input.report-type:checked');
    if(checkboxes.length<1){
        alert('Please select report type.');
    } else {
        var type=0;
        $.each(checkboxes, function(i,v){
            type += parseInt(v.value);
        });
        window.open('<?php echo $this->createAbsoluteUrl('reports/userstatus') ?>/type/'+type, 'Report', 'fullscreen=yes,width=600,height=400,scrollbars=yes');
    }
});
</script>