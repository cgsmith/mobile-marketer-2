<?php
/* @var $this ReprotsController */
/* @var $form TbActiveForm */
/* @var $model UseremailReports */
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'useremail-report-form',
    'enableAjaxValidation'=>true,
    'type'=>'horizontal',
));
//$form->radio($model, 'for');
?>
<div id="report-for-container">
<?php echo $form->radioButtonListRow($model, 'for', $model->getForTypes()); ?>
</div>
<div id="report-customerdropdown-container" style="display:none;" class="dropdowns-container">
<?php
 echo $form->dropDownListRow($model, 'customer', CHtml::listData($model->getCustomers(), 'id', 'name')); ?>
</div>
<div id="report-userdropdown-container" style="display:none;" class="dropdowns-container">
<?php echo $form->dropDownListRow($model, 'user', CHtml::listData($model->getUserList(), 'id', 'name')); ?>
</div>
<div id="report-type-container">
    <div class="control-group ">
<?php echo $form->radioButtonListRow($model, 'type', $model->getTypes(), array('class'=>'report_type_checkbox')); ?>
    </div>
</div>
<div id="report-dates-container">
<?php $date = array('from'=> date('m/d/Y', time()-(7*24*60*60)), 'to'=>date('m/d/Y', time())); ?>
<div class="control-group ">
    <label for="Date_from" class="control-label">Start Date: </label>
    <div class="controls">
<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'Date[from]',
    // additional javascript options for the date picker plugin
    'options'=>array(
        'showAnim'=>'fold',
    ),
    'value'=>$date['from'],
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));
?>
    </div>
</div>

<div class="control-group ">
    <label for="Date_to" class="control-label">End Date: </label>
    <div class="controls">
<?php
$this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'Date[to]',
    // additional javascript options for the date picker plugin
    'options'=>array(
        'showAnim'=>'fold',
    ),
    'value'=>$date['to'],
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));
?>
    </div>            
</div>
</div>
<div class="control-group ">
    <label class="control-label">&nbsp;</label>
    <div class="controls">
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Show')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
$('#UseremailReports_for_1').on('click', function(e){
    if($(this).is(':checked')){
        $('.dropdowns-container').hide();
        $('#report-customerdropdown-container').show();
    }
});

$('#UseremailReports_for_2').on('click', function(e){
    if($(this).is(':checked')){
        $('.dropdowns-container').hide();
        $('#report-userdropdown-container').show();
    }
});
$('#UseremailReports_for_0').on('click', function(e){
    if($(this).is(':checked')){
        $('.dropdowns-container').hide();
    }
});

$('form#useremail-report-form').on('submit', function(e){
    e.preventDefault();
    if($('.report_type_checkbox:checked').length>0){
        var from = new Date($('input#Date_from').val());
        var to = new Date($('input#Date_to').val());
        var date_from = from.valueOf()/1000;
        var date_to = to.valueOf()/1000;
        
        var type_array = [];
        $.each($('.report_type_checkbox:checked'), function(i,v){
            type_array[i] = v.value;
        });
        var report_for = $("input[name='UseremailReports[for]']:checked").val();
        var customer_or_user = '';
        if($('#report-userdropdown-container').css('display')!='none'){
            var user = $("select[name='UseremailReports[user]']").val();
            customer_or_user = '/user/'+user;
        }
        if($('#report-customerdropdown-container').css('display')!='none'){
            var customer = $("select[name='UseremailReports[customer]']").val();
            customer_or_user = '/customer/'+customer;
        }
        var type=type_array.join('-');        
        var report_url = '<?php echo $this->createAbsoluteUrl('reports/useremail') ?>/date_from/'+date_from+"/date_to/"+date_to+'/type/'+type+'/for/'+report_for+customer_or_user;
        window.open(report_url, 'Report', 'fullscreen=yes,width=600,height=400,scrollbars=yes');
    } else {
        alert('Please select one or more report type(s)');
    }
});
</script>