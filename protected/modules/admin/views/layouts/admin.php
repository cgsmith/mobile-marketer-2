<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <base href="<?php echo Yii::app()->request->baseUrl; ?>" />
    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

    <title><?php echo CHtml::encode($this->pageTitle); ?>®</title>
</head>

<body>

<div class="container" id="page">

    <?php
    $this->widget('bootstrap.widgets.TbNavbar', array(
        //'type'=>'inverse',
        'fixed'=>false,
        'brand'=>Yii::app()->name.'<sup><small>&reg;</small></sup>',
        'brandUrl'=>array('site/index'),
        'collapse'=>true, // requires bootstrap-responsive.css
        'items'=>array(
            array(
                'class'=>'bootstrap.widgets.TbMenu',
                'items'=>array(

                    //Leads
                    array('label'=>Yii::t('core','menu.leads'), 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                        array(
                            'label'=>Yii::t('core','menu.new'),
                            'icon'=>'plus',
                            'url'=>array(
                                '/lead/create'
                            )
                        ),
                        array(
                            'label'=>Yii::t('core','menu.lookup'),
                            'icon'=>'search',
                            'url'=>array(
                                '/lead/lookup'
                            )
                        ),
                        array(
                            'label'=>Yii::t('core','menu.list'),
                            'icon'=>'align-justify',
                            'url'=>array(
                                '/lead/index'
                            ),
                        ),
                        array(
                            'label'=>'Import',
                            'icon'=>'chevron-right',
                            'url'=>array(
                                '/lead/import'
                            ),
                        ),
                        array(
                            'label'=>'Export',
                            'icon'=>'chevron-left',
                            'url'=>array(
                                '/lead/export'
                            ),
                        ),
                        array(
                            'label'=>Yii::t('core','menu.admin'),
                            'url'=>array('/lead/admin'),
                            'visible'=>Yii::app()->user->checkAccess('admin')
                        )
                    ),
                        'visible'=>Yii::app()->user->checkAccess('user,manager,admin')
                    ),
                    //Campaigns
                    //array('label'=>Yii::t('core','menu.campaigns'), 'url'=>'#', 'visible'=>Yii::app()->user->checkAccess('user,manager,admin')),
                    //Reports
                    //array('label'=>Yii::t('core','menu.reports'), 'url'=>array('site/reports'), 'visible'=>Yii::app()->user->checkAccess('user,manager,admin')),

                    array(
                        'label'=>Yii::t('core','Orders'),
                        'url'=>array('orders/index'),
                        'visible'=>Yii::app()->user->checkAccess('user') && !Yii::app()->user->checkAccess('admin') && !Yii::app()->user->checkAccess('manager'),
                        'items'=>array()
                    ),


                    //Administration - Customers
                    array('label'=>Yii::t('core','Organizations'), 'url'=>'#', 'visible'=>Yii::app()->user->checkAccess('admin'),'items'=>array(
                        array('label'=>Yii::t('core','New'), 'icon'=>'plus','url'=>array('/customer/create'), 'visible'=>Yii::app()->user->checkAccess('admin')),
                        array('label'=>Yii::t('core','List'), 'icon'=>'align-justify','url'=>array('/customer/index'), 'visible'=>Yii::app()->user->checkAccess('admin')),
                    ),
                    ),
                    // only visible to managers
                    array('label'=>Yii::t('core','Organization'), 'url'=>'#', 'visible'=>Yii::app()->user->checkAccess('manager') && !Yii::app()->user->checkAccess('admin'),'items'=>array(
                        array('label'=>Yii::t('core','Settings'), 'icon'=>'align-justify','url'=>Yii::app()->createUrl('/customer/view',array('id'=>Yii::app()->user->getCustomerId())), 'visible'=>Yii::app()->user->checkAccess('manager')),
                    ),
                    ),


                    //Administration - Products
                    array('label'=>Yii::t('core','Products'), 'url'=>'#', 'visible'=>Yii::app()->user->checkAccess('admin'),'items'=>array(
                        array('label'=>Yii::t('core','New'), 'icon'=>'plus','url'=>array('/product/create'), 'visible'=>Yii::app()->user->checkAccess('admin')),
                        array('label'=>Yii::t('core','List'), 'icon'=>'align-justify','url'=>array('/product/index'), 'visible'=>Yii::app()->user->checkAccess('admin')),
                    ),
                    ),
                    //Administration - Administration
                    array('label'=>Yii::t('core','Settings'), 'url'=>'#', 'visible'=>Yii::app()->user->checkAccess('admin'),'items'=>array(
                        array('label'=>Yii::t('core','Reports'), 'icon'=>'plus','url'=>array('/admin/reports'), 'visible'=>Yii::app()->user->checkAccess('admin')),
                        array('label'=>Yii::t('core','Postage'), 'icon'=>'align-justify','url'=>array('/admin/postage'), 'visible'=>Yii::app()->user->checkAccess('admin')),
                        array('label'=>Yii::t('core','menu.cancel_order'), 'icon'=>'icon-remove','url'=>array('/orders/cancel'), 'visible'=>Yii::app()->user->checkAccess('admin')),
                    ),
                    ),
                    //Production
                    array('label'=>Yii::t('core','menu.production'), 'url'=>array('/production/index'), 'visible'=>Yii::app()->user->checkAccess('prod,admin')),

                    //Example Merge
                    //array('label'=>Yii::t('core','Example Merge'), 'url'=>array('production/merge')),

                ),
            ),
            array(
                'class'=>'bootstrap.widgets.TbMenu',
                'htmlOptions'=>array('class'=>'pull-right'),
                'items'=>array(
                    array('label'=>Yii::t('core','menu.login'), 'url'=>array('site/login'), 'visible'=>Yii::app()->user->isGuest),
                    array('label'=>Yii::t('core', 'Hello').' '.Yii::app()->user->name.' ('.Yii::app()->user->company.')!', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                        //array('label'=>Yii::t('core','menu.administration'), 'url'=>array('/admin'), 'visible'=>Yii::app()->user->checkAccess('admin')),
                        array('label'=>Yii::t('core','menu.profile'), 'url'=>array('/site/profile','id'=>Yii::app()->user->id),'icon'=>'user'),
                        /*'---',
                        array('label'=>'LANGUAGE'),
                        array('label'=>'English', 'url'=>array('site/language','lang'=>'en')),
                        array('label'=>'Español', 'url'=>array('site/language','lang'=>'es')),*/
                        array('label'=>Yii::t('core','menu.logout'), 'url'=>array('/site/logout'),'icon'=>'share'),
                        '---',
                        array('label'=>'Help', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest,'icon'=>'question-sign',),
                        array('label'=>'API', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest,'icon'=>'wrench',),
                    )),
                    array('label'=> 'Return to Admin Area', 'url'=>array('site/impersonateCancel'), 'visible'=>Yii::app()->user->impersonate && !Yii::app()->user->isGuest),
                ),
            ),
        ),
    ));
    ?>
    <?php if(isset($this->breadcrumbs)):?>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
    <?php endif?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
        Copyright &copy; <?php echo date('Y'); ?> by <a href="<?php echo Yii::app()->params->companyUrl;?>" target="_blank"><?php echo Yii::app()->params->company;?></a>.<br/>
        All Rights Reserved.<br/>
        Build <?php echo Yii::app()->params->build;?><br/>
    </div><!-- footer -->

</div><!-- page -->

</body>
</html>
