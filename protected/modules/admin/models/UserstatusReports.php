<?php

/**
 * This is the model class for table "settings_postage".
 *
 * The followings are the available columns in table 'settings_postage':
 * 
 */
class UserstatusReports extends Reports {
    const USER_NOOPEN_ORDERED = 1;
    const USER_NEVER_ORDERED = 2;
    const USER_NOOPEN_AND_NEVER_ORDERED = 3;
    
    public $type;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('type', 'required'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'type' => 'Type',
        );
    }

    public function getTypes(){
        return array(self::USER_NOOPEN_ORDERED=>'Users with no open orders', self::USER_NEVER_ORDERED=>'Users who have never ordered');
    }
    
    public function printReport($type){
        $sql = "";
        $report_title = "";
        $results = array();
        
        if($type==self::USER_NEVER_ORDERED){
            $report_title = "User who never ordered";
            $results = $this->getUsersWhoNeverOrdered();
            
        } else if($type==self::USER_NOOPEN_ORDERED){
            $report_title = "User with no open orders";
            $results = $this->getUsersWithNoOpenOrders();
            
        } else if($type==self::USER_NOOPEN_AND_NEVER_ORDERED){
            $report_title = "User who never ordered or with no open orders";
            $results = $this->getUsersWhoNeverOrdered();
            $results = array_merge($results, $this->getUsersWithNoOpenOrders());
        }
        ?>
        <style>
            th{text-align:left;}
        </style>
        <?php
        echo '<h1>'.$report_title.'</h1>';
        if(empty($results)){
            echo 'No result found.';
        } else {
            echo '<table style="width:100%;">';
//            echo '<thead><tr><th>Customer</th><th>Name</th><th>Phone</th><th>Email</th></tr></thead><tbody>';
            $prev_customer = '';
            foreach($results as $result){
                if($prev_customer==$result['customer_name']){
                    echo '<tr>';
                } else {
                    echo '<tr><th colspan="3">'.$result['customer_name'].'</th>';
                    echo '<tr>';
                }
                echo '<td>'.$result['first_name'].' '.$result['last_name'].' - </td>';
                echo '<td>'.$result['phone'].'</td>';
                echo '<td>'.$result['email'].'</td>';
                echo '</tr>';
                $prev_customer=$result['customer_name'];
            }
            echo '</tbody></table>';
        }
    }

    protected function getUsersWithOpenOrders()
    {
        $sql = "
            SELECT DISTINCT
                customer_user_id
            FROM
                `order`
            WHERE
                active=1
        ";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        //var_dump($result);exit;
        foreach ($result as $user)
        {
            $users[] = $user['customer_user_id'];
        }
        return $users;
    }

    private function getUsersWithNoOpenOrders(){

        $sql = "
            SELECT 
                cu.id AS id,
                cu.email AS email,
                cu.first_name AS first_name,
                cu.last_name AS last_name,
                cu.phone AS phone,
                cm.name AS customer_name
            FROM
                customer_user cu
                LEFT JOIN
                customer_main cm
                ON cm.id = cu.customer_main_id
            WHERE cu.id NOT IN (
            ";
        $users = $this->getUsersWithOpenOrders();
        $sql .= "";
        foreach ($users as $id)
        {
            $notIn .= $id.',';
        }
        $sql .= rtrim($notIn,',');
        $sql .= ")";
        $temp_results = Yii::app()->db->createCommand($sql)->queryAll();
        $results = array();
        // now check if any of the user have some other active order.
        foreach($temp_results as $index => $result){
            if(!Yii::app()->db->createCommand("SELECT id FROM `order` o WHERE o.active=1 AND o.customer_user_id=".$result['id'])->queryScalar()){
                $results[] = $result;
            }
        }
        return $results;
    }
    
    private function getUsersWhoNeverOrdered(){
        $sql = "SELECT
                    cu.id AS id,
                    cu.email AS email,
                    cu.first_name AS first_name,
                    cu.last_name AS last_name,
                    cu.phone AS phone,
                    cm.name AS customer_name
                FROM 
                    customer_user cu, customer_main cm
                WHERE 
                    ".//cu.active=1 AND 
                    "cm.id = cu.customer_main_id AND
                    cu.id NOT IN (SELECT customer_user_id FROM `order`)
                ORDER BY cm.name ASC";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }
}