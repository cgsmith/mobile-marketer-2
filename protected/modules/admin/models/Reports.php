<?php

/**
 * This is the model class for table "settings_postage".
 *
 * The followings are the available columns in table 'settings_postage':
 * 
 */
class Reports extends CFormModel
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Postage the static model class
     */
    public static function model($className=__CLASS__) {
            return parent::model($className);
    }

	/**
	 * @return array validation rules for model attributes.
	 */
    public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
//                    array('name, cost', 'required'),
            );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
//			'id' => 'ID',
//			'name' => 'Name',
//			'cost' => 'Cost',
		);
	}
}