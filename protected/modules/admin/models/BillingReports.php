<?php

/**
 * This is the model class for "Billing Reports".
 * 
 */
class BillingReports extends Reports {
    
    const TYPE_BILLING = 0;
    const TYPE_FORECAST = 1;
    
    public $type = self::TYPE_BILLING;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'type' => 'Production Forecast',
        );
    }
    
    protected function getBillingRecords($date){
        $date_from = date('Y-m-d H:i:s', $date['from']);
        $date_to = date('Y-m-d H:i:s', $date['to']+(24*60*60));
        $sql = "SELECT 
                    c.id AS component_id,
                    c.name AS component_name,
                    ob.sale_price AS cost,
                    ob.postage AS postage,
                    ob.material_cost AS material_cost,
                    ob.labor AS labor,
                    cu.id AS user_id,
                    cu.first_name AS user_first_name,
                    cu.last_name AS user_last_name,
                    cu.title AS user_title,
                    cm.id AS customer_id,
                    cm.name AS customer_name,
                    count(c.id) AS quantity,
                    op.order_id AS order_id,
                    p.name AS product_name,
                    op.date_printed AS date_printed
                FROM 
                    order_billing ob
                    LEFT JOIN `order` o
                    ON ob.order_id = o.id
                    LEFT JOIN order_printed op
                    ON op.id = ob.order_printed_id
                    LEFT JOIN component c
                    ON c.id = op.component_id
                    LEFT JOIN product p
                    ON p.id = c.product_id
                    LEFT JOIN
                    customer_user cu
                    ON cu.id = o.customer_user_id
                    LEFT JOIN
                    customer_main cm
                    ON
                    cu.customer_main_id = cm.id
                WHERE
                    op.date_printed >= '".$date_from."' AND op.date_printed < '".$date_to."'
                GROUP BY
                    c.id, cu.id, p.id
                ORDER BY
                    cm.id, cu.id, c.id
                ";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }
    
    protected function getForecastRecords($date){
        error_reporting(E_ERROR);
        $date_from = date('Y-m-d H:i:s', $date['from']);
        $date_to = date('Y-m-d H:i:s', $date['to']+(24*60*60));
        $sql = "
            SELECT
                c.id AS component_id,
                c.name AS component_name,
                c.price AS cost,
                
                c.postage_override AS postage,
                c.material_cost AS material_cost,
                c.labor_cost AS labor,

                c.sequence AS component_sequence,
                
                f.days AS frequency_days,
                
                o.date_ordered AS date_ordered,
                o.curr_seq_number AS product_curr_seq_number,
                ol.lead_id AS lead_id,
                cu.id AS user_id,
                cu.first_name AS user_first_name,
                cu.last_name AS user_last_name,
                cu.title AS user_title,
                cm.id AS customer_id,
                cm.name AS customer_name,
                count(c.id) AS quantity,
                o.id AS order_id,
                p.name AS product_name
            FROM
                `order` o
                LEFT JOIN customer_user cu ON cu.id = o.customer_user_id
                LEFT JOIN customer_main cm ON cm.id = cu.customer_main_id
                LEFT JOIN product p ON p.id = o.product_id
                LEFT JOIN frequency f ON f.id = p.frequency_id

                LEFT JOIN order_leads ol ON o.id = ol.order_id
                LEFT JOIN component c ON c.product_id = p.id
            WHERE
                CONCAT_WS('-', o.id, c.id,ol.lead_id) NOT IN (SELECT CONCAT_WS('-',order_id, component_id, lead_id) AS concatenated FROM order_printed)
            GROUP BY
                c.id, cu.id, p.id
            ORDER BY 
                cm.id, cu.id, c.id
            ";
        $components_data =  Yii::app()->db->createCommand($sql)->queryAll();
        $results = array();
        foreach($components_data as $key => $data){
            $frequency_days = json_decode($data['frequency_days']);
            $production_date_timestamp = strtotime($data['date_ordered'])+ ($frequency_days[(int)$data['component_sequence']-1] * 24*60*60);
//            $data['date_production'] = date('m-d-Y',$production_date_timestamp);
            $data['date_printed'] = date('m-d-Y',$production_date_timestamp);
            if(!empty($date_from) && !empty($date_to)){
                if($production_date_timestamp<strtotime($date_from) || $production_date_timestamp>=strtotime($date_to)){
                    continue;
                }
            }
            $data['user'] = $data['customer_user_firstname'].' '.$data['customer_user_lastname'];
            // order-component-lead
//            $data['selected'] = $data['id'].'-'.$data['component_id'].'-'.$data['lead_id'];
            $results[] = $data;
        }
        
        return $results;
    }
    
    public function printReport($date, $type=self::TYPE_BILLING,$echo = true){
        $report_title = '';
        $results = array();
        $date_from = date('m/d/Y', $date['from']+86400); // not sure why it is necessary
        $date_to = date('m/d/Y', $date['to']+86400); // not sure why it is necessary
        if($type==self::TYPE_BILLING){
            $report_title = "Billing Report from " . $date_from . ' through ' . $date_to;
            $results = $this->getBillingRecords($date);
        } else if($type==self::TYPE_FORECAST){
            $report_title = "Production Forecast from " . $date_from . ' through '. $date_to;
            $results = $this->getForecastRecords($date);
        } else {
            return false;
        }
        
        $body .= "
        <style>
            th{text-align:left;}
            td.num{text-align:right;}
            th, td{vertical-align:top}
            .tab-1{padding-left:20px;}
            .tab-2{padding-left:40px;}
            .tab-3{padding-left:60px;}
            .odd td, .odd th{background-color:#FAFAFA}
            .product-title th{background-color:#ECFBD4}
            .user-title th{background-color:#c4e3f3}
        </style>
        ";
        $body .= '<h1>'.$report_title.'</h1>';
        if(empty($results)){
            echo 'No result found.';
        } else {
            $body .= '<table style="width:100%;" cellspacing="0" cellpadding="0">';
            $body .= '<thead><tr><th>&nbsp;</th><th>Quantity</th><th>Cost</th><th>Total</th><!--<th>Labor</th><th>Material Cost</th>--></tr></thead><tbody>';
            $prev_customer = '';
            $prev_user = '';
            $prev_product = '';
            $row = 'odd';
            $currency_symbol = Yii::app()->params['currency_symbol'];
            
            // First Calculate the product components total cost
            $temp_product = '';
            $product_costs = array();
            $i = -1;
            
            $temp_user = '';
            $temp_company = '';
            $total_sales = null;
            $user_costs = array();
            foreach($results as $result){
                $total_cost = $result['quantity']*$result['cost'];
                $total_postage = $result['quantity']*$result['postage'];
                $total_sales += $total_cost + $total_postage;
                $result['user_name'] = $result['user_first_name'].' '.$result['user_last_name'];
                
                if($temp_product==$result['product_name'] && $temp_user==$result['user_name']){
                    $product_costs[$i] = $product_costs[$i] + $total_cost + $total_postage;
                } else {
                    $i++;
                    $product_costs[$i] = $total_cost + $total_postage;
                }
                $temp_product=$result['product_name'];
                
                // Calculate total cost for a user
                if($temp_user==$result['user_name']){
                    $user_costs[$result['user_id']] += $total_cost + $total_postage;
                } else {
                    $user_costs[$result['user_id']] = $total_cost + $total_postage;
                }
                $temp_user = $result['user_name'];

                if($temp_company==$result['customer_name']){
                    $company_costs[$result['customer_name']] += $total_cost + $total_postage;
                } else {
                    $company_costs[$result['customer_name']] = $total_cost + $total_postage;
                }
                $temp_company = $result['customer_name'];
            }

            $body .= '<h3>Total Sales: '.$currency_symbol.$total_sales.'</h3>';
            $i = 0; // for product total cost counter
            foreach($results as $result){
                if(!($prev_customer==$result['customer_name'])){
                    $body .= '<tr><th colspan="3">'.$result['customer_name'].' - '.$currency_symbol.$company_costs[$result['customer_name']].'</th></tr>';
                }
                $result['user_name'] = $result['user_first_name'].' '.$result['user_last_name'];
                if(!($prev_user==$result['user_name'])){
                    $body .= '<tr class="user-title"><th colspan="4" class="tab-1">'.$result['user_name'].' - '.$currency_symbol.$user_costs[$result['user_id']].'</th></tr>';
                }
                
                $total_cost = $result['quantity']*$result['cost'];
                $total_postage = $result['quantity']*$result['postage'];
                
                if(!($prev_product==$result['product_name']) || !($prev_user==$result['user_name'])){
                    $body .= '<tr class="product-title"><th colspan="3" class="tab-2">'.$result['product_name'].'</th><th>'.$currency_symbol.$product_costs[$i].'</th></tr>';
                    $i++;
                }

                $body .= '<tr class="'.$row.'">';
                $body .= '<td class="tab-3">'.$result['component_name'].'<br />Postage</td>';
                $body .= '<td>'.$result['quantity'].'</td>';
                $body .= '<td>'.$currency_symbol.sprintf('%.2f', $result['cost'],2).'<br />'.$currency_symbol.sprintf('%.2f', $result['postage'],2).'</td>';
                $body .= '<td>'.$currency_symbol.sprintf('%.2f', $total_cost,2).'<br />'.$currency_symbol.sprintf('%.2f', $total_postage,2).'</td>';
                $body .= '</tr>';
                
                $prev_customer=$result['customer_name'];
                $prev_user=$result['user_name'];
                $prev_product=$result['product_name'];
                $row = $row=='odd'?'even':'odd';
            }
            $body .= '</tbody></table>';
            $body .= '<h3>Total Sales: '.$currency_symbol.$total_sales.'</h3>';
            if ($echo) {
                echo $body;
            }else{
                return $body;
            }
        }
    }
}