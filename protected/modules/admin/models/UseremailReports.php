<?php

/**
 * This is the model class for table "settings_postage".
 *
 * The followings are the available columns in table 'settings_postage':
 * 
 */
class UseremailReports extends Reports {
    const FORALL = 0;
    const FORCUSTOMER = 1;
    const FORUSER = 2;
    
    const TYPE_NEW_LEADS = 10;
    const TYPE_CALL = 11;
    const TYPE_PRODUCT = 12;
    
    public $for = self::FORALL;
    public $customer;
    public $user;
    public $type;
    public $date_from;
    public $date_to;
    
    
	/**
	 * @return array validation rules for model attributes.
	 */
    public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
//                    array('name, cost', 'required'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'for' => 'For',
                'type' => 'Type',
//			'cost' => 'Cost',
            );
    }
    /**
     * returns types for User Email report form
     * 
     * @return type
     */
    public function getForTypes(){
        return array(self::FORALL=>'All', self::FORCUSTOMER=>'Customers', self::FORUSER=>'Users');
    }
    
    public function getTypes(){
        return array(self::TYPE_CALL=>'Call Report', self::TYPE_PRODUCT=>'Product Report');
    }
    
    public function getCustomers(){
        $customers = Customer::model()->findAll();
        return $customers;
    }
    
    public function getUsers(){
        $users = User::model()->findAll();
        return $users;
    }

    public function getUserList(){
        $users = Yii::app()->db->createCommand()
            ->select('u.id, CONCAT(u.last_name,", ",u.first_name, " - ", LEFT(c.name,9)) as name')
            ->from('customer_user u')
            ->leftJoin('customer_main c','c.id=u.customer_main_id')
            ->where('u.active=:active', array(':active'=>1))
            ->order('name asc')
            ->query();
        return $users;
    }
    
    /**
     * Print report based on Useremail Form subission
     * 
     * @param type $date
     * @param type $type
     * @param type $for
     */
    
    public function printReport($date, $type, $for, $customer_user_id){
        $this->date_from = date('Y-m-d H:i:s', $date['from']);
        $this->date_to = date('Y-m-d H:i:s', $date['to']+(24*60*60));
        
        $types = explode('-', $type);
        if(in_array(self::TYPE_CALL, $types)){
            if($for==self::FORALL) {
                $report_text = $this->getCallReportForAll($date);
            } else if($for==self::FORCUSTOMER){
                $report_text = $this->getCallReportForCustomer($date, $customer_user_id);
            } else if($for==self::FORUSER){
                $report_text = $this->getCallReportForUser($date, $customer_user_id);
            }
            if($report_text){
                echo $report_text;
            } else {
                echo 'No results found.';
            }
        }
    }
    
    public function getCallReportForCustomer($date, $customer_id){
        $sql = "
            SELECT
                count(c.id) AS quantity,
                CONCAT_WS(' ', cu.first_name, cu.last_name) AS user,
                cu.id AS user_id
            FROM
                `order` o
                INNER JOIN
                order_printed op
                ON o.id = op.order_id
                LEFT JOIN customer_user cu
                ON cu.id = o.customer_user_id
                LEFT JOIN
                customer_lead cl
                ON cl.id = op.lead_id
                LEFT JOIN component c
                ON c.id = op.component_id
            WHERE
                op.date_printed >= '".$this->date_from."' AND op.date_printed < '".$this->date_to."' AND cu.customer_main_id=".$customer_id."
            GROUP BY
                cu.id
            ORDER BY
                cu.first_name ASC
        ";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        var_dump($results);exit;
        if(empty($results)){
            return false;
        } else {
            $report_text = '';
            $report_text .= '<h1>Mobile Marketer Managers Call Report: '.date('m-d-Y', strtotime($this->date_from)).' through '.date('m-d-Y', strtotime($this->date_to)).'</h1>';
            foreach($results as $result){
                $lead_count_string = $result['quantity']>1?' leads':' lead';
                $report_text .= $result['user'] . ' - Materials sent to '.$result['quantity'] . $lead_count_string;
                $report_text .= '<br />=========================================<br/>';
            }
            return $report_text;
        }
    }
    public function getCallReportForUser($date, $user_id){
        $sql = "
            SELECT
                ohc.component_name AS component_name,
                ohc.component_type AS component_type,
                cl.first AS lead_firstname,
                cl.last AS lead_lastname,
                CONCAT_WS(' ', cu.first_name, cu.last_name) AS user,
                cl.company AS lead_company,
                cl.phone AS lead_phone,
                op.date_printed AS date_printed,
                o.confirmation_number AS confirmation
            FROM
                `order` o INNER JOIN order_printed op
                ON o.id = op.order_id
                
                LEFT JOIN customer_user cu
                ON cu.id = o.customer_user_id
                
                LEFT JOIN customer_lead cl
                ON cl.id = op.lead_id
                
                LEFT JOIN order_history_components ohc
                ON op.id = ohc.order_printed_id
            WHERE
                op.date_printed >= '".$this->date_from."' AND op.date_printed < '".$this->date_to."' AND o.customer_user_id=".$user_id."
            AND cu.receive_reports = 1
            AND ohc.component_type != 'Envelope'
            GROUP BY op.date_printed,ohc.component_name,lead_lastname asc
        ";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
//        echo '<pre>';
//        print_r($results);
//        echo '</pre>';
//        exit;
        if(empty($results)){
            return false;
        } else {
            $report_text = '';
            $report_text .= '<h2>User Call Report: '.date('m-d-Y', strtotime($this->date_from)).' through '.date('m-d-Y', strtotime($this->date_to)).'</h2>';
            $count_results = count($results);
            $lead_count_string = $count_results>1?' leads':' lead';
            $report_text .= '<h3>'.$results[0]['user'] . ' - Materials sent to '.$count_results . $lead_count_string. '</h3>';

            foreach($results as $result){
                $date = new DateTime($result['date_printed']);
                $date->modify('+1 week');
                $format =
               '%s, %s<br/>
                %s<br/>
                %s<br/>
                <br/>
                Call on %s<br/>
                <br/>
                %s - %s - %s<br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%s - %s<br/>';
                $report_text .= sprintf($format,$result['lead_lastname'],$result['lead_firstname'],$result['lead_company'],
                    $result['lead_phone'],$date->format('l m/d/Y'),$result['component_type'],$result['component_name'],
                    $result['confirmation'],$result['date_printed'],$result['component_name']);
                $report_text .= '-----------------------------------------<br /><br />';
            }
            return $report_text;
        }
    }


    public function getCallReportForAll($date){
        $date_from = $this->date_from;
        $date_to = $this->date_to;
        $sql = "
            SELECT
                c.name AS component_name,
                CONCAT_WS(' ',first, last) AS lead_name,
                cl.company AS lead_company,
                cl.phone AS lead_phone,
                CONCAT_WS(' ', cu.first_name, cu.last_name) AS user,
                cu.id AS user_id,
                date_printed
            FROM
                `order` o
                INNER JOIN
                order_printed op
                ON o.id = op.order_id
                LEFT JOIN customer_user cu
                ON cu.id = o.customer_user_id
                LEFT JOIN
                customer_lead cl
                ON cl.id = op.lead_id
                LEFT JOIN component c
                ON c.id = op.component_id
            WHERE
                op.date_printed >= '".$date_from."' AND op.date_printed < '".$date_to."'
                AND c.component_type_id <> 4
            ORDER BY
                cl.company ASC,cu.first_name ASC, c.name ASC, op.date_printed ASC
        ";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        if(empty($results)){
            return false;
        } else {
            $report_text = '';
            $report_text .= '<h1>Mobile Marketer All User Call Report: '.date('m-d-Y', $date['from']+86400).' through '.date('m-d-Y', $date['to']+86400).'</h1>';
            $prev_user = '';
            $prev_component = '';
            $prev_date = '';
            
            foreach($results as $result){
                if($prev_user!=$result['user']){
                    $lead_count = $this->countMaterialsForUser($result['user_id'], $date_from, $date_to);
                    $lead_count_string = $lead_count>1?' leads':' lead';
                    $report_text .= '<h3>'.$result['user'] . ' - Materials sent to '.$lead_count . $lead_count_string.'</h3>';
                }
                if($prev_component!=$result['component_name']){
                    $report_text .= '<br /><strong>Component Name: '.$result['component_name'] . '</strong><br />';
                }
                if($prev_date!=date('m-d-Y', strtotime($result['date_printed'])) || $prev_component!=$result['component_name']){
                    $report_text .= 'Sent: '.date('m-d-Y', strtotime($result['date_printed'])) . '<br />';
                    $report_text .= '-----------------------------------------<br />';
                }
                $report_text .= $result['lead_company'].': '.$result['lead_name'] . ' - ' . $result['lead_phone'] . '<br />';
                $prev_user = $result['user'];
                $prev_component = $result['component_name'];
                $prev_date = date('m-d-Y', strtotime($result['date_printed']));
            }
            return $report_text;
        }
    }
    
    private function countMaterialsForUser($user_id, $date_from, $date_to){
        $sql = "
                SELECT
                    count(op.id) AS count_components
                FROM
                    `order` o
                    INNER JOIN
                    order_printed op
                    ON o.id = op.order_id
                WHERE
                    o.customer_user_id = ".$user_id." AND op.date_printed >= '".$date_from."' AND op.date_printed < '".$date_to."'
            ";
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }
    
}