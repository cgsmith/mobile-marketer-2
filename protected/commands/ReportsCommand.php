<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Cron jobs for various purpose.
 *
 * @author Faisal Kaleem <faisal.cgs at nxvt.com>
 */
Yii::import('application.modules.admin.models.*');
require_once 'UseremailReports.php';
class reportsCommand extends CConsoleCommand {
    public function run($arg){
        $start = null;
        $end = null;
        if ($arg[0] === 'weekly'){
            $start = new DateTime('last monday');
            $end = new DateTime();
        } else if ($arg[0] === 'billing') {
            $this->sendMonthlyBillingEmail();
            exit;
        }
        $this->sendUserEmailReports($start,$end);
    }


    private function sendMonthlyBillingEmail()
    {

        $model = new BillingReports();
        $f = new DateTime('first day of last month');
        $date['from'] = $f->format('U');
        $date['from'] = $date['from'] - 86400;
        $t = new DateTime('last day of last month');
        $date['to'] = $t->format('U');
        $date['to'] = $date['to'] - 86400;
        $body = '<i>Please note that the report can be downloaded from Mobile Marketer</i><br/><br/>';
        $body .= $model->printReport(array('from'=>$date['from'], 'to'=>$date['to']), 0,false);

        $message = new YiiMailMessage;
        $message->view = 'email';
        $message->subject = "Billing Report from Mobile Marketer | ". date('m/d/Y');
        $params = array('content' => $body);
        $message->setBody($params, 'text/html');
        $message->addTo('minturn@salescampaigns.com');
        $message->addTo('peterson@salescampaigns.com');
        $message->from = Yii::app()->params['adminEmail'];

        Yii::app()->mail->send($message);
    }
    
    private function sendUserEmailReports($start,$end){
        echo "Starting User Email Report service...\r\n";
        $today = strtotime(date('Y-m-d',time())); // to remove the time part
        //$today = strtotime('2014-04-28');
        $date_from = ($start !== null) ? $start->format('Y-m-d') : date('Y-m-d',$today);
        $date_to = ($end !== null) ? $end->format('Y-m-d') : date('Y-m-d',$today+604800); // 1 week ahead
        $user_report = new UseremailReports;
        $users = User::model()->findAll();
        $user_report->date_from = $date_from;
        $user_report->date_to = $date_to;
        foreach($users as $user){
            $report_text = $user_report->getCallReportForUser(array('to'=>$date_to, 'from'=>$date_from), $user->id);
            if($report_text){
                echo 'Sending to '. $user->first_name. ' ' .$user->last_name. "\r\n";

                $message = new YiiMailMessage;
                $message->view = 'email';
                $message->subject = "User Call Report from Mobile Marketer | ". date('m/d/Y',$today);
                $params = array('content' => $report_text);
                $message->setBody($params, 'text/html');
                $message->addTo($user->email);
                //$message->addTo('chris@cgsmith.net'); //for testing
                // if dale carnegie then copy the boss
                if ($user->customer_main_id == 24) {
                    //$message->addCc('chris+steven@cgsmith.net'); //for testing                
                    $message->addCc('Steve.Bobowski@DaleCarnegie.com');
                }

                $message->from = Yii::app()->params['adminEmail'];

                if(Yii::app()->mail->send($message)){
                    $this->addToLog('report user email', array('to'=>$user->email, 'subject'=>$message->subject,'email_message'=>$report_text), 'Daily Cron for user emails.');
                }
            }
            
            if(strstr($user->roles, 'manager') ){
                $manager_report_text = $user_report->getCallReportForCustomer(array('to'=>$date_to, 'from'=>$date_from), $user->customer_main_id);
                if($manager_report_text){
                    echo "Sending to Manager:".$user->email." \r\n";
                    unset($message);
                    $message = new YiiMailMessage;
                    $message->view = 'email';
                    $message->subject = 'Mobile Marketer Manager Call Report: ' . date('m/d/Y',$today);
                    $params = array('content' => $manager_report_text);
                    $message->setBody($params, 'text/html');
                    //$message->addTo('chris@cgsmith.net'); //for testing
                    $message->addTo($user->email);
                    $message->from = Yii::app()->params['adminEmail'];

                    if(Yii::app()->mail->send($message)) {
                        $this->addToLog('report manager email', array('to'=>$user->email, 'subject'=>$message->subject,'email_message'=>$manager_report_text), 'Daily Cron for user emails.');
                    }
                }
            }
        }

        echo "Finished running! Was anything sent?\r\n";
    }
    
    protected function addToLog($type, $dumped_data=array(), $remarks=''){
        $data = array('type'=>$type, 'dumped_data'=> serialize($dumped_data), 'remarks'=>$remarks, 'timestamp'=>date('Y-m-d H:i:s',time()));
        Yii::app()->db->createCommand()->insert('cron_log', $data);
    }
}

?>
