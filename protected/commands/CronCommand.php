<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Cron jobs for various purpose.
 *
 * @author Faisal Kaleem <faisal.cgs at nxvt.com>
 */
class cronCommand extends CConsoleCommand {
    public function run($arg){
        $this->sendProductionEmails();
    }
    
    private function sendProductionEmails(){
        
//        $today = date('Y-m-d',time());
        $production = new Production();
//        $time = strtotime('9/05/2014');
        $time = time();
        $date_from = date('Y-m-d',$time);
        $date_to = date('Y-m-d',$time+86400);
        $list = Production::getComponentsOrdered($date_from, $date_to, Production::NOT_PRINTED, Component::TYPE_EMAIL);
        foreach($list as $l){
            echo "Attempting to email component {$l['component_id']} to lead {$l['lead_id']}\n";
            $result = Production::sendEmail($l['id'], $l['component_id'], $l['lead_id']);
            if($result){
                $production->markAsPrinted($l['id'], $l['component_id'], $l['lead_id']);
            }
        }
    }
}
