<?php
/*
 * Post deployment script.
 *
 * Scope:
 * - Sets permissions (mainly write permissions wherever needed)
 * - Creates some directories
 * - Flushes runtime directories
 * - Runs migrations (will not on your local machine, unless you explicitly ask it to)
 */
/*if ($argc < 2)
{
	echo "========================================================<br/>";
	echo "<br/>Usage:<br/><br/>";
	echo "========================================================<br/>";
	echo "php " . $argv[0] . " environmentType migrations<br/>";
	echo "<br/>environmentType (required): can be (by default): prod, private (private is your PC)<br/>";
	echo "<br/>*note* additional ones can also be added into the env folders.<br/>";
	echo "<br/>migrations (optional), can be any of these values: migrate, no-migrate<br/>";
	echo "========================================================<br/>";
	exit();
}*/
// $argv = array('no-migrate', 'private');

$runningOnWindows = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';

$envType = $argv[1];

$root = realpath(dirname(__FILE__)) . "/../../";

/**
 * replaces slashes by correspondent system directory separators
 * @param $path
 * @return mixed
 */
function pth($path)
{
	return str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
}

/**
 * returns the executable path according to OS
 * @return string
 */
function getPhpPath()
{
	global $runningOnWindows;
	if ($runningOnWindows) return "php";
	else return '/usr/bin/php';
}

/**
 * runs a command
 * @param $command
 */
function runCommand($command)
{
	global $runningOnWindows;
	if (!$runningOnWindows)
		$command .= ' 2>&1';
	echo "Running command:<br/> $command ";
	$result = array();
	exec($command, $result);
	echo "<br/>Result is: <br/>";
	foreach ($result as $row) echo $row, "<br/>";
	echo "========================================================<br/>";
}

/**
 * Creates a directory if it doesn't exists
 * @param $path
 */
function createDirIfNotExists($path)
{
	if (!file_exists($path))
	{
		printLine("Creating directory: $path");
		mkdir($path);
	}

}

/**
 * Prints a nice line of text
 * @param $text
 */
function printLine($text)
{
	echo "========================================================<br/>";
	echo "$text <br/>";
	echo "========================================================<br/>";
}

/**
 * Remove directories recursively
 * @param $path
 */
function rmDirRecursive($path)
{
	global $runningOnWindows;
	if (!file_exists($path)) return;

	if ($runningOnWindows)
		runCommand("rd /S /Q " . $path);
	else
		runCommand("/bin/rm -rf " . $path);
}

if (!$runningOnWindows)
{
	$result = array();
	echo "Running as:";
	exec('/usr/bin/whoami 2>&1', $result);
	foreach ($result as $row) echo $row, "<br/>";
}

// Flush assets and create directory if not existing
rmDirRecursive(pth($root . "assets"));
createDirIfNotExists(pth($root . "assets"));

// runtime
createDirIfNotExists(pth($root . "protected/runtime"));

// permissions
chmod(pth($root . "protected/runtime"), 02777); // permissions with setguid
chmod(pth($root . "assets"), 02777);

//Copy *-environmnetName.php to *-env.php
$directories = array(
	pth($root . "protected/config/"));  // protected

//choose environment specific configs
foreach ($directories as $dir)
{
	$files = glob(pth($dir . "env/" . "*-" . $envType . '.php'));
	foreach ($files as $srcFile)
	{
		$pattern = "/-" . $envType . ".php$/";
		$dstFile = preg_replace($pattern, "-env.php", $srcFile);
		// remove "env/" or "env\" from the path
		$dstFile = preg_replace('/env\\\|env\//', "", $dstFile);
		$res = copy($srcFile, $dstFile);
		if (!$res)
			echo "Error. Failed to copy $srcFile to $dstFile<br/>";
		// for some reason after copying the permissions are wrong
		chmod($dstFile, 0644);
	}
}

// applying migrations (for local machines is preferred to be done manually but...)
if (($envType != 'private' && !in_array('no-migrate', $argv)) || in_array('migrate', $argv))
{
	runCommand(getPhpPath() . ' \'' . $root . "yiic' migrate --interactive=0");
	if (in_array($envType, array('prod'))) // include any of environment types here at will
		runCommand(getPhpPath() . ' \'' . $root . "yiic' migrate --interactive=0 --connectionID=db");
}

echo "Done!<br/>";