$(document).ready(function(){
    $("#cloneAddress").click(function() {
        $('.MainAddress').clone().appendTo('.AltAddress');
        return false;
        //$(".MainAddress").appendTo('.AltAddress');
        //$('.AltAddress').html($('.MainAddress').html());
    });
    
    $("#cloneManager").click(function() {
//        console.log('test');
        var count = $(".user_email").length;
        console.log(count);
        var clonned = $('.manager-user-fields:first').clone().find('[name^="User"]').each(function(){
//            this.id = this.id.replace('_0_', '_'+count+'_');
            this.id = this.id.replace(/^User_[0-9]+_/,'User_'+count+'_');
//            this.name = this.name.replace('[0]', '['+count+']');
            this.name = this.name.replace(/^User\[[0-9]+\]/, 'User['+count+']');
            this.value = '';
//            if(this.id=='User_'+count+'_id'){ //remove hidden id fields
            if(this.id.match(/^User_[0-9]+_id$/)){ //remove hidden id fields
                $(this).remove();
            }
        }).end();
//        console.log(clonned);
        if($('input#addnew').length > 0){
            var current_value = $('input#addnew').val();
            if(current_value==''){
                $('input#addnew').val(count);
            } else {
                $('input#addnew').val(current_value+','+count);
            }
        }
        $('.manager-user-fields:last').after(clonned);
        clonned.prepend('<button class="btn btn-mini btn-danger remove-user-form"><i class="icon-white icon-remove white"></i></button');
        clonned.before("<hr/>");
        
        $("#User_"+count+"_phone").mask("(999) 999-9999? x99999");
        $("#User_"+count+"_mobile").mask("(999) 999-9999");
        $("#User_"+count+"_fax").mask("(999) 999-9999");
    });
    
    $("button.remove-user-form").live('click', function(e){
        e.preventDefault();
//        if($('input#remove').length > 0){
//            var input_remove = $('input#remove');
//            var current_value = input_remove.val();
//            var idToRemove = 2;
//            
//            if(current_value==''){
//                input_remove.val(idToRemove);
//            } else {
//                input_remove.val(current_value+','+idToRemove);
//            }
//        }
        var elem = this;
        if($(this).siblings('.useridhidden').length>0){
            if(confirm("Are you sure you want to remove this user?")){
                var id = $(this).siblings('.useridhidden').val();
                $.ajax({
                    url:$('base').attr('href')+'/user/delete/'+id,
                    type:"post",
                    dataType:'json',
                    data:{id:id, YII_CSRF_TOKEN:$('input[name="YII_CSRF_TOKEN"]').val(), ajax:'user-form'},
                    success:function(data){
                        if(data.success){
                            removeDivs(elem);
                        } else {
                            alert('Some critical error occured. Could not delete the user. Please try again later.');
                        }
                        
                    }
                });
            } else {
                return false;
            }
            return true;
        }
        removeDivs(elem);
    });
    
});

function removeDivs(elem){
    $(elem).parent('.manager-user-fields').prev("hr").remove();
    $(elem).parent('.manager-user-fields').fadeOut(1000, function(){
        $(elem).remove();
    });
}
