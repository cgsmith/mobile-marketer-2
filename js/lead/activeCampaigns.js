

function openCancelOrderDialog(){
	$('<div></div>').appendTo('body')
	  .html('<div><h6>Are you sure you want to cancel this order for this lead?</h6></div>')
	  .dialog({
	      modal: true, title: 'Cancel Order', zIndex: 10000, autoOpen: true,
	      width: 'auto', resizable: false,
	      buttons: {
	          Yes: function () {
	              cancelOrder();
	              $(this).dialog("close");
	          },
	          No: function () {
	              // doFunctionForNo();
	              $(this).dialog("close");
	          }
	      },
	      close: function (event, ui) {
	          $(this).remove();
	      }
	});
}

function cancelOrder(){
	_url = $('#hid-baseUrl').val()+'/orders/cancelOrder/orderId/'+$('#hid-order-id').val();
    // /orders/cancelOrder/orderId/20603
	$.ajax({
		url: _url,
		async: false,
		data: {'YII_CSRF_TOKEN': $('#hid-csrf').val(),
               'leadId':$('#hid-lead-id').val()},
        type: 'post',
		dataType: 'json',
	}).done(function(data){
		if (data.status == 'failure'){
			alert('Server failure!');
		} else if (data.status == 'success'){
			//alert('Success!');
            $.fn.yiiGridView.update('grid_active_campaigns');
		} else {
			alert('Unknown server failure');
		}
	}).fail(function() {
		//---
	});
	return false;
}