<div style = "margin-left:auto; margin-right:auto;">
<?php
/* @var $this ControllerOrder */
$this->breadcrumbs=array(
    'Verify Order',
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>

<div class="grey_back">

<div style = "font-weight:bold; font-size:18px; padding:10px 0px 0px 25px;">Verify Order - <span style = "text-decoration:underline;"><?php echo $product->name; ?></span></div>
<br clear = "all">
<?php echo CHtml::beginForm(array('orders/confirm')); ?>
<?php
echo CHtml::hiddenField('order_selected',$_POST['selected']);
echo CHtml::hiddenField('order_product_id',$product->id);
?>



<div style = "margin-left:auto; margin-right:auto; width:96%;">
<div style = "text-align:right; margin-left:auto; margin-right:auto; padding-bottom:25px; width:96%;">
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>Yii::t('core','lead.index.order_products'),
    'type'=>'success',
    'icon'=>'shopping-cart white',
    'htmlOptions'=>array('id'=>'order-btn'),
    'url'=>array('orders/verify'),
)); ?>
</div>

<?php echo CHtml::endForm(); ?>
<?php 
$this->widget('ext.selgridview.SelGridView', array(
//$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'mygrid',
    'dataProvider' => $order_customer_leads_list,
    'selectableRows' => 2,
    'beforeAjaxUpdate' => "function(id, options) {
        if(!options.url.match(/product_id/)){
            options.url += '&selected=".$_POST['selected']."';
            options.url += '&product_id=".$product->id."';
            var params = $.deparam.querystring(options.url);
            options.url = $.param.querystring(options.url, params);
        }
    } ",
    'columns' => array(
        array('name'=>'id','header'=> '#', 'visible'=>false),
        array('name'=>'first','header'=> 'First Name'),
        array('name'=>'last','header'=> 'Last Name'),
		array('name'=>'company','header'=> 'Company'),
//        array('name'=>'email','header'=> 'Email'),
    )
));

//$gridColumns = array(
//    array('name'=>'id','header'=> '#', 'visible'=>false),
//    array('name'=>'first','header'=> 'First Name'),
//    array('name'=>'last','header'=> 'Last Name'),
//    array('name'=>'title','header'=> 'Title'),
//    array(
//        'htmlOptions' => array('nowrap'=>'nowrap'),
//        'class'=>'bootstrap.widgets.TbButtonColumn',
//    )
//);
//
//$this->widget('bootstrap.widgets.TbGridView', array(
//    'dataProvider'=> $order_customer_leads_list,
//    'template'=>"{items}",
//    'columns'=>$gridColumns,
//));
?>
</div>
</div></div>