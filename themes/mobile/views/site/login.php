
<?php

$this->pageTitle=Yii::app()->name . ' - Login';


// Display information messages

if (Yii::app()->request->getQuery('logout') !== null) {

    Yii::app()->user->setFlash('success', '<strong>Logout Successful</strong> You\'ve successfully logged out!');

    $this->widget('bootstrap.widgets.TbAlert');

}

$style = '-webkit-box-shadow: inset #c9c9c9 5px 5px 7px; -moz-box-shadow: inset #c9c9c9 4px 5px 7px; box-shadow: inset #c9c9c9 4px 5px 7px; border:1px solid #b9b9b9;';


?>



<div style = "background-image:url(<?php echo Yii::app()->theme->baseUrl; ?>/images/form_field_sides.png); background-repeat:repeat-y; padding:10px 0px 10px 20px;">
	

<?php $form=$this->beginWidget('CActiveForm', array(

	'id'=>'login-form',

	'enableClientValidation'=>true,

	'clientOptions'=>array(

		'validateOnSubmit'=>true,

	),

)); ?>



<?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
	Html5::activeTextField($model,'username',array('style'=>$style))));?> 
			
<?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
	Html5::activeTextField($model,'password',array('style'=>$style,'type'=>'password'))));?>
 

 <?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
	$form->checkBox($model,'rememberMe',array('style'=>$style)).$form->label($model,'rememberMe')
	));?>
	
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>'Login'));?>


<?php $this->endWidget(); ?>
    <br/>

<?php $this->widget('bootstrap.widgets.TbButton', array('label'=>'Reset Password','url'=>'/site/resetpassword'));?>

</div><!-- form -->