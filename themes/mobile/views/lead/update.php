<div class="grey_back">
<?php
$this->breadcrumbs=array(
	'Leads'=>array('index'),

    $model->last.', '.$model->first => array('view','id'=>$model->id),
	'Update',
);

?>

<div style = "padding:15px 0px 25px 30px; font-size:16px; font-weight:bold;">Update <?php echo $model->last; ?>, <?php echo $model->first;?> </div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>