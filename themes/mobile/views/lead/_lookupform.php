<div class="create_form">

<script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {
jQuery('a[rel="tooltip"]').tooltip();
jQuery('a[rel="popover"]').popover();
jQuery("#Lead_phone").mask("(999) 999-9999? x99999");
jQuery("#Lead_fax").mask("(999) 999-9999");

					if(!window.location.hash)
						jQuery('#Lead_first').focus();
				
});
/*]]>*/
</script>
<?php
$style = '-webkit-box-shadow: inset #c9c9c9 5px 5px 7px; -moz-box-shadow: inset #c9c9c9 4px 5px 7px; box-shadow: inset #c9c9c9 4px 5px 7px; border:1px solid #b9b9b9; font-size:18px; text-transform:capitalize;';



$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'lead-form',
    'method'=>'get',
    'type'=>'horizontal',
    'focus'=>array($model,'first'),
));
?>

	<?php echo $form->errorSummary($model); ?>

		<?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'first',array('style'=>$style))));?>
		<?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'last',array('style'=>$style))));?>
        <?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'title',array('style'=>$style))));?>
        <?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'company',array('style'=>$style))));?>
        <?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'address1',array('style'=>$style))));?>
        <?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'address2',array('style'=>$style))));?>
        <?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'city',array('style'=>$style))));?>
            
		<?php echo $form->dropDownListRow($model,'state',CHtml::listData(States::model()->findAll(array('order'=>'name')), 'abbreviation','name'),array('style'=>'','empty'=>'--select state--')); ?>

		<?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'zip',array('type'=>'number','style'=>$style))));?>

         <?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'phone',array('type'=>'number','style'=>$style))));?>
         <?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'fax',array('type'=>'number','style'=>$style))));?>
         <?php echo CHtml::tag('div',array('class'=>'control-group'),CHtml::tag('div',array('class'=>'controls'),
            Html5::activeTextField($model,'email',array('type'=>'email','style'=>$style))));?>
            

        <div class="control-group">
        <?php echo $form->label($model,'keywords',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php
            echo CHtml::checkBoxList('keywords',
                CJSON::decode($model->keywords),
                CHtml::listData(Keyword::model()->findAll(array(
                        'condition'=>'customer_main_id=:id AND active=1',
                        'params'=>array(':id'=>Yii::app()->user->customer_id)
                    )),'id','keyword'),
                array('style'=>'float: left')
            ); ?>
        </div>
    </div>

	<div class="form-actions" style = "background-color:transparent;">
       <div style = "float:left; padding-right:15px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit','type'=>'primary','label'=>'Search'));
		?>
        </div>
        <div style = "float:left;">
		<?
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset','label'=>'Reset'));
	    ?>
        </div><br clear = "all" />
    </div>
    </div>
    
<?php $this->endWidget(); ?>

</div><!-- form -->