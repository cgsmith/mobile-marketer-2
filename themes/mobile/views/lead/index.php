<?php
$this->breadcrumbs=array(
	'Leads',
);

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".info").animate({opacity: 1.0}, 3000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<div class="info">
    <?php
    foreach(Yii::app()->user->getFlashKeys() as $key) {
        if(Yii::app()->user->hasFlash($key)) { ?>
            <div class="flash-<?php echo $key; ?>">
                <?php echo Yii::app()->user->getFlash($key); ?>
            </div>
            <?php }
    }?>
</div>

<div class="grey_back">
<div style = "padding:15px 0px 0px 15px; font-size:18px; font-weight:bold;"><?php echo Yii::t('core','lead.Leads'); ?></div>

<?php if(Yii::app()->user->hasFlash('error')){ ?>
<div class="flash-error">
    <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php } ?>

<div style = "padding:0px 0px 0px 15px;"><?php echo Yii::t('core','lead.hint'); ?></div>

<div style = "width:95%; margin-left:auto; margin-right:auto;">
<?php
$this->widget('ext.selgridview.SelGridView', array(
    'id'=>'mygrid',
    'dataProvider'=>$dataProvider,
    'selectableRows'=>2,
    'columns'=>array(
        array(
            'class'=>'CCheckBoxColumn',
        ),
        array('name'=>'first', 'header'=>Yii::t('core','model.lead.first')),
        array('name'=>'last', 'header'=>Yii::t('core','model.lead.last')),
        array('name'=>'company', 'header'=>Yii::t('core','model.lead.company')),
        array(
            'class'=>'CLinkColumn',
            'label'=>'View', 
			'urlExpression'=>'Yii::app()->createUrl("lead/view",array("id"=>$data->id))',
        ),
    ),
));
?>
</div>


<script type="text/javascript">
jQuery(function($) {
$("#order-btn").click(function(){
var selected = $("#mygrid").selGridView("getAllSelection");
$("#selected-keys").val("[" + selected.join(", ") + "]");
});

});
</script>
<?php echo CHtml::beginForm(array('orders/create')); ?>

<?php echo CHtml::hiddenField('selected','',array('id'=>'selected-keys')); ?>

<div id="submitfooter" style="width:100%; padding:0px 0px 10px 13px; background-color: transparent;">
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType'=>'submit',
    'label'=>Yii::t('core','lead.index.order_products'),
    'type'=>'success',
    'icon'=>'shopping-cart white',
    'htmlOptions'=>array('id'=>'order-btn'),
    'url'=>array('orders/create'),
    )); ?>
</div>

<?php echo CHtml::endForm(); ?>
</div>