<?php
$this->breadcrumbs=array(
	'Leads'=>array('index'),
	'New',
);

// Display information messages
if (Yii::app()->request->getQuery('success') !== null) {
    Yii::app()->user->setFlash('success', '<strong>Success</strong> '. Yii::app()->request->getQuery('success'));
    $this->widget('bootstrap.widgets.TbAlert');
}
?>

<div class="grey_back">
<div class="form_title" style = "padding-bottom:20px;">New Lead</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>