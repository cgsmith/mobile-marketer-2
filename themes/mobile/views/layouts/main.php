<!DOCTYPE html>
<html>
 <head>
   
<meta name="viewport" content="target-densitydpi=device-dpi" /> 
  <title><?php echo CHtml::encode($this->pageTitle); ?>®</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/site_global.css?3896285136"/>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/index.css?74475191" id="pagesheet"/>
  <!-- Other scripts -->
  <script type="text/javascript">
   document.documentElement.className = 'js';
</script>
   </head>
 <body>

  <div class="gradient clearfix" id="page"><!-- column -->
   <div class="position_content" id="page_position_content">
    <div class="clearfix colelem" id="u91"><!-- group -->
     <div style = "text-align:center;"><div style = "padding:10px 0px 10px 0px;"><img id="u86_img" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/mm_logo_new.png" alt="" width="300" /></div></div>
    </div>
    <div class="clearfix colelem" id="u103"><!-- group -->
     <div class="grpelem" id="u143"><!-- image -->
       <?php if (!Yii::app()->user->isGuest) { ?>
       <div style = "float:left; width:40%; font-size:18px; font-weight:bold; color:#CCC; padding-left:15px;"> <?php if(!Yii::app()->user->isGuest) { echo 'Hello, '. Yii::app()->user->name; } ?></div>
       
        <div style = "text-align:right; padding-right:10px; float:left; width:45%"><a href="/site/logout"><img id="u143_img" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logout_button.png" alt="" width="58" height="24"/></a></div><br clear = "all">
       <?php } else { ?>
        
        <div style = "float:left; width:40%; font-size:18px; font-weight:bold; color:#CCC; padding-left:15px;">&nbsp; <?php if(!Yii::app()->user->isGuest) { echo 'Hello, '. Yii::app()->user->name; } ?></div>
        
        <div style = "text-align:right; padding-right:10px; float:left; width:45%">
            <?php if (Yii::app()->request->url !== '/site/login') { ?>
            <a href="/site/login"><img id="u143_img" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/login_button.png" alt="" width="58" height="24"/></a>
            <?php } ?>
        </div>
        
        <br clear = "all">
        
        <?php } ?>
     </div>
    </div>

    
    
    
       <?php if (!Yii::app()->user->isGuest) { // Three icons on login 
       ?>
    <div class="clearfix colelem" id="u110" style = "height:100px; border-top:1px solid #333; border-bottom:1px solid #333;">
    <!-- group -->
    <div style = "min-width:380px; width:100%; margin-left:auto; margin-right:auto; padding-top:10px;">
       
         <a href="/lead/create"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/new_lead.png" alt="" width="85" /></a>
         <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/link_spacer_large.png">
         <a href="/lead/lookup"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/lookup_lead.png" alt="" width="75" /></a>
         <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/link_spacer_large.png">
         <a href="/lead/index"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/list_lead.png" alt="" width="65" /></a>
       
    </div>
    
    </div>
    
       <?php } ?>
    
    
    
    <div class="verticalspacer"><?php echo $content; ?></div>
    
   </div>
  </div>
  <!-- JS includes -->
  <script type="text/javascript">
   if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/1.1/jquery-1.7.min.js" type="text/javascript">\x3C/script>');
</script>
  <script type="text/javascript">
   window.jQuery || document.write('\x3Cscript src="scripts/1.1/jquery-1.7.min.js" type="text/javascript">\x3C/script>');
</script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/scripts/1.1/museutils.js?410024474" type="text/javascript"></script>
  <script src="<?php echo Yii::app()->theme->baseUrl; ?>/scripts/1.1/jquery.watch.js?3882405427" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
   Muse.Utils.addSelectorFn('body', Muse.Utils.transformMarkupToFixBrowserProblemsPreInit);/* body */
Muse.Utils.addSelectorFn('body', Muse.Utils.prepHyperlinks); /* body */
Muse.Utils.fullPage('#page'); /* 100% height page */
Muse.Utils.addSelectorFn('body', Muse.Utils.showWidgetsWhenReady);/* body */
Muse.Utils.addSelectorFn('body', Muse.Utils.transformMarkupToFixBrowserProblems);/* body */

</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39235068-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
   </body>
</html>
