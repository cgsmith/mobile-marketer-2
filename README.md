# Mobile Marketer

**The current version of Mobile Marketer is** `v2.0.1`

## Requirements
Mobile Marketer has dependencies like any other code base.  Currently the following dependencies are:

* PHP >= 5.4.29
    * $_SERVER
    * Calendar
    * Ctype
    * curl
    * date
    * dom
    * ereg
    * exif
    * fileinfo
    * filter
    * ftp
    * gd with freetype
    * hash
    * iconv
    * imagick
    * intl
    * json
    * libxml
    * mbstring
    * mcrypt
    * mysql
    * mysqli
    * openssl
    * pcre
    * PDO
    * PDO MySQL
    * Phar
    * Reflection
    * session
    * SimpleXML
    * soap
    * SPL
    * tidy
    * zip
    * zlib
    * INI Settings
        * display_errors = off
        * file_uploads = on
        * upload_max_filesize = 2M
        * memory_limit = 512M
        * post_max_size = 8M
        * log_errors = on
        * log_errors_max_len = 1024
        * error_reporting = E_ALL & ~E_NOTICE
* MySQL 5.5.38
* Yii 1.1.15
* Ability to setup CRON/scheduled tasks
* Access to standard mail ports (587 for Mandrill)

## CRON Setup

```
0 7 * * 1 php /{path}/protected/cron.php reports weekly
11 * * * * /usr/bin/find /tmp -user {username} -cmin +60 -not -path "*sess*" -print0 2> /dev/null|/usr/bin/xargs -0 /bin/rm -rf
0 6 1 * * php /{path}/protected/cron.php reports billing
*/5 * * * * php /{path}/protected/cron.php cron > /dev/nul 2>&1
```

## After committing

After you are done committing your changes locally test them on the QA/Staging server.  Once the code is accepted you may tag to the new version (see versioning)
and pull into production.  **Remember** commit your sql scripts to run when running the post deploy scripts and always backup the DB before pulling changes!

## Versioning

This project uses [SemVer](http://www.semver.org) versioning.  If you are committing a bug fix (or feature add) please follow SemVer and push your
new tag as well.